/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auxiliares;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Locale;

import com.dropbox.core.DbxAccountInfo;
import com.dropbox.core.DbxAppInfo;
import com.dropbox.core.DbxAuthFinish;
import com.dropbox.core.DbxClient;
import com.dropbox.core.DbxEntry;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.DbxWebAuthNoRedirect;
import com.dropbox.core.DbxWriteMode;

public class UploadDropbox {

    private static final String DROP_BOX_APP_KEY = "8f1drqbjhi9k1nc";
    private static final String DROP_BOX_APP_SECRET = "emqfaj28ue1n8ng";
    DbxClient dbxClient;

    public static void cargarArchivo(String nombre, String Url) throws IOException, DbxException {
        UploadDropbox javaDropbox = new UploadDropbox();
        String codigo = Codigo.obtenerCodigo().getCodId();
        System.out.println(codigo);
        javaDropbox.authDropbox(DROP_BOX_APP_KEY, DROP_BOX_APP_SECRET,codigo);
        Codigo.inhabilitarCodigo(codigo);
        javaDropbox.uploadToDropbox(nombre,Url);
    }
    
    public static void descargarArchivo(String nombre, String Url) throws IOException, DbxException {
        UploadDropbox javaDropbox = new UploadDropbox();
        String codigo = Codigo.obtenerCodigo().getCodId();
        javaDropbox.authDropbox(DROP_BOX_APP_KEY, DROP_BOX_APP_SECRET,codigo);
        Codigo.inhabilitarCodigo(codigo);
        javaDropbox.downloadFromDropbox(nombre, Url);
    }

    

    private DbxClient authDropbox(String dropBoxAppKey, String dropBoxAppSecret, String code)
            throws IOException, DbxException {
        DbxAppInfo dbxAppInfo = new DbxAppInfo(dropBoxAppKey, dropBoxAppSecret);
        DbxRequestConfig dbxRequestConfig = new DbxRequestConfig(
                "JavaDropboxTutorial/1.0", Locale.getDefault().toString());
        DbxWebAuthNoRedirect dbxWebAuthNoRedirect = new DbxWebAuthNoRedirect(
                dbxRequestConfig, dbxAppInfo);
                      
        String dropboxAuthCode = code;
        DbxAuthFinish authFinish = dbxWebAuthNoRedirect.finish(dropboxAuthCode);
        String authAccessToken = authFinish.accessToken;
        dbxClient = new DbxClient(dbxRequestConfig, authAccessToken);
         return dbxClient;
    }

    /* returns Dropbox size in GB */
    private long getDropboxSize() throws DbxException {
        long dropboxSize = 0;
        DbxAccountInfo dbxAccountInfo = dbxClient.getAccountInfo();
        // in GB :)
        dropboxSize = dbxAccountInfo.quota.total / 1024 / 1024 / 1024;
        return dropboxSize;
    }

    private void uploadToDropbox(String fileName, String URL) throws DbxException,
            IOException {
        File inputFile = new File(URL);
        FileInputStream fis = new FileInputStream(inputFile);
        try {
            DbxEntry.File uploadedFile = dbxClient.uploadFile("/" + fileName,
                    DbxWriteMode.add(), inputFile.length(), fis);
            String sharedUrl = dbxClient.createShareableUrl("/" + fileName);
            System.out.println("Uploaded: " + uploadedFile.toString() + " URL "
                    + sharedUrl);
        } finally {
            fis.close();
        }
    }

    private void createFolder(String folderName) throws DbxException {
        dbxClient.createFolder("/" + folderName);
    }

    private void listDropboxFolders(String folderPath) throws DbxException {
        DbxEntry.WithChildren listing = dbxClient
                .getMetadataWithChildren(folderPath);
        System.out.println("Files List:");
        for (DbxEntry child : listing.children) {
            System.out.println("	" + child.name + ": " + child.toString());
        }
    }

    private void downloadFromDropbox(String fileName, String Url) throws DbxException,
            IOException {
        FileOutputStream outputStream = new FileOutputStream(Url);

        try {
            DbxEntry.File downloadedFile = dbxClient.getFile("/" + fileName,
                    null, outputStream);
            
        } finally {
            outputStream.close();
        }
    }
}
