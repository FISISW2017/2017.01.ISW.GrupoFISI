/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auxiliares;

import Conector.Conexion;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import Modelo.Codigos;
/**
 *
 * @author  
 */
public class Codigo {
    public static Codigos obtenerCodigo() {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Codigos> apoyo = session.createQuery("from Codigos where estado <> 'N' ").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo.get(0);
        } else {
            return null;
        }

    }
    public static void inhabilitarCodigo(String codId){
         SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Codigos> apoyo = session.createQuery("from Codigos where codId = '"+ codId +"'").list();
        tx.commit();
        session.close();
        Codigos codigo = apoyo.get(0);
        codigo.setEstado('N');
         SessionFactory sesion2 = Conexion.getSessionFactory();
        Session session2 = sesion2.openSession();
        Transaction tx2 = session2.beginTransaction();
        session2.update(codigo);
         tx2.commit();
        session2.close();
        
    }
    
}
