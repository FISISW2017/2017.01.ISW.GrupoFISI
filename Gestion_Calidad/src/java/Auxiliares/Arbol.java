/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auxiliares;

import Controlador.C_Jerarquia;
import Controlador.C_Niveles;
import Controlador.C_Proceso;
import Modelo.Jerarquia;
import Modelo.NivelInferior;
import Modelo.NivelInferiorId;
import Modelo.NivelSuperior;
import Modelo.Niveles;
import Modelo.NivelesId;
import Servicios.S_AdminPrograma;
import Servicios.S_Administracion;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;

/**
 *
 * @author  
 */
public class Arbol {

    private static int secuencia;

    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
 /*+         FUNCIONES PARA OBTENER ÁRBOL GUARDADO EN LA BD           +*/
 /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    public static JTree obtieneArbol(String procesoId) {
        JTree tree = null;
        if (C_Niveles.existeNivelProceso(procesoId)) {
            String nombre = C_Proceso.obtenerProceso(procesoId).getProcesoNombre();
            DefaultMutableTreeNode raiz = new DefaultMutableTreeNode(nombre);
            DefaultTreeModel modelo = new DefaultTreeModel(raiz);
            tree = new JTree(modelo);
            int secRaiz = 0;
            List<Niveles> niveles = S_Administracion.nivelesProceso(procesoId);
            ArrayList<DefaultMutableTreeNode> Nodos = new ArrayList<>();
            ArrayList<String> ids = new ArrayList<>();
            ArrayList<Integer> secuen = new ArrayList<>();
            /*+ primer paso: crear los nodos asociando los niveles y llenar los vectors auxiliares+*/
            for (int i = 0; i < niveles.size(); i++) {
                String nom_nodo = niveles.get(i).getNivelNombre();
                String niv_id = niveles.get(i).getId().getNivelId();
                DefaultMutableTreeNode nodo = new DefaultMutableTreeNode(nom_nodo);
                Nodos.add(nodo);
                ids.add(niv_id);
                secuen.add(0);
            }

            /*+ segundo paso: recuperar niveles inferiores y generar árbol +*/
            List<NivelInferior> nivInferior = S_Administracion.nivelesInferiorProceso(procesoId);
            /*+ recorrelos id buscando inferiores y creando árbol +*/
            for (int i = 0; i < ids.size(); i++) {
                String id_sup = buscaInferior(nivInferior, ids.get(i));
                if ("".equals(id_sup)) {
                    /*+ relaciona nodo a la raiz +*/
                    modelo.insertNodeInto(Nodos.get(i), raiz, secRaiz);
                    secRaiz++;
                } else {
                    /*+ busca posición de nodo padre para relacionar +*/
                    int pos_padre = posicionPadre(id_sup, ids);
                    /*+ asocia nodo con su padre+*/
                    int sec = secuen.get(pos_padre);
                    modelo.insertNodeInto(Nodos.get(i), Nodos.get(pos_padre), sec);
                    sec++;
                    secuen.set(pos_padre, sec);
                }
            }
        }
        return tree;
    }

    public static String buscaInferior(List<NivelInferior> lista, String idbus) {
        String idsup = "";
        for (int i = 0; i < lista.size(); i++) {
            String id = lista.get(i).getId().getIdInf();
            if (id.equals(idbus)) {
                idsup = lista.get(i).getId().getIdSup();
                i = lista.size();
            }
        }

        return idsup;
    }

    public static int posicionPadre(String idpadre, ArrayList<String> ids) {
        int pos = 0;
        for (int i = 0; i < ids.size(); i++) {
            String id = ids.get(i);
            if (id.equals(idpadre)) {
                pos = i;
                i = ids.size();
            }
        }
        return pos;
    }

    /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
 /*+         FUNCIONES PARA GUARDAR ÁRBOL EN LA BD           +*/
 /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    public static String saveTree(JTree tree, String procesoId) {
        String mensaje;
        String proc_name = C_Proceso.obtenerProceso(procesoId).getProcesoNombre();
        if (!S_Administracion.existeNivelProceso(proc_name)) {
            guardarArbol(tree, procesoId);
            mensaje = "Niveles Guardados";
        } else if (!S_AdminPrograma.existeProgramaProceso(procesoId)) {
            S_Administracion.borrarNivelesProceso(procesoId);
            guardarArbol(tree, procesoId);
            mensaje = "Niveles Modificados con éxito";
        } else {
            
            mensaje = "ERROR! YA SE HA ASIGNADO EL PROCESO A UN PROGRAMA.\n";
            mensaje += "No se puede modificar la estructura de Niveles cuando existe una asociación de este proceso a un programa";
        }
        return mensaje;
    }

    public static void guardarArbol(JTree tree, String procesoId) {
        TreeNode raiz = (TreeNode) tree.getModel().getRoot();
        secuencia = 0;
        int jerarquia = 0;
        String nivel_padre = "";
        int jerarq_padre = 0;
        guardaNodo(raiz, jerarquia, nivel_padre, jerarq_padre, procesoId);
    }

    public static void guardaNodo(TreeNode nodo, int jerarquiaId, String nivel_padre, int jerarq_padre, String procesoId) {
        String npadre = "";

        if (jerarquiaId > 0) {
            /*+ guardar nivel actual+*/
            secuencia++;
            String nivelId = procesoId + generaId(secuencia, 4);
            NivelesId id = new NivelesId(nivelId, jerarquiaId, procesoId);
            Jerarquia jerarquia = S_Administracion.obtenerJerarquia(jerarquiaId);
            String nivelNombre = nodo.toString();
            Niveles niveles = new Niveles(id, jerarquia, nivelNombre);
            S_Administracion.nuevoNivel(niveles);

            /*+ guardar nivel actual como superior+*/
            NivelSuperior nivel_superior = new NivelSuperior(nivelId, niveles);
            S_Administracion.nuevoNivelSuperior(nivel_superior);

            /*+ valida si hay padre y guarda relación nivel y padre+*/
            if (!"".equals(nivel_padre)) {

                NivelInferiorId id_inf = new NivelInferiorId(nivelId, nivel_padre);
                Niveles nivel = S_Administracion.obtenerNivel(nivelId, procesoId, jerarquiaId);
                NivelSuperior nivel_sup = S_Administracion.obtenerNivelSuperior(nivel_padre);
                NivelInferior nivel_inf = new NivelInferior(id_inf, nivel_sup, nivel);
                S_Administracion.nuevoNivelInferior(nivel_inf);

            }

            //asigna npadre
            npadre = nivelId;

            System.out.println("Guardando: " + nodo.toString());
        } else {
            jerarquiaId = S_Administracion.menorJerarquia(procesoId) - 1;
        }

        if (nodo.getChildCount() >= 0) {
            for (Enumeration e = nodo.children(); e.hasMoreElements();) {
                TreeNode n = (TreeNode) e.nextElement();
                guardaNodo(n, jerarquiaId + 1, npadre, jerarquiaId, procesoId);
            }
        }
    }

    public static String generaId(int num, int longitud) {
        String numero = num + "";
        int cant = longitud - numero.length();
        String zeros = "";
        for (int i = 0; i < cant; i++) {
            zeros += "0";
        }
        String id = zeros + numero;
        return id;
    }

}
