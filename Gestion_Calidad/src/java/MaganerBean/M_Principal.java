/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MaganerBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.html.HtmlPanelGrid;
import org.primefaces.component.chart.Chart;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.panel.Panel;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author Richard
 */
@ManagedBean (name = "M_Principal")
@SessionScoped
public class M_Principal {
    
    private HtmlPanelGrid panelGrid;

    public HtmlPanelGrid getPanelGrid() {
         agregarPaneles();
        return panelGrid;
    }

    public void setPanelGrid(HtmlPanelGrid panelGrid) {
        this.panelGrid = panelGrid;
    }
    
    
    private void agregarPaneles(){
     
      panelGrid = new HtmlPanelGrid();
         panelGrid.getChildren().add(nuevoPanel("INGENIERIA DE SOFTWARE"));
          panelGrid.getChildren().add(nuevoPanel("INGENIERIA DE SISTEMAS"));
                panelGrid.getChildren().add(nuevoPanel("INGENIERIA DE SISTEMA DOS"));
          panelGrid.getChildren().add(nuevoPanel("INGENIERIA DE SISTEMAS TRES"));
           panelGrid.getChildren().add(nuevoPanel("INGENIERIA DE SISTEMAS CUATRO"));
    
    }
    
    private Panel nuevoPanel(String encabecado)
    {
        Panel panel = new Panel();
        panel.setHeader(encabecado);
        panel.setStyle("height: auto; width: 500px;padding-left: 0px; padding-right: 0px; padding-top: 0px");
        
         Chart grafica = new Chart();
        grafica.setType("pie");

        PieChartModel pieModel2 = new PieChartModel();

        pieModel2.set("Brand 1", 540);
        pieModel2.set("Brand 2", 325);
       

        pieModel2.setTitle("Custom Pie");
        pieModel2.setLegendPosition("e");
        pieModel2.setFill(false);
        pieModel2.setShowDataLabels(true);
        pieModel2.setDiameter(150);

        grafica.setModel(pieModel2);
        grafica.setStyle("width:auto;height:300px");
        
        CommandButton boton = new CommandButton();
        boton.setValue("Mas Detalles");
        boton.setStyle("width : 100%");
        panel.getChildren().add(grafica);
        panel.getChildren().add(boton);
        return panel;
    }
    
    
    
}
