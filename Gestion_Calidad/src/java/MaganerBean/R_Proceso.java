/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MaganerBean;

import Servicios.S_Administracion;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.swing.JFrame;
import org.primefaces.component.dashboard.Dashboard;
import org.primefaces.component.panel.Panel;
import org.primefaces.context.RequestContext;

import org.primefaces.model.DashboardColumn;
import org.primefaces.model.DashboardModel;
import org.primefaces.model.DefaultDashboardColumn;
import org.primefaces.model.DefaultDashboardModel;

@ManagedBean
@ViewScoped
public class R_Proceso implements Serializable {

 
    private Dashboard dashboard;
    private DashboardModel model;
    private String nivel = "";
    private String proceso = "";
    private String descripcion;

    private boolean estado = false;

    
    
    
    public boolean isEstado() {
        return estado;
    }
    
    
    
    
    
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
  
    
    
    
    
    public String getProceso() {
        return proceso;
    }

    public void setProceso(String proceso) {
        this.proceso = proceso;
    }
    
    
    
    
    private ArrayList<String> losNiveles = new ArrayList<>();
    
    DashboardColumn column1 = new DefaultDashboardColumn();
    
    String[] estilos = new String[] {"azul","verde","oscuro","claro"};
    private int i = 0;
    
    
    
    
    @PostConstruct
    public void init() {
        model = new DefaultDashboardModel();
        
       model.addColumn(column1);

    }

     public void buttonAction(ActionEvent actionEvent) {
      
       RequestContext context = RequestContext.getCurrentInstance();
       
       i++;
       losNiveles.add(nivel.toUpperCase());
         Panel p = new Panel();
         p.setClosable(true);
        p.setStyle("margin: 10px; width : 250px ; padding: 0px " );
        int j = i%4;
        p.setStyleClass(estilos[j]);
         p.setId("a"+i);
        p.setHeader(nivel.toUpperCase());
        dashboard.getChildren().add(p);
       
        if(i == 4)
            estado = true;
       column1.addWidget(p.getId());
      nivel = "";
       context.addCallbackParam("loggedIn", true);
        
    }

    public Dashboard getDashboard() {
        return dashboard;
    }

    public void setDashboard(Dashboard dashboard) {
        this.dashboard = dashboard;
    }
    
 public String getNivel() {
    
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }
    
    public DashboardModel getModel() {
        return model;
    }
    
      public void accionGuardar(ActionEvent actionEvent) {
         
          guardarDatos();
      }
    
    
    public void guardarDatos()
    {
        String[] Niveles= new String[dashboard.getChildCount()];
        for(int j = 0; j < dashboard.getChildCount();j++)
            Niveles[j] = ((Panel) dashboard.getChildren().get(j)).getHeader();
        boolean global = true;
        String estado = "Activo";
        S_Administracion.nuevoProceso(proceso, Niveles, global, estado, descripcion);
        String respuesta = "Proceso Creado";
         addMessage(respuesta);
    }
    
     
 
    
    
    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    
    
}
