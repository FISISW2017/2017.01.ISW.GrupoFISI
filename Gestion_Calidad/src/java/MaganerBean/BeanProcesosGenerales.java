/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MaganerBean;

import MaganerBean.Adicionales.Proceso;
import Servicios.S_Administracion;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "ProcesoGeneral")
@SessionScoped
@ViewScoped
public class BeanProcesosGenerales {

    private ArrayList<ArrayList<String>> valores;
    //private List<ArrayList> valores;
    private List<Proceso> lista = new ArrayList<>();

    @PostConstruct
    public void init() {

        valores = S_Administracion.todosProcesos();
        for (int i = 0; i < valores.size(); i++) {
            lista.add(new Proceso(valores.get(i)));
            
        }
        /*
        valores = new ArrayList<>();
        // en ves de uno nuevo pones tu metodo;
        ArrayList<String> p = new ArrayList<>();
        p.add("MATRICULA");
        p.add("nivel 1");
        p.add("nivel 2");
        p.add("nivel 3");

        valores.add(p);
        valores.add(p);
        valores.add(p);
        valores.add(p);
        for (int i = 0; i < valores.size(); i++) {
            lista.add(new Proceso(valores.get(i)));
        }*/
    }

    public List<Proceso> getLista() {
        return lista;
    }

    public void setLista(List<Proceso> lista) {
        this.lista = lista;
    }

}
