/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MaganerBean;

import Servicios.S_AdminPrograma;
import Servicios.S_Administracion;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

@ManagedBean
@SessionScoped
@ViewScoped

public class BeanGestionDocumentos {

    private String programa, proceso; // asignar valores 
    private List<String> list_prog, list_proc; // lista de los combo box

    private boolean Pa1, Pa3, Pa4, Pa5;

    private int Num_Niv = 0, clic = 0;
    private String[] losNiveles;
    private boolean[] mostrar = {true, true, true, true};

    private String Sn1, Sn2, Sn3, Sn4;

    private ArrayList<ArrayList<String>> L1 = new ArrayList<>();
    private ArrayList<ArrayList<String>> L2 = new ArrayList<>();
    private ArrayList<ArrayList<String>> L3 = new ArrayList<>();
    private ArrayList<ArrayList<String>> L4 = new ArrayList<>();

    private List<String[]> criterios;
    private List<String[]> lasFuentes;
    private boolean Btn0;
    private boolean Btn1;

    private String seleccionado;
    
    private String criterioSelec;
    
    
    
    @PostConstruct
    public void init() {
        list_prog = S_Administracion.todosNombresProgramas();
        
        list_proc = new ArrayList<>();
        

        Pa1 = false;
        Pa3 = false;

        Btn1 = true;
        Btn0 = true;
        
        L1.add(new ArrayList<>());
        L2.add(new ArrayList<>());
        L3.add(new ArrayList<>());
        L4.add(new ArrayList<>());

    }

    public String getCriterioSelec() {
        return criterioSelec;
    }

    public void setCriterioSelec(String criterioSelec) {
        this.criterioSelec = criterioSelec;
    }

   

    
    
    
    public List<String[]> getLasFuentes() {
        return lasFuentes;
    }

    
    
    public boolean isPa5() {
        return Pa5;
    }

    public boolean isBtn0() {
        return Btn0;
    }

    public List<String[]> getCriterios() {
        return criterios;
    }

    public boolean isPa4() {
        return Pa4;
    }

    public boolean isBtn1() {
        return Btn1;
    }

    public boolean isPa3() {
        return Pa3;
    }

    public List<String> getL1() {
        return L1.get(0);
    }

    public List<String> getL2() {
        return L2.get(0);
    }

    public List<String> getL3() {
        return L3.get(0);
    }

    public List<String> getL4() {
        return L4.get(0);
    }

    public String getSn1() {
        return Sn1;
    }

    public void setSn1(String Sn1) {
        this.Sn1 = Sn1;
    }

    public String getSn2() {
        return Sn2;
    }

    public void setSn2(String Sn2) {
        this.Sn2 = Sn2;
    }

    public String getSn3() {
        return Sn3;
    }

    public void setSn3(String Sn3) {
        this.Sn3 = Sn3;
    }

    public String getSn4() {
        return Sn4;
    }

    public void setSn4(String Sn4) {
        this.Sn4 = Sn4;
    }

    public boolean[] getMostrar() {
        return mostrar;
    }

    public void setMostrar(boolean[] mostrar) {
        this.mostrar = mostrar;
    }

    public int getNum_Niv() {
        return Num_Niv;
    }

    public void setNum_Niv(int Num_Niv) {
        this.Num_Niv = Num_Niv;
    }

    public String[] getLosNiveles() {
        return losNiveles;
    }

    public void setLosNiveles(String[] losNiveles) {
        this.losNiveles = losNiveles;
    }

    public boolean isPa1() {
        return Pa1;
    }

    public void setPa1(boolean Pa1) {
        this.Pa1 = Pa1;
    }

    public String getPrograma() {
        return programa;
    }

    public void setPrograma(String programa) {
        this.programa = programa;
    }

    public String getProceso() {
        return proceso;
    }

    public void setProceso(String proceso) {
        this.proceso = proceso;
    }

    public List<String> getList_prog() {
        return list_prog;
    }

    public void setList_prog(List<String> list_prog) {
        this.list_prog = list_prog;
    }

    public List<String> getList_proc() {
        return list_proc;
    }

    public void setList_proc(List<String> list_proc) {
        this.list_proc = list_proc;
    }

    public void actualizarProcesos() {

        if (programa != null) {
            String p_c = S_Administracion.obtenerCodigoPrograma(programa);
            list_proc = S_AdminPrograma.procesoPorPrograma(p_c);
            
            Pa1 = true;
            Pa3 = false;
        } else {
            Pa1 = false;

        }

       
    }

    public void actualizarBtn0() {
        if (programa != null && proceso != null) {
            Btn0 = false;
           

        } else {
            Btn0 = true;
            Pa3 = false;
        }

    }

    public void cargarCriterios(ActionEvent actionEvent) {

        String p_c = S_Administracion.obtenerCodigoPrograma(programa);
        criterios = S_AdminPrograma.criteriosByNivel(proceso, p_c, seleccionado);
        
        Pa4 = true;

    }

    public void cargarNiveles(ActionEvent actionEvent) {

        
         L1 = S_Administracion.obtieneMenorNivel(proceso);
        losNiveles = S_Administracion.procesoJerarquias(proceso);
        Num_Niv = losNiveles.length;
        for(int i = Num_Niv;i<4;i++)
            mostrar[i] = false;
        
        Pa3 = true;
    }

    public void cargar_N2() {

        L2.clear();
        L3.clear();
        L3.add(new ArrayList<String>());
        L4.clear();
        L4.add(new ArrayList<String>());
        String id = "";
        for (int i = 0; i < L1.get(0).size(); i++) {
            if (L1.get(0).get(i).equals(Sn1)) {
                id = L1.get(1).get(i);
                i = L1.get(0).size();
            }
        }

       clic = 1;
        activarBoton(id);
        L2 = S_Administracion.obtieneNivelesHijo(proceso, id);

        
    }

    public void cargar_N3() {

        L3.clear();
        L3.add(new ArrayList<String>());
        L4.clear();
        L4.add(new ArrayList<String>());
        String id = "";
        for (int i = 0; i < L2.get(0).size(); i++) {
            if (L2.get(0).get(i).equals(Sn2)) {
                id = L2.get(1).get(i);
                i = L2.get(0).size();
            }
        }

       clic = 2;
        activarBoton(id);
        L3 = S_Administracion.obtieneNivelesHijo(proceso, id);

       
    }

    public void cargar_N4() {

        L4.clear();

        String id = "";
        for (int i = 0; i < L3.get(0).size(); i++) {
            if (L3.get(0).get(i).equals(Sn3)) {
                id = L3.get(1).get(i);
                i = L3.get(0).size();
            }
        }

        clic = 3;
        activarBoton(id);
        L4 = S_Administracion.obtieneNivelesHijo(proceso, id);

        
    }
    
      private void activarBoton(String id) {

        if (clic == Num_Niv) {
            seleccionado = id;
            Btn1 = false;
        } else {
            Btn1 = true;
        }
    }
      
      
      public void cargarUnCriterio(String criterioCod)
      {
          lasFuentes = S_AdminPrograma.fuentesByCriterio(criterioCod);
          criterioSelec = criterioCod;
          Pa5 = true;
      }

    public String getSeleccionado() {
        return seleccionado;
    }

}
