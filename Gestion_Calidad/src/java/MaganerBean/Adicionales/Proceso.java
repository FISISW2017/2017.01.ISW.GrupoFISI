/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MaganerBean.Adicionales;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Richard
 */
public class Proceso {
   
    
    private String nombre;
    private List<String> niveles = new ArrayList<>(); 
    
    public Proceso(ArrayList<String> valores){
        
        nombre = valores.get(0);
        for(int i = 1 ; i<valores.size();i++)
            niveles.add(valores.get(i));
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<String> getNiveles() {
        return niveles;
    }

    public void setNiveles(List<String> niveles) {
        this.niveles = niveles;
    }
    
    

}
