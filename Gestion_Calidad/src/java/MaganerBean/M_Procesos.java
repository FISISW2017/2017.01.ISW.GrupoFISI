/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MaganerBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlPanelGrid;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.graphicimage.GraphicImage;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.panel.Panel;

@ ManagedBean(name ="M_Procesos")
@SessionScoped
@ViewScoped

public class M_Procesos {
    
    private HtmlForm contexto;

    public HtmlForm getContexto() {
        contexto = new HtmlForm();
        contexto.getChildren().add(OpcionesProcesos("PRE GRADO"));
        return contexto;
    }

    public void setContexto(HtmlForm contexto) {
        this.contexto = contexto;
    }
    
    private Panel OpcionesProcesos(String proceso)
    {
        
        Panel principal = new Panel();
        principal.setHeader(proceso);
        
        HtmlPanelGrid panel = new HtmlPanelGrid();
        panel.setColumns(5);
        panel.setCellpadding("3px");

        panel.getChildren().add(opcion("REGISTRAR PROCESO","Imagenes/document.png", "#036fab"));
         panel.getChildren().add(opcion("DEFINIR NIVELES","Imagenes/nivel.png", "#009900"));
            panel.getChildren().add(opcion("DEFINIR CRITERIOS","Imagenes/criterios.png", "#0099cc"));
       
        principal.getChildren().add(panel);
        
        
         return principal;
    
    }
    
    private HtmlPanelGrid opcion(String opcion, String imagen, String color)           
    {
        HtmlPanelGrid op = new HtmlPanelGrid();
         op.setColumns(1);
        op.setStyle("border-style : none; background : "+color+";height: 100px; width :250px; border-radius: 5px 5px 5px 5px");
        
        HtmlPanelGrid aux = new HtmlPanelGrid(); 
       
        aux.setColumns(2);
        
        OutputLabel label = new OutputLabel();
        label.setValue(opcion);
        label.setStyle("opacity: 100%; 7c7c7c; display:  block; width: content-box; float: none; padding: 5px; margin: 2px;"
                + " font-size:  20px; font-weight: normal; color : #ffffff");
        
        GraphicImage ima = new GraphicImage();
        ima.setStyle("height :64px; padding : 8px 0px 5px 15px");
        ima.setName(imagen);
       
        aux.getChildren().add(ima);
        aux.getChildren().add(label);
        
        
        op.getChildren().add(aux);
       
        CommandButton boton = new CommandButton();
        boton.setValue("Ingresar");
        boton.setStyle("background : #ffffff; border-style: none; color : "+color+"; width : 100%; font-weight: bold");
        
        op.getChildren().add(boton);
       
        return op;
    }
    
    
}
