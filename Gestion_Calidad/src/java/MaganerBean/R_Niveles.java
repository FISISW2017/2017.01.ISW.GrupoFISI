/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MaganerBean;

import Servicios.S_Administracion;
import Transformador.T_Niveles;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;
import org.primefaces.component.accordionpanel.AccordionPanel;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.tabview.Tab;
import org.primefaces.context.RequestContext;

@ManagedBean
@ViewScoped
@SessionScoped

public class R_Niveles {

    private AccordionPanel raiz =  new AccordionPanel();
    private CommandButton button;
    private String valor;
    private int i = 0, n = 0, v = -1;
    private int can_n;
    private AccordionPanel apoyo = null;
    private String proceso;

  

   

  
    
    private String respuesta = "";
    
    
    
    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    private String[] colores = new String[]{"Tab1", "Tab2", "Tab3", "Tab4"};

    public AccordionPanel getRaiz() {

        return raiz;
    }

    public void setRaiz(AccordionPanel raiz) {

        this.raiz = raiz;

    }

    public void setButton(CommandButton button) {

        this.button = button;

        this.button.setId("ejem");
        this.button.setUpdate("raiz");
     

    }

    public CommandButton getButton() {

        return button;
    }

    private Tab nodoArbol(String titulo, boolean boton, int n) {
        Tab nodo = new Tab();
        nodo.setTitle(titulo);
        nodo.setTitleStyleClass(colores[n % 4]);
        CommandButton aux = new CommandButton();
        aux.setIcon("id_agregar");

        //aux.setUpdate("form:raiz");
        aux.setId("a" + i);
        
        AccordionPanel ac = new AccordionPanel();
        ac.setActiveIndex("null");
        ac.setId("p" + i);
        aux.setOnclick("PF('dlg').show();");
        i++;
       
        aux.addActionListener(new ActionListener() {
            @Override
            public void processAction(ActionEvent event) {
                //ac.getChildren().add(nodoArbol("a ver ayudita"+(n+1), true,n+1));
               
                recorrerArbol(aux.getId(), 0, raiz);
                apoyo = ac;

            }
        });
        
        System.out.println("N: "+n);
        if(n >= can_n-1 )
        {  
            aux.setDisabled(true);
            aux.setRendered(false);
        }
       
        if (boton) {
            nodo.getChildren().add(aux);
        }

        nodo.getChildren().add(ac);

        return nodo;
    }

    public void help(ActionEvent event) {

        v = 0;

    }

    private void recorrerArbol(String id,int valor, AccordionPanel nodo)
    {
     
       if(nodo.getChildCount() >0)
       {
         
           
          for(int i = 0; i< nodo.getChildCount();i++)
          {
              String titulo = nodo.getChildren().get(i).getChildren().get(0).getId();
              if(titulo.equals(id))
              {  
                  v = valor+1;
              }
              else
              {
                 
                  int valor2 = valor+1;
                  AccordionPanel nodo2 = (AccordionPanel) nodo.getChildren().get(i).getChildren().get(1);
                  recorrerArbol(id, valor2, nodo2);
              }
          }
           
       }
      
        
    }
    
    public void processAction(ActionEvent event) {

        RequestContext context = RequestContext.getCurrentInstance();
        if (v <=0) {
            raiz.getChildren().add(nodoArbol(valor, true, 0));
             raiz.setActiveIndex("null");
        } else {
            
         
            apoyo.getChildren().add(nodoArbol(valor, true, v));
           // System.out.println("panel: "+apoyo.getId()+", \t nivel: "+v);
            
        }
        context.addCallbackParam("loggedIn", true);
        
       

    }

    public String getProceso() {
        return proceso;
    }

    public void setProceso(String proceso) {
        this.proceso = proceso;
         can_n = S_Administracion.cantidadJerarquias(proceso);
         T_Niveles.obtenerPanel(proceso);
         if(S_Administracion.existeNivelProceso(proceso)){
                raiz = T_Niveles.getPanel();
         }
    }
    
    public void accionGuardar(ActionEvent actionEvent) {
       
        T_Niveles niveles = new T_Niveles(raiz, proceso);
        respuesta = niveles.generarArbol();
        addMessage(respuesta);
       
    }
        
          public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
          
}

