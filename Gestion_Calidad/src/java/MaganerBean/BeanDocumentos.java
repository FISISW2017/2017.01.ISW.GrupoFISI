/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MaganerBean;

import Servicios.S_AdminPrograma;
import com.dropbox.core.DbxException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.context.PrimeFacesContext;
import org.primefaces.context.PrimeFacesContextFactory;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

@ManagedBean
@SessionScoped
@ViewScoped
public class BeanDocumentos implements Serializable {

    String proceso;
    String programa;
    String criterioId;
    String criterioNomb;
    String fuenteId;
    String fuenteNomb;
    
    String[] elegido;
    
    List<String[]> Documentos;
    
    boolean validado = false;
        
    private UploadedFile file;
    File f;

    private StreamedContent fileDown;

    public String[] getElegido() {
        return elegido;
    }

    public void setElegido(String[] elegido) {
        this.elegido = elegido;
    }

    
    
    public StreamedContent getFileDown() {
        return fileDown;
    }

    public void setFileDown(StreamedContent fileDown) {
        this.fileDown = fileDown;
    }
    
    public String getCriterioNomb() {
        return criterioNomb;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public List<String[]> getDocumentos() {
        return Documentos;
    }

    public void setDocumentos(List<String[]> Documentos) {
        this.Documentos = Documentos;
    }

    public boolean isValidado() {
        return validado;
    }

    public void setValidado(boolean validado) {
        this.validado = validado;
    }

    public String getProceso() {
        return proceso;
    }

    public void setProceso(String proceso) {
        this.proceso = proceso;
    }

    public String getPrograma() {
        return programa;
    }

    public void setPrograma(String programa) {
        this.programa = programa;
    }

    public String getCriterioId() {
        return criterioId;
    }

    public void setCriterioId(String criterioId) {
        this.criterioId = criterioId;
        criterioNomb = S_AdminPrograma.descripcionCriterio(criterioId);
    }

    public String getFuenteId() {
        return fuenteId;
    }

    public void setFuenteId(String fuenteId) {
        this.fuenteId = fuenteId;
        System.out.println("Fuen: "+fuenteId);
        Documentos = S_AdminPrograma.documentosByFuente(fuenteId);
        System.out.println("Tam: "+Documentos.size());
        validado = S_AdminPrograma.estaValidadaFuente(fuenteId);
    }

    public String getFuenteNomb() {
        return fuenteNomb;
    }

    public void setFuenteNomb(String fuenteNomb) {
        this.fuenteNomb = fuenteNomb;
    }

    public void upload() throws IOException {
        if (file != null) {
            /*f = new File(file.getFileName());
            System.out.print(f.getAbsolutePath());
            Path O = Paths.get(f.getPath());
            Path De = Paths.get(file.getFileName());
            
            Files.copy(O, De, StandardCopyOption.REPLACE_EXISTING);*/
            String file_name = file.getFileName();
            String url = "C:/Users/marisol/Desktop/" + file_name;
            FileInputStream in = (FileInputStream) file.getInputstream();
            FileOutputStream ou = new FileOutputStream(url);

            byte[] buffer = new byte[(int) file.getSize()];
            int contador = 0;

            while ((contador = in.read(buffer)) != -1) {
                ou.write(buffer, 0, contador);
            }
            in.close();
            ou.close();
            Date date = new Date();
            try {
                S_AdminPrograma.subirDocumento(url, file_name, date, fuenteId);
            } catch (DbxException ex) {
                Logger.getLogger(BeanDocumentos.class.getName()).log(Level.SEVERE, null, ex);
            }
            FacesMessage message = new FacesMessage("Succesful", file.getFileName());
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        Documentos = S_AdminPrograma.documentosByFuente(fuenteId);
    }
    
    public void cargarFileDownload(SelectEvent event) throws FileNotFoundException, IOException, DbxException{
        
        String docId = ((String []) event.getObject())[0];
        System.out.println(docId);
        String nombre = S_AdminPrograma.descargarDocumento(docId);
        
        InputStream stream = new FileInputStream("C:\\Users\\marisol\\Desktop\\PROYECTO SW\\ULTIMO PROYECTO SOFTWARE CALIDAD\\Gestion_Calidad"+nombre);
        System.out.print(stream);
        fileDown = new DefaultStreamedContent(stream,"txt/jpg/png/pdf",nombre);
    }
    
    public void guardarValidacion(ActionEvent actionEvent){
        if(validado) S_AdminPrograma.validarFuente(fuenteId);
    }
    
    
    
}
