/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MaganerBean;

import Servicios.S_Administracion;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "verPrograma")
@ViewScoped
@SessionScoped
public class BeanProgramas {

    private List<String[]> listaProgramas;
    private List<String> facultades;

    private String n_fac, n_nom, n_cod;

    @PostConstruct
    public void init() {
        listaProgramas = S_Administracion.todosProgramas();
        facultades = S_Administracion.todasFacultades();
     

        
    }

    public List<String[]> getListaProgramas() {
        return listaProgramas;
    }

    public void setListaProgramas(List<String[]> listaProgramas) {
        this.listaProgramas = listaProgramas;
    }

    public List<String> getFacultades() {
        return facultades;
    }

    public void setFacultades(List<String> facultades) {
        this.facultades = facultades;
    }

    public String getN_fac() {
        return n_fac;
    }

    public void setN_fac(String n_fac) {
        this.n_fac = n_fac;
    }

    public String getN_nom() {
        return n_nom;
    }

    public void setN_nom(String n_nom) {
        this.n_nom = n_nom;
    }

    public String getN_cod() {
        return n_cod;
    }

    public void setN_cod(String n_cod) {
        this.n_cod = n_cod;
    }

    public void accionRegistrar(ActionEvent actionEvent) {

        RequestContext context = RequestContext.getCurrentInstance();
        
        if(S_Administracion.nuevoPrograma(n_fac, n_nom, n_cod)){
            
        }
        else{
            
        }
        listaProgramas = S_Administracion.todosProgramas();      
        n_cod = "";
        n_nom = "";
      
        
        context.addCallbackParam("loggedIn", true);
    }

}
