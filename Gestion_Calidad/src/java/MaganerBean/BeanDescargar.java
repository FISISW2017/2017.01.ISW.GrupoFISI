/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MaganerBean;

import Servicios.S_AdminPrograma;
import com.dropbox.core.DbxException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

@ManagedBean
public class BeanDescargar {
    
       private StreamedContent file;
     
    public BeanDescargar() throws FileNotFoundException, IOException, DbxException {
        
        String docId = "P0100100101001";
        String nombre = S_AdminPrograma.descargarDocumento(docId);
        
        InputStream stream = new FileInputStream("C:\\Users\\marisol\\Desktop\\PROYECTO SW\\ULTIMO PROYECTO SOFTWARE CALIDAD\\Gestion_Calidad"+nombre);
        System.out.print(stream);
        file = new DefaultStreamedContent(stream,"txt/jpg/png/pdf",nombre);
    }
 
    public StreamedContent getFile() {
        return file;
    }
    
}
