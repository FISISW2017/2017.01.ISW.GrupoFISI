/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MaganerBean;

import Servicios.S_AdminPrograma;
import Servicios.S_Administracion;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.primefaces.context.RequestContext;


@ManagedBean
@SessionScoped
@ViewScoped

public class BeanProcesos {
    
    private List<String> listaProcesos;
    
    private String codigo_fac;
    private String facultad;
    private String programa;
    
    private boolean v_a, c_o;

     private String seleccionado;
    private List<String> lista;

    
  
    
    
    public boolean isV_a() {
        return v_a;
    }

    public void setV_a(boolean v_a) {
        this.v_a = v_a;
    }

    public boolean isC_o() {
        return c_o;
    }

    public void setC_o(boolean c_o) {
        this.c_o = c_o;
    }
    
    
    
   
    public String getSeleccionado() {
        return seleccionado;
    }

    public void setSeleccionado(String seleccionado) {
        this.seleccionado = seleccionado;
    }

    public List<String> getLista() {
        return lista;
    }

 
    
    public String getCodigo_fac() {
        return codigo_fac;
    }

    public void setCodigo_fac(String codigo_fac) {
        this.codigo_fac = codigo_fac;
        listaProcesos = S_AdminPrograma.procesoPorPrograma(codigo_fac);
        lista = S_AdminPrograma.procesosPosiblesAPrograma(codigo_fac);
    }

    public String getFacultad() {
        return facultad;
    }

    public void setFacultad(String facultad) {
        this.facultad = facultad;
    }

    public String getPrograma() {
        return programa;
    }

    public void setPrograma(String programa) {
        this.programa = programa;
         
    }

    public List<String> getListaProcesos() {
        return listaProcesos;
    }

  
      public void AsociarUnProceso(ActionEvent actionEvent) {
      
       RequestContext context = RequestContext.getCurrentInstance();
       
       S_AdminPrograma.nuevaAsociacion( programa, seleccionado ,"Aperturado","Iniciado", c_o, v_a);
       lista = S_AdminPrograma.procesosPosiblesAPrograma(codigo_fac);
       listaProcesos = S_AdminPrograma.procesoPorPrograma(codigo_fac);
       
       
      
       context.addCallbackParam("loggedIn", true);
        
    }
    
    
            
   
    
            
}
