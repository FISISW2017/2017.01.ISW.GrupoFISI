/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MaganerBean;

import Servicios.S_AdminPrograma;
import Servicios.S_Administracion;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.component.panel.Panel;
import org.primefaces.context.RequestContext;

@ManagedBean
@ViewScoped
@SessionScoped
public class R_Criterios {

    //private List<String> N1, N2, N3, N4;
    private ArrayList<ArrayList<String>> N1, N2, N3, N4;
    private String select_1, select_2, select_3, select_4;

    private String proceso;
    private String facultad, codigo_prog, nom_prog;

    private int val, clic;

    private boolean[] mostrar = new boolean[4];

    private ArrayList<String[]> fuentes = new ArrayList<>();
private ArrayList<String[]> fuentes_guarda = new ArrayList<>();
    private List<String[]> criterios;

    private String[] losNiveles = {"","","",""};
    
    private String contenido;

    private String nom_v, tipo_v, est_v;
    private boolean estado_v,estado_p1;

    private boolean desactivo;

    private String selecionado;
    
    private String id_criterio;

    private String titulo, boton;
    private boolean estado_panel;

    
    
    
    
    
    @PostConstruct
    public void init() {
        clic = 0;
        desactivo = true;
        id_criterio = "";
        
        estado_panel = false;
        estado_p1 = false;
        
     
        
      
        
    }

    public boolean isEstado_p1() {
        return estado_p1;
    }

    public void setEstado_p1(boolean estado_p1) {
        this.estado_p1 = estado_p1;
    }

    
    public String[] getLosNiveles() {
        return losNiveles;
    }

    public void setLosNiveles(String[] losNiveles) {
        this.losNiveles = losNiveles;
    }

    
    
    
    
    public String getTitulo() {
        return titulo;
    }

    public String getBoton() {
        return boton;
    }

    
    
    private void activarBoton(String id) {

        if (clic == val) {
            selecionado = id;
            desactivo = false;
        } else {
            desactivo = true;
        }
    }
    
    public boolean isEstado_panel() {
        return estado_panel;
    }

    public void setEstado_panel(boolean estado_panel) {
        this.estado_panel = estado_panel;
    }

    public boolean isDesactivo() {
        return desactivo;
    }

    public void setDesactivo(boolean desactivo) {
        this.desactivo = desactivo;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getNom_v() {
        return nom_v;
    }

    public String getTipo_v() {
        return tipo_v;
    }

    public List<String[]> getFuentes() {
        return fuentes;
    }

    public List<String[]> getCriterios() {
        return criterios;
    }

    public void setCriterios(List<String[]> criterios) {
        this.criterios = criterios;
    }

    public String getEst_v() {
        return est_v;
    }

    public void setEst_v(String est_v) {
        this.est_v = est_v;
    }

    public void setNom_v(String nom_v) {
        this.nom_v = nom_v;
    }

    public void setTipo_v(String tipo_v) {
        this.tipo_v = tipo_v;
    }

    public boolean isEstado_v() {
        return estado_v;
    }

    public void setEstado_v(boolean estado_v) {
        this.estado_v = estado_v;
        if (this.estado_v) {
            est_v = "Y";
        } else {
            est_v = "N";
        }

    }

    public boolean[] getMostrar() {
        return mostrar;
    }

    public void setMostrar(boolean[] mostrar) {
        this.mostrar = mostrar;
    }

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }

    private void datos_N1() {
        //Codigo de conexion con la capa inferior

        /*for (int i = 0; i < 10; i++) {
            N1.add("Dimension " + (i + 1));
        }*/
        N1 = S_Administracion.obtieneMenorNivel(proceso);
    }

    public List<String> getN1() {
        return N1.get(0);
    }

    public List<String> getN2() {
        return N2.get(0);
    }

    public List<String> getN3() {
        return N3.get(0);
    }

    public List<String> getN4() {
        return N4.get(0);
    }

    public String getSelect_1() {
        return select_1;
    }

    public void setSelect_1(String select_1) {
        this.select_1 = select_1;
    }

    public String getSelect_2() {
        return select_2;
    }

    public void setSelect_2(String select_2) {
        this.select_2 = select_2;
    }

    public String getSelect_3() {
        return select_3;
    }

    public void setSelect_3(String select_3) {
        this.select_3 = select_3;
    }

    public String getSelect_4() {
        return select_4;
    }

    public void setSelect_4(String select_4) {
        this.select_4 = select_4;
    }

    public void cargar_N2() {

        N2.clear();
        N3.clear();
        N3.add(new ArrayList<String>());
        N4.clear();
        N4.add(new ArrayList<String>());
        String id = "";
        for (int i = 0; i < N1.get(0).size(); i++) {
            if (N1.get(0).get(i).equals(select_1)) {
                id = N1.get(1).get(i);
                i = N1.get(0).size();
            }
        }

        clic = 1;
        activarBoton(id);
        N2 = S_Administracion.obtieneNivelesHijo(proceso, id);

        //N2.add(select_1);
    }

    public void cargar_N3() {

        N3.clear();
        N3.add(new ArrayList<String>());
        N4.clear();
        N4.add(new ArrayList<String>());
        String id = "";
        for (int i = 0; i < N2.get(0).size(); i++) {
            if (N2.get(0).get(i).equals(select_2)) {
                id = N2.get(1).get(i);
                i = N2.get(0).size();
            }
        }

        clic = 2;
        activarBoton(id);
        N3 = S_Administracion.obtieneNivelesHijo(proceso, id);

        //N3.add(select_2);
    }

    public void cargar_N4() {

        N4.clear();

        String id = "";
        for (int i = 0; i < N3.get(0).size(); i++) {
            if (N3.get(0).get(i).equals(select_3)) {
                id = N3.get(1).get(i);
                i = N3.get(0).size();
            }
        }

        clic = 3;
        activarBoton(id);
        N4 = S_Administracion.obtieneNivelesHijo(proceso, id);

        //N4.add(select_3);
    }

    public void buttonAction(ActionEvent actionEvent) {

        RequestContext context = RequestContext.getCurrentInstance();

        String[] valor = new String[4];
        valor[0] ="";
        valor[1] = nom_v;
        valor[2] = tipo_v;
        valor[3] = est_v;

        fuentes.add(valor);
        fuentes_guarda.add(valor);
        context.addCallbackParam("loggedIn", true);

    }

    public void limpiarRegistro(ActionEvent actionEvent) {

        nom_v = "";
        tipo_v = "";

    }

    public void registrarCriterio(ActionEvent actionEvent) {

        
        S_AdminPrograma.guardarCriterio(id_criterio, proceso, codigo_prog, selecionado, contenido, fuentes_guarda);
        criterios = S_AdminPrograma.criteriosByNivel(proceso, codigo_prog, selecionado);
        contenido = "";
        fuentes.clear();
        fuentes_guarda.clear();
        estado_panel = false;
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public String getProceso() {
        return proceso;
    }

    public void setProceso(String proceso) {

        this.proceso = proceso;
           String[] apoyo = S_Administracion.procesoJerarquias(proceso);
        for(int i = 0; i < apoyo.length;i++)
            losNiveles[i] = apoyo[i];
        val = S_Administracion.cantidadJerarquias(proceso);
        N1 = new ArrayList<>();
        N2 = new ArrayList<>();
        N2.add(new ArrayList<>());
        N3 = new ArrayList<>();
        N3.add(new ArrayList<>());
        N4 = new ArrayList<>();
        N4.add(new ArrayList<>());

        datos_N1();

        for (int i = 0; i < mostrar.length; i++) {
            mostrar[i] = false;
        }

        for (int i = 0; i < val; i++) {
            mostrar[i] = true;
        }
    }

    public String getFacultad() {
        return facultad;
    }

    public void setFacultad(String facultad) {
        this.facultad = facultad;
    }

    public String getCodigo_prog() {
        return codigo_prog;
    }

    public void setCodigo_prog(String codigo_prog) {

        this.codigo_prog = codigo_prog;
    }

    public String getNom_prog() {
        return nom_prog;
    }

    public void setNom_prog(String nom_prog) {
        this.nom_prog = nom_prog;
    }

    public void cargarCriterios(ActionEvent actionEvent) {
        id_criterio = "";
        criterios = S_AdminPrograma.criteriosByNivel(proceso, codigo_prog, selecionado);
       estado_panel = false;
       estado_p1 = true;

    }
    
     public void nuevoCriterio(ActionEvent actionEvent) {
        id_criterio = "";
        estado_panel = true;
        fuentes_guarda.clear();
        titulo ="REGISTRAR CRITERIO";
        boton ="Registrar";
        contenido = "";
        fuentes.clear();
        System.out.print(titulo+"  "+estado_panel);

    }
    
    
    
    public void cargarUnCriterio(String valor)
    {
        //System.out.println("fsdjkjklsdjksslkfj");
        estado_panel = true;
        titulo ="MODIFICAR CRITERIO";
        boton ="Guardar";
         System.out.println(valor);
        contenido = S_AdminPrograma.descripcionCriterio(valor);
        fuentes = S_AdminPrograma.nombreFuentesByCriterio(valor);
        id_criterio = valor;
    }

   
    


}
