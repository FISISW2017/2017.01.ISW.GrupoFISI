/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Transformador;

import Auxiliares.Arbol;
import Controlador.C_Proceso;
import MaganerBean.R_Niveles;
import Servicios.S_Arbol;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import org.primefaces.component.accordionpanel.AccordionPanel;
import org.primefaces.component.tabview.Tab;

/**
 *
 * @author Fabio
 */
public class T_Niveles {

    private static AccordionPanel panel;
    String proceso;
    private ArrayList<DefaultMutableTreeNode> Nodos;
    private ArrayList<Integer> padres;
    private ArrayList<Integer> secuencia;
    int secRaiz;
    private static String[] colores = new String[]{"Tab1", "Tab2", "Tab3", "Tab4"};

    public T_Niveles(AccordionPanel panel, String proceso) {
        this.panel = panel;
        this.Nodos = new ArrayList<>();
        this.padres = new ArrayList<>();
        this.secuencia = new ArrayList<>();
        this.secRaiz = -1;
        this.proceso = proceso;
    }

    public String generarArbol() {
        int padre = -1;
        generarListas(panel, padre);
        JTree arb = generarArbolJava();
        String respuesta = S_Arbol.guardarTree(arb, proceso);
        
//mostrarArbol(arb);
        return respuesta;
    }

    private void generarListas(AccordionPanel nodo, int padre) {

        if (nodo.getChildCount() > 0) {

            for (int i = 0; i < nodo.getChildCount(); i++) {
                String titulo = ((Tab) nodo.getChildren().get(i)).getTitle();
                DefaultMutableTreeNode nodito = new DefaultMutableTreeNode(titulo);
                Nodos.add(nodito);
                padres.add(padre);
                secuencia.add(-1);
                int subpadre = Nodos.size() - 1;
                AccordionPanel nodo2 = (AccordionPanel) nodo.getChildren().get(i).getChildren().get(1);
                generarListas(nodo2, subpadre);

            }

        }

    }

    public JTree generarArbolJava() {
        DefaultMutableTreeNode raizJava = new DefaultMutableTreeNode(proceso);
        DefaultTreeModel modelo = new DefaultTreeModel(raizJava);
        JTree tree = new JTree(modelo);
        for (int j = 0; j < Nodos.size(); j++) {
            int pos_padre = padres.get(j);
            if (padres.get(j) == -1) {
                secRaiz = secRaiz + 1;
                modelo.insertNodeInto(Nodos.get(j), raizJava, secRaiz);
            } else {

                int sec = secuencia.get(pos_padre);
                sec = sec + 1;
                secuencia.set(pos_padre, sec);
                modelo.insertNodeInto(Nodos.get(j), Nodos.get(pos_padre), sec);
            }

        }
        return tree;
    }

    /* public void mostrarArbol(JTree tree) {

        TreeNode raiz2 = (TreeNode) tree.getModel().getRoot();
        try {
            imprimirNodo(raiz2);
        } catch (IOException ex) {
            Logger.getLogger(R_Niveles.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void imprimirNodo(TreeNode nodo) throws IOException {

        

        if (nodo.getChildCount() >= 0) {
            for (Enumeration e = nodo.children(); e.hasMoreElements();) {
                TreeNode n = (TreeNode) e.nextElement();
                imprimirNodo(n);
            }
        }
    }*/
    public static void obtenerPanel(String proces_name) {

        String procesId = C_Proceso.processByName(proces_name).getProcesoId();
        JTree arbol = Arbol.obtieneArbol(procesId);
        panel = new AccordionPanel();
        if (arbol != null) {
            int i = -1;
            TreeNode raiz = (TreeNode) arbol.getModel().getRoot();
            if (raiz.getChildCount() > 0) {
                generaTab(raiz, i);
            }
        }

    }

    public static Tab generaTab(TreeNode nodo, int n) {

        int val = n;
        Tab nodoTab = new Tab();
        nodoTab.setTitle(nodo.toString());
        if(n > -1)
              nodoTab.setTitleStyleClass(colores[n % 4]);
        
        System.out.println("Valor de:  "+nodoTab.getTitle()+ " : " + n );
        AccordionPanel ac = new AccordionPanel();

        if (nodo.getChildCount() >= 0) {
            val++;
            for (Enumeration e = nodo.children(); e.hasMoreElements();) {
                TreeNode newNode = (TreeNode) e.nextElement();
                if (n < 0) {

                    panel.getChildren().add(generaTab(newNode, val));
                } else {
                    
                    
                    ac.getChildren().add(generaTab(newNode, val));
                }
                nodoTab.getChildren().add(ac);
            }

        }

        return nodoTab;
    }

    public static AccordionPanel getPanel() {
        return panel;
    }

   

}
