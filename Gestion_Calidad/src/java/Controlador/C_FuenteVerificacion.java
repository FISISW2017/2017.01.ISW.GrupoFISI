/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conector.Conexion;
import Modelo.Criterio;
import Modelo.FuenteVerificacion;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author  
 */
public class C_FuenteVerificacion {

    public static void nuevo(FuenteVerificacion nuevo) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();

        session.save(nuevo);

        tx.commit();

        session.close();
    }

    public static void modificar(FuenteVerificacion fuente) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        session.update(fuente);
        tx.commit();
        session.close();
    }

    public static List<FuenteVerificacion> fuentesByCriterio(String criterioId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<FuenteVerificacion> apoyo = session.createQuery("from FuenteVerificacion where criterio.criterioId = '" + criterioId + "' ").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo;
        } else {
            return null;
        }

    }

    public static int totalFuentesByCriterio(String criterioId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<FuenteVerificacion> apoyo = session.createQuery("from FuenteVerificacion where criterio.criterioId = '" + criterioId + "' ").list();
        tx.commit();
        session.close();
        return apoyo.size();

    }

    public static int totalFuentesObligaByCriterio(String criterioId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<FuenteVerificacion> apoyo = session.createQuery("from FuenteVerificacion where criterio.criterioId = '" + criterioId + "' and fuenteObligatoria = 1").list();
        tx.commit();
        session.close();
        return apoyo.size();

    }

    public static int totalFuentesObligIngresadasByCriterio(String criterioId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<FuenteVerificacion> apoyo = session.createQuery("from FuenteVerificacion A where criterio.criterioId = '" + criterioId + "' and fuenteObligatoria = 1 "
                + "and exists (select 'X' from Documento B where B.fuenteVerificacion.fuenteId = A.fuenteId)").list();
        tx.commit();
        session.close();

        return apoyo.size();

    }

    public static int totalFuentesIngresadasByCriterio(String criterioId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<FuenteVerificacion> apoyo = session.createQuery("from FuenteVerificacion A where criterio.criterioId = '" + criterioId + "' "
                + "and exists (select 'X' from Documento B where B.fuenteVerificacion.fuenteId = A.fuenteId)").list();
        tx.commit();
        session.close();

        return apoyo.size();

    }
    
    
    
    public static int totalFuentesObligValidadasByCriterio(String criterioId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<FuenteVerificacion> apoyo = session.createQuery("from FuenteVerificacion where criterio.criterioId = '" + criterioId + "' and fuenteObligatoria = 1 "
                + " and estadoValidacion.estadoValidShort = 'VAL' ").list();
        tx.commit();
        session.close();

        return apoyo.size();

    }

    public static int totalFuentesValidadasByCriterio(String criterioId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<FuenteVerificacion> apoyo = session.createQuery("from FuenteVerificacion where criterio.criterioId = '" + criterioId + "' "
                + " and estadoValidacion.estadoValidShort = 'VAL' ").list();
        tx.commit();
        session.close();

        return apoyo.size();

    }

    public static FuenteVerificacion fuenteById(String fuenteId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<FuenteVerificacion> apoyo = session.createQuery("from FuenteVerificacion where fuenteId = '" + fuenteId + "' ").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo.get(0);
        } else {
            return null;
        }

    }

    public static boolean esFuenteValidada(String fuenteId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<FuenteVerificacion> apoyo = session.createQuery("from FuenteVerificacion where fuenteId = '" + fuenteId + "' ").list();
        tx.commit();
        session.close();
        String stat = apoyo.get(0).getEstadoValidacion().getEstadoValidId();
        return "EV03".equals(stat);

    }

    public static String generarId(String criterioId) {
        String id;
        id = criterioId;
        if (fuentesByCriterio(criterioId) == null) {
            id += "01";
        } else {
            id += siguienteId(criterioId);
        }
        return id;
    }

    public static String siguienteId(String criterioId) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<FuenteVerificacion> apoyo = session.createQuery("from FuenteVerificacion A where A.criterio.criterioId = '" + criterioId
                + "' and A.fuenteId = (Select max(B.fuenteId) from FuenteVerificacion B where B.criterio.criterioId = A.criterio.criterioId)").list();
        tx.commit();
        session.close();
        String fuenteId = apoyo.get(0).getFuenteId();
        int maxId = Integer.parseInt(fuenteId.substring(9));
        maxId++;
        String siguiente = "";
        if (maxId < 10) {
            siguiente += "0" + maxId;
        } else {
            siguiente += maxId;
        }
        return siguiente;
    }
}
