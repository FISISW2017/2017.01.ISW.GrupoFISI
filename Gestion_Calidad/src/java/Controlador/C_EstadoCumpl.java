/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conector.Conexion;
import Modelo.EstadoCumpl;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Fabio
 */
public class C_EstadoCumpl {

    public static List<EstadoCumpl> todosEstadosCumpl() {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<EstadoCumpl> apoyo = session.createQuery("from EstadoCumpl").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo;
        } else {
            return null;
        }

    }

    public static EstadoCumpl recuperarEstadoId(String estado_long) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<EstadoCumpl> apoyo = session.createQuery("from EstadoCumpl where estadoCumplLong = '" + estado_long + "'").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo.get(0);
        } else {
            return null;
        }

    }

    public static EstadoCumpl recuperarEstadoDescr(String estadoId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<EstadoCumpl> apoyo = session.createQuery("from EstadoCumpl where estadoCumplId = '" + estadoId + "'").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo.get(0);
        } else {
            return null;
        }

    }

}
