/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conector.Conexion;
import Modelo.Facultad;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Fabio
 */
public class C_Facultad {

    public static Facultad facultadById(String facultadId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Facultad> apoyo = session.createQuery("from Facultad where "
                + "facultadId = '" + facultadId + "'").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo.get(0);
        } else {
            return null;
        }

    }

    public static Facultad facultadByNombre(String facultadNombre) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Facultad> apoyo = session.createQuery("from Facultad where "
                + "facultadNombre = '" + facultadNombre + "'").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo.get(0);
        } else {
            return null;
        }

    }

    public static List<Facultad> todasFacultades() {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Facultad> apoyo = session.createQuery("from Facultad").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo;
        } else {
            return null;
        }

    }

    public static void nuevo(Facultad nuevo) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();

        session.save(nuevo);

        tx.commit();

        session.close();
    }

    public static void modificar(Facultad facultad) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        session.update(facultad);
        tx.commit();
        session.close();
    }
}
