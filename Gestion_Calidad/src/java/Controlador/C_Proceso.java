/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conector.Conexion;
import Modelo.Proceso;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Fabio
 */
public class C_Proceso {

    public static Proceso obtenerProceso(String procesoId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Proceso> apoyo = session.createQuery("from Proceso where "
                + "procesoId = '" + procesoId + "'").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo.get(0);
        } else {
            return null;
        }

    }

    public static Proceso processByName(String process_name) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Proceso> apoyo = session.createQuery("from Proceso where "
                + "procesoNombre = '" + process_name + "'").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo.get(0);
        } else {
            return null;
        }

    }

    public static List<Proceso> todosProcesos() {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Proceso> apoyo = session.createQuery("from Proceso").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo;
        } else {
            return null;
        }

    }

    public static void nuevo(Proceso nuevo) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        session.save(nuevo);

        tx.commit();

        session.close();
    }

    public static void modificar(Proceso proceso) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        session.update(proceso);
        tx.commit();
        session.close();
    }

    public static String generarId() {
        String id;
        if (todosProcesos() == null) {
            id = "P01";
        } else {
            id = siguienteId();
        }
        return id;
    }

    public static String siguienteId() {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Proceso> apoyo = session.createQuery("from Proceso A where A.procesoId = (Select Max(B.procesoId) from Proceso B)").list();
        tx.commit();
        session.close();
        String procesoId = apoyo.get(0).getProcesoId();
        int maxId = Integer.parseInt(procesoId.substring(1));
        maxId++;
        String siguiente = "P";
        if (maxId < 10) {
            siguiente += "0" + maxId;
        } else {
            siguiente += maxId;
        }
        return siguiente;
    }
    
    public static List<Proceso> procesoParaPrograma(String programaId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Proceso> apoyo = session.createQuery("from Proceso A where not exists (select 'X' from ProcesoPrograma B where"
                + " B.id.programaId = '" + programaId + "' and A.procesoId = B.id.procesoId )").list();
        tx.commit();
        session.close();

        if (apoyo.size() > 0) {
            return apoyo;
        } else {
            return null;
        }

    }
}
