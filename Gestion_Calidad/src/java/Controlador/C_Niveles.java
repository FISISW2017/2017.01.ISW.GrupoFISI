/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conector.Conexion;
import Modelo.Niveles;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Fabio
 */
public class C_Niveles {

    public static Niveles obtenerNivel(String nivelId, String procesoId, int jerarquiaId) {

        SessionFactory sesion = Conexion.getSessionFactory();

        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Niveles> apoyo = session.createQuery("from Niveles A where "
                + "A.id.nivelId = '" + nivelId + "' and A.id.procesoId = '" + procesoId
                + "' and A.id.jerarquiaId = '" + jerarquiaId + "'").list();
        tx.commit();
        session.close();

        if (apoyo.size() > 0) {

            return apoyo.get(0);
        } else {
            return null;
        }

    }

    public static List<Niveles> nivelesJerarquia(String procesoId, int jerarquiaId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Niveles> apoyo = session.createQuery("from Niveles A where "
                + " A.id.procesoId = '" + procesoId
                + "' and A.id.jerarquiaId = '" + jerarquiaId + "'").list();
        tx.commit();
        session.close();

        if (apoyo.size() > 0) {

            return apoyo;
        } else {
            return null;
        }

    }

    public static List<Niveles> nivelesProceso(String procesoId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Niveles> apoyo = session.createQuery("from Niveles A where "
                + " A.id.procesoId = '" + procesoId + "'").list();
        tx.commit();
        session.close();

        if (apoyo.size() > 0) {

            return apoyo;
        } else {
            return null;
        }

    }
    
     

    public static boolean existeNivelProceso(String procesoId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Niveles> apoyo = session.createQuery("from Niveles A where "
                + " A.id.procesoId = '" + procesoId + "'").list();
        tx.commit();
        session.close();

        return apoyo.size() > 0;

    }

    public static List<Niveles> nivelesMenoresProceso(String procesoId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Niveles> apoyo = session.createQuery("from Niveles A where "
                + " A.id.procesoId = '" + procesoId + "' and A.id.jerarquiaId = (Select min(B.id.jerarquiaId) from Jerarquia B where B.id.procesoId = '"
                + procesoId + "')").list();
        tx.commit();
        session.close();

        if (apoyo.size() > 0) {

            return apoyo;
        } else {
            return null;
        }

    }

    public static List<Niveles> obtenerNivelesHijo(String procesoId, String nivelId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Niveles> apoyo = session.createQuery("from Niveles A where "
                + " A.id.procesoId = '" + procesoId + "' and A.id.nivelId in (Select B.id.idInf from NivelInferior B "
                + "where B.niveles.id.procesoId = A.id.procesoId and B.id.idSup = '" + nivelId + "')").list();
        tx.commit();
        session.close();

        if (apoyo.size() > 0) {

            return apoyo;
        } else {
            return null;
        }

    }

    public static void nuevo(Niveles nuevo) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();

        session.save(nuevo);

        tx.commit();

        session.close();
    }

    public static void modificar(Niveles nivel) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        session.update(nivel);
        tx.commit();
        session.close();
    }

    public static void borrarNivelProceso(String procesoId) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Niveles> nivel = nivelesProceso(procesoId);
        for (int i = 0; i < nivel.size(); i++) {
            Niveles niv = nivel.get(i);
            session.delete(niv);
        }

        tx.commit();
        session.close();
    }

}
