/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conector.Conexion;
import Modelo.ProcesoPrograma;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author  
 */
public class C_ProcesoPrograma {

    public static void nuevo(ProcesoPrograma nuevo) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();

        session.save(nuevo);

        tx.commit();

        session.close();
    }

    public static void modificar(ProcesoPrograma proc_prog) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        session.update(proc_prog);
        tx.commit();
        session.close();
    }

    public static ProcesoPrograma obtieneProgramaProceso(String procesoId, String programaId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<ProcesoPrograma> apoyo = session.createQuery("from ProcesoPrograma where"
                + " id.procesoId = '" + procesoId + "' and id.programaId = '" + programaId + "'").list();
        tx.commit();
        session.close();

        if (apoyo.size() > 0) {
            return apoyo.get(0);
        } else {
            return null;
        }

    }

    public static List<ProcesoPrograma> procesoPorPrograma(String programaId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<ProcesoPrograma> apoyo = session.createQuery("from ProcesoPrograma where"
                + " id.programaId = '" + programaId + "'").list();
        tx.commit();
        session.close();

        if (apoyo.size() > 0) {
            return apoyo;
        } else {
            return null;
        }

    }
    
    

    public static boolean existeProgramaProceso(String procesoId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<ProcesoPrograma> apoyo = session.createQuery("from ProcesoPrograma where"
                + " id.procesoId = '" + procesoId + "'").list();
        tx.commit();
        session.close();

        return apoyo.size() > 0;

    }

}
