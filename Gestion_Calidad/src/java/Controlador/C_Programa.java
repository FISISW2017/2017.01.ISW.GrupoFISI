/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conector.Conexion;
import Modelo.Programa;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Fabio
 */
public class C_Programa {

    public static Programa programaById(String programaId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Programa> apoyo = session.createQuery("from Programa where "
                + "programaId = '" + programaId + "'").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo.get(0);
        } else {
            return null;
        }

    }

    public static Programa programByName(String program_name) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Programa> apoyo = session.createQuery("from Programa where "
                + "programaNombre = '" + program_name + "'").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo.get(0);
        } else {
            return null;
        }

    }

    public static Programa programByCode(String program_code) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Programa> apoyo = session.createQuery("from Programa where "
                + "programaCodigo = '" + program_code + "'").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo.get(0);
        } else {
            return null;
        }

    }
public static boolean existeCodigo(String program_code) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Programa> apoyo = session.createQuery("from Programa where "
                + "programaCodigo = '" + program_code + "'").list();
        tx.commit();
        session.close();
        return apoyo.size() > 0;

    }
    
    public static List<Programa> todosProgramas() {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Programa> apoyo = session.createQuery("from Programa").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo;
        } else {
            return null;
        }

    }

    public static List<Programa> facultadProgramas(String facultadId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Programa> apoyo = session.createQuery("from Programa where id.facultadId = '"
                + facultadId + "'").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo;
        } else {
            return null;
        }

    }

    public static void nuevo(Programa nuevo) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();

        session.save(nuevo);

        tx.commit();

        session.close();
    }

    public static void modificar(Programa programa) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        session.update(programa);
        tx.commit();
        session.close();
    }

    public static String generarId() {
        String id;
        if (todosProgramas() == null) {
            id = "001";
        } else {
            id = siguienteId();
        }
        return id;
    }

    public static String siguienteId() {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Programa> apoyo = session.createQuery("from Programa A where A.programaId = (Select Max(B.programaId) from Programa B)").list();
        tx.commit();
        session.close();
        String programaId = apoyo.get(0).getProgramaId();
        int maxId = Integer.parseInt(programaId);
        maxId++;
        String siguiente = "";
        if (maxId < 10) {
            siguiente += "00" + maxId;
        } else if (maxId < 100) {
            siguiente += "0" + maxId;
        } else {
            siguiente += maxId;
        }
        return siguiente;
    }
}
