/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
 /*          FAC_001 - 01/07/17 - inicio             */
 /*++++++++++++++++++++++++++++++++++++++++++++++++++*/
package Controlador;

import Conector.Conexion;
import Modelo.Criterio;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author  
 */
public class C_Criterio {

    public static void nuevo(Criterio nuevo) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();

        session.save(nuevo);

        tx.commit();

        session.close();
    }

    public static void modificar(Criterio criterio) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        session.update(criterio);
        tx.commit();
        session.close();
    }

    public static List<Criterio> todosCriterios(String procesoId, String programId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Criterio> apoyo = session.createQuery("from Criterio where nivProcPrograma.id.proProcesoId = '"
                + procesoId + "' and nivProcPrograma.id.programaId = '" + programId + "' ").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo;
        } else {
            return null;
        }

    }

    public static List<Criterio> CriteriosByNivel(String procesoId, String programId, String nivelId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Criterio> apoyo = session.createQuery("from Criterio where nivProcPrograma.id.proProcesoId = '"
                + procesoId + "' and nivProcPrograma.id.programaId = '" + programId
                + "' and nivProcPrograma.id.nivelId = '" + nivelId + "' ").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo;
        } else {
            return null;
        }

    }

    public static Criterio CriteriosById(String criterioId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Criterio> apoyo = session.createQuery("from Criterio where criterioId = '" + criterioId + "' ").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo.get(0);
        } else {
            return null;
        }

    }

    public static String generarId(String procesoId, String programId) {
        String id;
        id = procesoId + programId;
        if (todosCriterios(procesoId, programId) == null) {
            id += "001";
        } else {
            id += siguienteId(procesoId, programId);
        }
        return id;
    }

    public static String siguienteId(String procesoId, String programId) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Criterio> apoyo = session.createQuery("from Criterio A where A.nivProcPrograma.id.proProcesoId = '" + procesoId
                + "' and A.nivProcPrograma.id.programaId = '" + programId + "' and A.criterioId = (Select Max(B.criterioId) "
                + " from Criterio B where B.nivProcPrograma.id.proProcesoId = A.nivProcPrograma.id.proProcesoId"
                + " and B.nivProcPrograma.id.programaId = A.nivProcPrograma.id.programaId) ").list();
        tx.commit();
        session.close();
        String criterioId = apoyo.get(0).getCriterioId();
        int maxId = Integer.parseInt(criterioId.substring(6));
        maxId++;
        String siguiente = "";
        if (maxId < 10) {
            siguiente += "00" + maxId;
        } else if (maxId < 100) {
            siguiente += "0" + maxId;
        } else {
            siguiente += maxId;
        }
        return siguiente;
    }
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
 /*          FAC_001 - 01/07/17 - fin             */
 /*++++++++++++++++++++++++++++++++++++++++++++++++++*/
