/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conector.Conexion;
import Modelo.Jerarquia;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Fabio
 */
public class C_Jerarquia {

    public static Jerarquia obtenerJerarquia(int jerarquiaId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Jerarquia> apoyo = session.createQuery("from Jerarquia where "
                + "id.jerarquiaId = '" + jerarquiaId + "'").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo.get(0);
        } else {
            return null;
        }

    }

    public static List<Jerarquia> procesoJerarquias(String procesoId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Jerarquia> apoyo = session.createQuery("from Jerarquia where id.procesoId = '"
                + procesoId + "'").list();
        tx.commit();
        session.close();

        if (apoyo.size() > 0) {
            return apoyo;
        } else {
            return null;
        }

    }

    public static int menorJerarquia(String procesoId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Jerarquia> apoyo = session.createQuery("from Jerarquia A where A.id.procesoId = '"
                + procesoId + "' and A.id.jerarquiaId = (select min(B.id.jerarquiaId) from Jerarquia B where B.id.procesoId = A.id.procesoId)").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo.get(0).getId().getJerarquiaId();
        } else {
            return 0;
        }

    }

    public static int mayorJerarquia(String procesoId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Jerarquia> apoyo = session.createQuery("from Jerarquia A where A.id.procesoId = '"
                + procesoId + "' and A.id.jerarquiaId = (select max(B.id.jerarquiaId) from Jerarquia B where B.id.procesoId = A.id.procesoId)").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo.get(0).getId().getJerarquiaId();
        } else {
            return 0;
        }

    }

    public static void nuevo(Jerarquia nuevo) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();

        session.save(nuevo);

        tx.commit();

        session.close();
    }

    public static void modificar(Jerarquia jerarquia) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        session.update(jerarquia);
        tx.commit();
        session.close();
    }

    public static void borrarJerarquiaProceso(String procesoId) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Jerarquia> jerarq = procesoJerarquias(procesoId);
        for (int i = 0; i < jerarq.size(); i++) {
            Jerarquia jer = jerarq.get(i);
            session.delete(jer);
        }

        tx.commit();
        session.close();
    }

}
