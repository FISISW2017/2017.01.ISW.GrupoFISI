/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
 /*          FAC_001 - 01/07/17 - inicio             */
 /*++++++++++++++++++++++++++++++++++++++++++++++++++*/
package Controlador;

import Conector.Conexion;
import Modelo.NivProcPrograma;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Fabio
 */
public class C_NivProcPrograma {

    public static void nuevo(NivProcPrograma nuevo) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();

        session.save(nuevo);

        tx.commit();

        session.close();

    }

    public static NivProcPrograma obtenerNivProcPrograma(String procesoId, String programId, String nivelId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<NivProcPrograma> apoyo = session.createQuery("from NivProcPrograma where id.proProcesoId = '"
                + procesoId + "' and id.programaId = '" + programId + "' and id.nivelId = '" + nivelId + "'").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo.get(0);
        } else {
            return null;
        }

    }
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
 /*          FAC_001 - 01/07/17 - fin             */
 /*++++++++++++++++++++++++++++++++++++++++++++++++++*/
