/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conector.Conexion;
import Modelo.Criterio;
import Modelo.Documento;
import Modelo.FuenteVerificacion;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author  
 */
public class C_Documento {

    public static void nuevo(Documento nuevo) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();

        session.save(nuevo);

        tx.commit();

        session.close();
    }

    public static void modificar(Documento documento) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        session.update(documento);
        tx.commit();
        session.close();
    }

    public static List<Documento> documentosByFuente(String fuenteId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Documento> apoyo = session.createQuery("from Documento where fuenteVerificacion.fuenteId = '" + fuenteId + "' ").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo;
        } else {
            return null;
        }

    }

    public static Documento documentoById(String documentoId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Documento> apoyo = session.createQuery("from Documento where docId = '" + documentoId + "' ").list();
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo.get(0);
        } else {
            return null;
        }

    }

    public static String generarId(String fuenteId) {
        String id;
        id = fuenteId;
        if (documentosByFuente(fuenteId) == null) {
            id += "001";
        } else {
            id += siguienteId(fuenteId);
        }
        return id;
    }

    public static String siguienteId(String fuenteId) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<Documento> apoyo = session.createQuery("from Documento A where A.fuenteVerificacion.fuenteId = '" + fuenteId
                + "' and A.docId = (Select max(B.docId) from Documento B where B.fuenteVerificacion.fuenteId = A.fuenteVerificacion.fuenteId)").list();
        tx.commit();
        session.close();
        String docId = apoyo.get(0).getDocId();
        int maxId = Integer.parseInt(docId.substring(11));
        maxId++;
        String siguiente = "";
          if (maxId < 10) {
            siguiente += "00" + maxId;
        } else if (maxId < 100) {
            siguiente += "0" + maxId;
        } else {
            siguiente += maxId;
        }
        return siguiente;
    }
}
