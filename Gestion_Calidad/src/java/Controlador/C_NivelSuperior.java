/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conector.Conexion;
import Modelo.NivelInferior;
import Modelo.NivelSuperior;
import java.util.List;
import net.sf.ehcache.hibernate.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Fabio
 */
public class C_NivelSuperior {

    public static NivelSuperior obtenerNivelSuperior(String idSup) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<NivelSuperior> apoyo = session.createQuery("from NivelSuperior where idSup = '"
                + idSup + "'").list();
        tx.commit();
        session.close();

        if (apoyo.size() > 0) {

            return apoyo.get(0);
        } else {
            return null;
        }

    }

    public static void nuevo(NivelSuperior nuevo) {
        /* Session session = Conexion.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();*/
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();

        session.save(nuevo);

        tx.commit();

        session.close();
    }

    public static void modificar(NivelSuperior nivel_sup) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        session.update(nivel_sup);
        tx.commit();
        session.close();
    }

    public static List<NivelSuperior> nivelesSuperiorProceso(String procesoId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<NivelSuperior> apoyo = session.createQuery("from NivelSuperior where "
                + " niveles.id.procesoId = '" + procesoId + "'").list();
        tx.commit();
        session.close();

        if (apoyo.size() > 0) {

            return apoyo;
        } else {
            return null;
        }
    }

    public static void borrarSuperiorProceso(String procesoId) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<NivelSuperior> niv_sup = nivelesSuperiorProceso(procesoId);
        for (int i = 0; i < niv_sup.size(); i++) {
            NivelSuperior nSup = niv_sup.get(i);
            session.delete(nSup);
        }

        tx.commit();
        session.close();
    }
}
