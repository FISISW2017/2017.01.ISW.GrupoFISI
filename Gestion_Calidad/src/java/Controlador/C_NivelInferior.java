/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conector.Conexion;
import Modelo.NivelInferior;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Fabio
 */
public class C_NivelInferior {

    public static void nuevo(NivelInferior nuevo) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();

        session.save(nuevo);

        tx.commit();

        session.close();
    }

    public static void modificar(NivelInferior relnivel) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        session.update(relnivel);
        tx.commit();
        session.close();
    }

    public static List<NivelInferior> nivelesInferiorProceso(String procesoId) {

        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<NivelInferior> apoyo = session.createQuery("from NivelInferior where "
                + " niveles.id.procesoId = '" + procesoId + "'").list();
        tx.commit();
        session.close();

        if (apoyo.size() > 0) {

            return apoyo;
        } else {
            return null;
        }
    }

    public static void borrarInferiorProceso(String procesoId) {
        SessionFactory sesion = Conexion.getSessionFactory();
        Session session = sesion.openSession();
        Transaction tx = session.beginTransaction();
        List<NivelInferior> niv_inf = nivelesInferiorProceso(procesoId);
        for (int i = 0; i < niv_inf.size(); i++) {
            NivelInferior nInf = niv_inf.get(i);
            session.delete(nInf);
        }

        tx.commit();
        session.close();
    }
}
