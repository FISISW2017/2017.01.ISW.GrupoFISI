package Modelo;
// Generated 14/07/2017 05:11:44 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * NivelSuperior generated by hbm2java
 */
public class NivelSuperior  implements java.io.Serializable {


     private String idSup;
     private Niveles niveles;
     private Set nivelInferiors = new HashSet(0);

    public NivelSuperior() {
    }

	
    public NivelSuperior(String idSup, Niveles niveles) {
        this.idSup = idSup;
        this.niveles = niveles;
    }
    public NivelSuperior(String idSup, Niveles niveles, Set nivelInferiors) {
       this.idSup = idSup;
       this.niveles = niveles;
       this.nivelInferiors = nivelInferiors;
    }
   
    public String getIdSup() {
        return this.idSup;
    }
    
    public void setIdSup(String idSup) {
        this.idSup = idSup;
    }
    public Niveles getNiveles() {
        return this.niveles;
    }
    
    public void setNiveles(Niveles niveles) {
        this.niveles = niveles;
    }
    public Set getNivelInferiors() {
        return this.nivelInferiors;
    }
    
    public void setNivelInferiors(Set nivelInferiors) {
        this.nivelInferiors = nivelInferiors;
    }




}


