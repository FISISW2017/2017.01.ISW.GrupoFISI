package Modelo;
// Generated 14/07/2017 05:11:44 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * EstadoProcProg generated by hbm2java
 */
public class EstadoProcProg  implements java.io.Serializable {


     private String estadoProcprogId;
     private String estadoProcprogShort;
     private String estadoProcprogLong;
     private String estadoProcprogDescr;
     private Set procesoProgramas = new HashSet(0);

    public EstadoProcProg() {
    }

	
    public EstadoProcProg(String estadoProcprogId, String estadoProcprogShort, String estadoProcprogLong, String estadoProcprogDescr) {
        this.estadoProcprogId = estadoProcprogId;
        this.estadoProcprogShort = estadoProcprogShort;
        this.estadoProcprogLong = estadoProcprogLong;
        this.estadoProcprogDescr = estadoProcprogDescr;
    }
    public EstadoProcProg(String estadoProcprogId, String estadoProcprogShort, String estadoProcprogLong, String estadoProcprogDescr, Set procesoProgramas) {
       this.estadoProcprogId = estadoProcprogId;
       this.estadoProcprogShort = estadoProcprogShort;
       this.estadoProcprogLong = estadoProcprogLong;
       this.estadoProcprogDescr = estadoProcprogDescr;
       this.procesoProgramas = procesoProgramas;
    }
   
    public String getEstadoProcprogId() {
        return this.estadoProcprogId;
    }
    
    public void setEstadoProcprogId(String estadoProcprogId) {
        this.estadoProcprogId = estadoProcprogId;
    }
    public String getEstadoProcprogShort() {
        return this.estadoProcprogShort;
    }
    
    public void setEstadoProcprogShort(String estadoProcprogShort) {
        this.estadoProcprogShort = estadoProcprogShort;
    }
    public String getEstadoProcprogLong() {
        return this.estadoProcprogLong;
    }
    
    public void setEstadoProcprogLong(String estadoProcprogLong) {
        this.estadoProcprogLong = estadoProcprogLong;
    }
    public String getEstadoProcprogDescr() {
        return this.estadoProcprogDescr;
    }
    
    public void setEstadoProcprogDescr(String estadoProcprogDescr) {
        this.estadoProcprogDescr = estadoProcprogDescr;
    }
    public Set getProcesoProgramas() {
        return this.procesoProgramas;
    }
    
    public void setProcesoProgramas(Set procesoProgramas) {
        this.procesoProgramas = procesoProgramas;
    }




}


