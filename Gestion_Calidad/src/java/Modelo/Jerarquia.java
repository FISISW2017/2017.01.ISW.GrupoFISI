package Modelo;
// Generated 14/07/2017 05:11:44 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * Jerarquia generated by hbm2java
 */
public class Jerarquia  implements java.io.Serializable {


     private JerarquiaId id;
     private Proceso proceso;
     private String jerarquiaNombre;
     private Set niveleses = new HashSet(0);

    public Jerarquia() {
    }

	
    public Jerarquia(JerarquiaId id, Proceso proceso, String jerarquiaNombre) {
        this.id = id;
        this.proceso = proceso;
        this.jerarquiaNombre = jerarquiaNombre;
    }
    public Jerarquia(JerarquiaId id, Proceso proceso, String jerarquiaNombre, Set niveleses) {
       this.id = id;
       this.proceso = proceso;
       this.jerarquiaNombre = jerarquiaNombre;
       this.niveleses = niveleses;
    }
   
    public JerarquiaId getId() {
        return this.id;
    }
    
    public void setId(JerarquiaId id) {
        this.id = id;
    }
    public Proceso getProceso() {
        return this.proceso;
    }
    
    public void setProceso(Proceso proceso) {
        this.proceso = proceso;
    }
    public String getJerarquiaNombre() {
        return this.jerarquiaNombre;
    }
    
    public void setJerarquiaNombre(String jerarquiaNombre) {
        this.jerarquiaNombre = jerarquiaNombre;
    }
    public Set getNiveleses() {
        return this.niveleses;
    }
    
    public void setNiveleses(Set niveleses) {
        this.niveleses = niveleses;
    }




}


