/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servicios;

import Controlador.C_EstadoProceso;
import Controlador.C_Facultad;
import Controlador.C_Jerarquia;
import Controlador.C_NivelSuperior;
import Controlador.C_Niveles;
import Controlador.C_Proceso;
import Controlador.C_Programa;
import Controlador.C_NivelInferior;
import Controlador.C_ProcesoPrograma;
import Modelo.EstadoProceso;
import Modelo.Facultad;
import Modelo.Jerarquia;
import Modelo.JerarquiaId;
import Modelo.NivelInferior;
import Modelo.NivelSuperior;
import Modelo.Niveles;
import Modelo.Proceso;
import Modelo.Programa;
import java.util.ArrayList;

import java.util.List;

/**
 *
 * @author Fabio
 */
public class S_Administracion {

    /*++++++++++++++++++++++++++++++++++++++++++++++++*/
 /*++++++       SERVICIOS DE FACULTAD        ++++++*/
 /*++++++++++++++++++++++++++++++++++++++++++++++++*/
    public static Facultad obtenerFacultad(String facultadId) {
        return C_Facultad.facultadById(facultadId);
    }

    public static String obtenerNombreFacultad(String facultadId) {
        return C_Facultad.facultadById(facultadId).getFacultadNombre();
    }

    public static void nuevaFacultad(String facultadId, String fac_name) {
        Facultad nuevo = new Facultad(facultadId, facultadId);
        C_Facultad.nuevo(nuevo);
    }

    public static void modificarFacultad(String facultadId, String fac_name) {
        Facultad facultad = C_Facultad.facultadById(facultadId);
        facultad.setFacultadNombre(fac_name);
        C_Facultad.modificar(facultad);
    }

    public static ArrayList<String> todasFacultades() {
        List<Facultad> facult = C_Facultad.todasFacultades();
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < facult.size(); i++) {
            list.add(facult.get(i).getFacultadNombre());
        }
        return list;
    }

    /*++++++++++++++++++++++++++++++++++++++++++++++++*/
 /*++++++        SERVICIOS DE PROCESO        ++++++*/
 /*++++++++++++++++++++++++++++++++++++++++++++++++*/
    public static Proceso obtenerProceso(String process_name) {
        String procesoId = C_Proceso.processByName(process_name).getProcesoId();
        return C_Proceso.obtenerProceso(procesoId);
    }

    public static String obtenerNombreProceso(String procesoId) {
        return C_Proceso.obtenerProceso(procesoId).getProcesoNombre();
    }

    public static boolean esCriterioGlobalProceso(String process_name) {
        String procesoId = C_Proceso.processByName(process_name).getProcesoId();
        return C_Proceso.obtenerProceso(procesoId).isCriteriosGlobal();
    }

    public static ArrayList<ArrayList<String>> todosProcesos() {
        List<Proceso> proc = C_Proceso.todosProcesos();
        ArrayList<ArrayList<String>> list = new ArrayList<>();
        if (proc != null) {
            for (int i = 0; i < proc.size(); i++) {
                ArrayList<String> proceso = new ArrayList<>();
                String procesId = proc.get(i).getProcesoId();
                String proc_name = proc.get(i).getProcesoNombre();
                proceso.add(proc_name);
                List<Jerarquia> jerarq = C_Jerarquia.procesoJerarquias(procesId);
                for (int j = 0; j < jerarq.size(); j++) {
                    String jerar_nomb = jerarq.get(j).getJerarquiaNombre();
                    proceso.add(jerar_nomb);
                }
                list.add(proceso);
            }
        }
        return list;
    }

    public static void borrarNivelesProceso(String procesoId) {

        C_NivelInferior.borrarInferiorProceso(procesoId);
        C_NivelSuperior.borrarSuperiorProceso(procesoId);
        C_Niveles.borrarNivelProceso(procesoId);

    }

    public static void nuevoProceso(String proc_name, String[] jerarquias, boolean global, String estado_long, String descripcion) {
        String procesId = C_Proceso.generarId();
        EstadoProceso estado = C_EstadoProceso.recuperarEstadoId(estado_long);
        Proceso proceso = new Proceso(procesId, proc_name, descripcion,global);
        proceso.setEstadoProceso(estado);
        C_Proceso.nuevo(proceso);

        /*+inserta las jerarquías+*/
        for (int i = 0; i < jerarquias.length; i++) {
            int jerarquiaId = i + 1;
            String jerarqName = jerarquias[i];
            JerarquiaId id = new JerarquiaId(jerarquiaId, procesId);
            Jerarquia jerarquia = new Jerarquia(id, proceso, jerarqName);
            C_Jerarquia.nuevo(jerarquia);
        }
    }

    public static boolean modificarProceso(String process_name, String proc_name, String[] jerarquias, boolean global, boolean flag, String estad_long) {
        String procesId = C_Proceso.processByName(process_name).getProcesoId();
        boolean modif;

        if (!C_ProcesoPrograma.existeProgramaProceso(procesId)) {
            Proceso proceso = C_Proceso.obtenerProceso(procesId);
            EstadoProceso estado = C_EstadoProceso.recuperarEstadoId(estad_long);
            proceso.setProcesoNombre(proc_name);
            proceso.setCriteriosGlobal(global);
            proceso.setEstadoProceso(estado);
            C_Proceso.modificar(proceso);
            modif = true;

            if (flag) {
                if (!C_Niveles.existeNivelProceso(procesId)) {
                    /*+ borra e inserta las jerarquías+*/
                    C_Jerarquia.borrarJerarquiaProceso(procesId);

                    for (int i = 0; i < jerarquias.length; i++) {
                        int jerarquiaId = i + 1;
                        String jerarqName = jerarquias[i];
                        JerarquiaId id = new JerarquiaId(jerarquiaId, procesId);
                        Jerarquia jerarquia = new Jerarquia(id, proceso, jerarqName);
                        C_Jerarquia.nuevo(jerarquia);
                    }
                } else {
                    System.out.println("ERROR!! NO SE PUEDE MODIFICAR JERARQUIAS, NIVELES YA CREADOS");
                    modif = false;
                }
            }
        } else {
            System.out.println("ERROR!! NO SE PUEDE MODIFICAR PROCESO, PROGRAMA YA ASIGNÓ ESTE PROCESO");
            modif = false;
        }
        return modif;
    }

    /* ESTADOS DE PROCESO!*/
    public static String[] obtenerEstadosProceso() {
        List<EstadoProceso> estado = C_EstadoProceso.todosEstadosProceso();
        int cant_estad = estado.size();
        String[] estad_names = new String[cant_estad];
        for (int i = 0; i < cant_estad; i++) {
            estad_names[i] = estado.get(i).getEstadoProcLong();
        }
        return estad_names;
    }

    public static String descripEstadoProceso(String process_name) {
        String procesoId = C_Proceso.processByName(process_name).getProcesoId();
        String estadoId = C_Proceso.obtenerProceso(procesoId).getEstadoProceso().getEstadoProcId();
        return C_EstadoProceso.recuperarEstadoDescr(estadoId).getEstadoProcLong();
    }

    /*++++++++++++++++++++++++++++++++++++++++++++++++*/
 /*++++++       SERVICIOS DE PROGRAMA        ++++++*/
 /*++++++++++++++++++++++++++++++++++++++++++++++++*/
    public static Programa programaById(String programaId) {
        return C_Programa.programaById(programaId);

    }

    public static String obtenerNombrePrograma(String programaId) {
        return C_Programa.programaById(programaId).getProgramaNombre();

    }

     public static String obtenerCodigoPrograma(String progrma_name) {
        String programaId = C_Programa.programByName(progrma_name).getProgramaId();
        return C_Programa.programaById(programaId).getProgramaCodigo();

    }

    public static boolean nuevoPrograma(String facul_name, String nom_prog, String cod_prog) {
        Facultad facultad = C_Facultad.facultadByNombre(facul_name);
        String programId = C_Programa.generarId();
        Programa nuevo = new Programa(programId, facultad, nom_prog);
        nuevo.setProgramaCodigo(cod_prog);
        /*VALIDAR CODIGO UNICO*/
        if (!C_Programa.existeCodigo(programId)) {
            C_Programa.nuevo(nuevo);
            return true;
        } else {
            return false;//System.out.println("Códio ya existe");
        }
    }

    public static void modificarPrograma(String programaId, String facultadId, String prog_name, String prog_code) {
        Programa programa = C_Programa.programaById(programaId);
        programa.setFacultad(C_Facultad.facultadById(facultadId));
        programa.setProgramaNombre(prog_name);
        programa.setProgramaCodigo(prog_code);
        C_Programa.modificar(programa);
    }

    public static String[] facultadProgramas(String facul_name) {
        String facultadId = C_Facultad.facultadByNombre(facul_name).getFacultadId();
        List<Programa> prog = C_Programa.facultadProgramas(facultadId);
        String[] list = new String[prog.size()];
        for (int i = 0; i < prog.size(); i++) {
            list[i] = prog.get(i).getProgramaNombre();
        }
        return list;
    }

    public static ArrayList<String[]> todosProgramas() {

        List<Programa> prog = C_Programa.todosProgramas();
        ArrayList<String[]> datos = new ArrayList<>();
        if (prog != null) {
            for (int i = 0; i < prog.size(); i++) {
                String[] list = new String[3];
                list[0] = prog.get(i).getProgramaCodigo();
                String facultadId = prog.get(i).getFacultad().getFacultadId();
                list[1] = C_Facultad.facultadById(facultadId).getFacultadNombre();
                list[2] = prog.get(i).getProgramaNombre();
                datos.add(list);
            }
        }
        return datos;
    }

    public static ArrayList<String> todosNombresProgramas() {

        List<Programa> prog = C_Programa.todosProgramas();
        ArrayList<String> datos = new ArrayList<>();
        if (prog != null) {
            for (int i = 0; i < prog.size(); i++) {
                datos.add(prog.get(i).getProgramaNombre());
            }
        }
        return datos;
    }

    /*++++++++++++++++++++++++++++++++++++++++++++++++*/
 /*++++++       SERVICIOS DE JERARQUÍA        ++++++*/
 /*++++++++++++++++++++++++++++++++++++++++++++++++*/
    public static Jerarquia obtenerJerarquia(int jerarquiaId) {
        return C_Jerarquia.obtenerJerarquia(jerarquiaId);
    }

    public static String[] procesoJerarquias(String process_name) {
        String procesoId = C_Proceso.processByName(process_name).getProcesoId();
        List<Jerarquia> list = C_Jerarquia.procesoJerarquias(procesoId);
        String[] jerarquias = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            jerarquias[i] = list.get(i).getJerarquiaNombre();
        }
        return jerarquias;
    }

    public static int mayorJerarquia(String process_name) {
        String procesoId = C_Proceso.processByName(process_name).getProcesoId();
        return C_Jerarquia.mayorJerarquia(procesoId);
    }

    public static int menorJerarquia(String procesoId) {
        return C_Jerarquia.menorJerarquia(procesoId);
    }

    public static int cantidadJerarquias(String process_name) {
        String procesoId = C_Proceso.processByName(process_name).getProcesoId();
        return C_Jerarquia.procesoJerarquias(procesoId).size();
    }

    /*++++++++++++++++++++++++++++++++++++++++++++++++*/
 /*++++++        SERVICIOS DE NIVELS         ++++++*/
 /*++++++++++++++++++++++++++++++++++++++++++++++++*/
    public static Niveles obtenerNivel(String nivelId, String procesoId, int jerarquiaId) {
        return C_Niveles.obtenerNivel(nivelId, procesoId, jerarquiaId);
    }

    public static void nuevoNivel(Niveles nuevo) {
        C_Niveles.nuevo(nuevo);
    }

    public static void nuevoNivelSuperior(NivelSuperior nuevo) {
        C_NivelSuperior.nuevo(nuevo);
    }

    public static void nuevoNivelInferior(NivelInferior nuevo) {
        C_NivelInferior.nuevo(nuevo);
    }

    public static NivelSuperior obtenerNivelSuperior(String idSup) {
        return C_NivelSuperior.obtenerNivelSuperior(idSup);
    }

    public static List<Niveles> nivelesJerarquia(String procesoId, int jerarquiaId) {
        return C_Niveles.nivelesJerarquia(procesoId, jerarquiaId);
    }

    public static List<Niveles> nivelesProceso(String procesoId) {
        return C_Niveles.nivelesProceso(procesoId);
    }

    public static List<NivelInferior> nivelesInferiorProceso(String procesoId) {
        return C_NivelInferior.nivelesInferiorProceso(procesoId);
    }

    public static boolean existeNivelProceso(String proces_name) {
        String procesoId = C_Proceso.processByName(proces_name).getProcesoId();
        return C_Niveles.existeNivelProceso(procesoId);
    }

    public static ArrayList<ArrayList<String>> obtieneMenorNivel(String proces_name) {
        String procesId = C_Proceso.processByName(proces_name).getProcesoId();
        List<Niveles> niveles = C_Niveles.nivelesMenoresProceso(procesId);
        int cant_niv = niveles.size();
        ArrayList<ArrayList<String>> nivel_menor = new ArrayList<>();
        System.out.println("Cantidad: " + cant_niv);
        ArrayList<String> nombres = new ArrayList<>();
        ArrayList<String> ids = new ArrayList<>();
        for (int i = 0; i < cant_niv; i++) {
            nombres.add(niveles.get(i).getNivelNombre());
            ids.add(niveles.get(i).getId().getNivelId());
        }
        nivel_menor.add(nombres);
        nivel_menor.add(ids);
        return nivel_menor;
    }

    public static ArrayList<ArrayList<String>> obtieneNivelesHijo(String proces_name, String nivelId) {
        String procesId = C_Proceso.processByName(proces_name).getProcesoId();
        List<Niveles> niveles = C_Niveles.obtenerNivelesHijo(procesId, nivelId);
        ArrayList<ArrayList<String>> nivel_hijo = new ArrayList<>();

        if (niveles != null) {
            int cant_niv = niveles.size();
            System.out.println("Cantidad: " + cant_niv);
            ArrayList<String> nombres = new ArrayList<>();
            ArrayList<String> ids = new ArrayList<>();

            for (int i = 0; i < cant_niv; i++) {
                nombres.add(niveles.get(i).getNivelNombre());
                ids.add(niveles.get(i).getId().getNivelId());
            }
            nivel_hijo.add(nombres);
            nivel_hijo.add(ids);
        }
        return nivel_hijo;

    }

}
