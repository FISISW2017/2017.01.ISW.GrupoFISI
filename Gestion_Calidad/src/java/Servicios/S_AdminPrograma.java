package Servicios;

import Auxiliares.UploadDropbox;
import Controlador.C_Criterio;
import Controlador.C_Documento;
import Controlador.C_EstadoCumpl;
import Controlador.C_EstadoProcProg;
import Controlador.C_EstadoValidacion;
import Controlador.C_FuenteVerificacion;
import Controlador.C_Jerarquia;
import Controlador.C_NivProcPrograma;
import Controlador.C_Niveles;
import Controlador.C_Proceso;
import Controlador.C_ProcesoPrograma;
import Controlador.C_Programa;
import Modelo.Criterio;
import Modelo.Documento;
import Modelo.EstadoCumpl;
import Modelo.EstadoProcProg;
import Modelo.EstadoValidacion;
import Modelo.FuenteVerificacion;
import Modelo.NivProcPrograma;
import Modelo.NivProcProgramaId;
import Modelo.Niveles;
import Modelo.Proceso;
import Modelo.ProcesoPrograma;
import Modelo.ProcesoProgramaId;
import Modelo.Programa;
import com.dropbox.core.DbxException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author  
 */
public class S_AdminPrograma {

    public static void nuevaAsociacion(String program_name, String proces_name, String estado_rel, String estado_cump, boolean opcional, boolean validar) {
        String programaId = C_Programa.programByName(program_name).getProgramaId();
        String procesoId = C_Proceso.processByName(proces_name).getProcesoId();
        ProcesoProgramaId id = new ProcesoProgramaId(programaId, procesoId);
        Proceso proc = C_Proceso.obtenerProceso(procesoId);
        Programa prog = C_Programa.programaById(programaId);
        EstadoProcProg estad_relac = C_EstadoProcProg.recuperarEstadoId(estado_rel);
        EstadoCumpl estad_cump = C_EstadoCumpl.recuperarEstadoId(estado_cump);
        ProcesoPrograma nuevo = new ProcesoPrograma(id, proc, prog);
        nuevo.setConsideraOpcionales(opcional);
        nuevo.setValidacionHabilitada(validar);
        nuevo.setEstadoCumpl(estad_cump);
        nuevo.setEstadoProcProg(estad_relac);
        C_ProcesoPrograma.nuevo(nuevo);

        /*++++++++++++++++++++++++++++++++++++++++++++++++++*/
 /*          FAC_001 - 01/07/17 - inicio             */
 /*++++++++++++++++++++++++++++++++++++++++++++++++++*/
 /*+ Asocia los Niveles del Proceso al Programa +*/
        List<Niveles> niveles = C_Niveles.nivelesProceso(procesoId);
        EstadoCumpl estadoC = C_EstadoCumpl.recuperarEstadoId(estado_cump);
        for (int i = 0; i < niveles.size(); i++) {
            String nivelId = niveles.get(i).getId().getNivelId();
            int jerarquId = niveles.get(i).getId().getJerarquiaId();
            String procesId = niveles.get(i).getId().getProcesoId();
            NivProcProgramaId nivId = new NivProcProgramaId(programaId, procesoId, nivelId, jerarquId, procesId);
            Niveles nivel = C_Niveles.obtenerNivel(nivelId, procesId, jerarquId);
            ProcesoPrograma procProg = C_ProcesoPrograma.obtieneProgramaProceso(procesoId, programaId);
            NivProcPrograma nivProc = new NivProcPrograma(nivId, nivel, procProg);
            nivProc.setEstadoCumpl(estadoC);
            C_NivProcPrograma.nuevo(nivProc);
        }

        /*++++++++++++++++++++++++++++++++++++++++++++++++++*/
 /*          FAC_001 - 01/07/17 - fin             */
 /*++++++++++++++++++++++++++++++++++++++++++++++++++*/
    }

    public static boolean existeProgramaProceso(String procesoId) {
        return C_ProcesoPrograma.existeProgramaProceso(procesoId);

    }

    /* ESTADOS DE RELACION PROCESO PROGRAMA!*/
    public static String[] obtenerEstadosProcProg() {
        List<EstadoProcProg> estado = C_EstadoProcProg.todosEstadosProcProg();
        int cant_estad = estado.size();
        String[] estad_names = new String[cant_estad];
        for (int i = 0; i < cant_estad; i++) {
            estad_names[i] = estado.get(i).getEstadoProcprogLong();
        }
        return estad_names;
    }

    public static String obtenerEstadoCumplLong(String estadoId) {
        return C_EstadoCumpl.recuperarEstadoDescr(estadoId).getEstadoCumplLong();
    }

    public static void modificarAsociacion(String program_name, String proces_name, String estado_rel, boolean opcional, boolean validar) {
        String programaId = C_Programa.programByName(program_name).getProgramaId();
        String procesoId = C_Proceso.processByName(proces_name).getProcesoId();

        ProcesoPrograma relacion = C_ProcesoPrograma.obtieneProgramaProceso(procesoId, programaId);
        EstadoProcProg estad_relac = C_EstadoProcProg.recuperarEstadoId(estado_rel);
        relacion.setConsideraOpcionales(opcional);
        relacion.setValidacionHabilitada(validar);
        relacion.setEstadoProcProg(estad_relac);
        C_ProcesoPrograma.modificar(relacion);
    }

    public static ArrayList<String> procesoPorPrograma(String program_code) {
        String programaId = C_Programa.programByCode(program_code).getProgramaId();
        List<ProcesoPrograma> procesos = C_ProcesoPrograma.procesoPorPrograma(programaId);
        ArrayList<String> datos = new ArrayList();
        if (procesos != null) {
            int cant_proc = procesos.size();
            for (int i = 0; i < cant_proc; i++) {
                String procesoId = procesos.get(i).getId().getProcesoId();
                Proceso proc = C_Proceso.obtenerProceso(procesoId);
                String proc_prog = proc.getProcesoNombre();
                datos.add(proc_prog);
            }
        }
        return datos;
    }

    public static ArrayList<String> procesosPosiblesAPrograma(String program_code) {
        String programaId = C_Programa.programByCode(program_code).getProgramaId();
        List<Proceso> procesos = C_Proceso.procesoParaPrograma(programaId);
        ArrayList<String> datos = new ArrayList();
        if (procesos != null) {
            int cant_proc = procesos.size();
            for (int i = 0; i < cant_proc; i++) {
                String procesoId = procesos.get(i).getProcesoId();
                String proces_name = procesos.get(i).getProcesoNombre();
                if (C_Jerarquia.procesoJerarquias(procesoId) != null) {
                    if (C_Niveles.existeNivelProceso(procesoId)) {
                        datos.add(proces_name);
                    } else {
                        System.out.println("Proceso " + proces_name + " sin Niveles definidos");
                    }
                } else {
                    System.out.println("Grave!! Proceso " + proces_name + " sin Jerarquías definidas (?)");
                }

            }
        }
        return datos;
    }

    public static String[] datosDeAsociacion(String proces_name, String program_code) {
        String programaId = C_Programa.programByCode(program_code).getProgramaId();
        String procesoId = C_Proceso.processByName(proces_name).getProcesoId();
        ProcesoPrograma proProg = C_ProcesoPrograma.obtieneProgramaProceso(procesoId, programaId);
        String[] datos = new String[2];
        String opcionales, validacion;
        if (proProg.getConsideraOpcionales()) {
            opcionales = "Y";
        } else {
            opcionales = "N";
        }
        if (proProg.getValidacionHabilitada()) {
            validacion = "Y";
        } else {
            validacion = "N";
        }
        datos[0] = opcionales;
        datos[1] = validacion;
        return datos;
    }

    public static String obtieneEstadoProcProgr(String proces_name, String programaId) {
        Proceso proceso = C_Proceso.processByName(proces_name);
        String procesoId = proceso.getProcesoId();
        String estadoId = C_ProcesoPrograma.obtieneProgramaProceso(procesoId, programaId).getEstadoProcProg().getEstadoProcprogId();
        return C_EstadoProcProg.recuperarEstadoDescr(estadoId).getEstadoProcprogLong();
    }

    public static boolean consideraOpcionales(String proces_name, String programaId) {
        Proceso proceso = C_Proceso.processByName(proces_name);
        String procesoId = proceso.getProcesoId();
        return C_ProcesoPrograma.obtieneProgramaProceso(procesoId, programaId).getConsideraOpcionales();
    }

    public static boolean estaAbiertoValidacion(String proces_name, String programaId) {
        Proceso proceso = C_Proceso.processByName(proces_name);
        String procesoId = proceso.getProcesoId();
        return C_ProcesoPrograma.obtieneProgramaProceso(procesoId, programaId).getValidacionHabilitada();
    }

    public static String obtieneEstadoCumplimiento(String proces_name, String programaId) {
        Proceso proceso = C_Proceso.processByName(proces_name);
        String procesoId = proceso.getProcesoId();
        String estadoId = C_ProcesoPrograma.obtieneProgramaProceso(procesoId, programaId).getEstadoCumpl().getEstadoCumplId();
        return C_EstadoCumpl.recuperarEstadoDescr(estadoId).getEstadoCumplLong();
    }

    /*++++++++++++++++++++++++++++++++++++++++++++++++++*/
 /*          FAC_001 - 01/07/17 - inicio             */
 /*++++++++++++++++++++++++++++++++++++++++++++++++++*/
    public static void guardarCriterio(String criterioId, String proces_name, String prog_code, String nivelId, String descripcion, ArrayList<String[]> fuentesDatos) {
        String programId = C_Programa.programByCode(prog_code).getProgramaId();
        String procesId = C_Proceso.processByName(proces_name).getProcesoId();
        NivProcPrograma nivPcPg = C_NivProcPrograma.obtenerNivProcPrograma(procesId, programId, nivelId);
        EstadoCumpl estadoCumpl = C_EstadoCumpl.recuperarEstadoId("Iniciado");
        if ("".equals(criterioId)) {
            criterioId = C_Criterio.generarId(procesId, programId);
            Criterio nuevo = new Criterio(criterioId, nivPcPg, descripcion);
            nuevo.setEstadoCumpl(estadoCumpl);
            nuevo.setIngresaPrctOblig(0.0);
            nuevo.setIngresaPrctOpcio(0.0);
            nuevo.setValidPrctOblig(0.0);
            nuevo.setValidPrctOpcio(0.0);
            C_Criterio.nuevo(nuevo);

        } else {
            Criterio criterio = C_Criterio.CriteriosById(criterioId);
            criterio.setCriterioDescripcion(descripcion);
            C_Criterio.modificar(criterio);
        }
        /* Inserta las Fuentes de Verificación */
        for (int i = 0; i < fuentesDatos.size(); i++) {
            String fuenteId = C_FuenteVerificacion.generarId(criterioId);
            Criterio criterio = C_Criterio.CriteriosById(criterioId);
            FuenteVerificacion nuevaFuente = new FuenteVerificacion(fuenteId, criterio);
            nuevaFuente.setFuenteDescr(fuentesDatos.get(i)[1]);
            nuevaFuente.setFuenteTipo(fuentesDatos.get(i)[2]);
            boolean obligatorio = "Y".equals(fuentesDatos.get(i)[3]);
            nuevaFuente.setFuenteObligatoria(obligatorio);
            EstadoValidacion estVal = C_EstadoValidacion.recuperarEstadoId("Definido");
            nuevaFuente.setEstadoValidacion(estVal);
            C_FuenteVerificacion.nuevo(nuevaFuente);
        }
        recalcularPctjIngCriterio(criterioId);
    }

    public static ArrayList<String[]> criteriosByNivel(String proces_name, String prog_code, String nivelId) {
        String programId = C_Programa.programByCode(prog_code).getProgramaId();
        String procesId = C_Proceso.processByName(proces_name).getProcesoId();
        ArrayList<String[]> datos = new ArrayList<>();
        List<Criterio> criterios = C_Criterio.CriteriosByNivel(procesId, programId, nivelId);
        ProcesoPrograma procProg = C_ProcesoPrograma.obtieneProgramaProceso(procesId, programId);
        boolean opcional = procProg.getConsideraOpcionales();
        int tamaño = 0;
        if (criterios != null) {
            tamaño = criterios.size();
        }
        for (int i = 0; i < tamaño; i++) {
            String[] linea = new String[4];//0--> criterioId, 1--> Descripciom, 2--> %Ingresados, 3--> %Validados
            double ptjIngr, ptjVal;
            DecimalFormat df = new DecimalFormat("#.00");
            if (opcional) {
                ptjIngr = criterios.get(i).getIngresaPrctOpcio();
                ptjVal = criterios.get(i).getValidPrctOpcio();
            } else {
                ptjIngr = criterios.get(i).getIngresaPrctOblig();
                ptjVal = criterios.get(i).getValidPrctOblig();
            }
            linea[0] = criterios.get(i).getCriterioId();
            linea[1] = criterios.get(i).getCriterioDescripcion();
            if (ptjIngr == 0.0) {
                linea[2] = "0,00%";
            } else {
                linea[2] = df.format(ptjIngr) + "%";
            }
            if (ptjVal == 0.0) {
                linea[3] = "0,00%";
            } else {
                linea[3] = df.format(ptjVal) + "%";
            }
            datos.add(linea);
        }
        return datos;
    }

    public static ArrayList<String[]> nombreFuentesByCriterio(String criterioId) {
        ArrayList<String[]> datos = new ArrayList<>();
        List<FuenteVerificacion> fuentes = C_FuenteVerificacion.fuentesByCriterio(criterioId);
        for (int i = 0; i < fuentes.size(); i++) {
            String[] linea = new String[2];//0--> fuenteId, 1--> Descripcion
            linea[0] = fuentes.get(i).getFuenteId();
            linea[1] = fuentes.get(i).getFuenteDescr();
            datos.add(linea);
        }
        return datos;
    }

    public static String descripcionCriterio(String criterioId) {
        return C_Criterio.CriteriosById(criterioId).getCriterioDescripcion();
    }

    public static ArrayList<String[]> fuentesByCriterio(String criterioId) {
        List<FuenteVerificacion> fuente = C_FuenteVerificacion.fuentesByCriterio(criterioId);
        ArrayList<String[]> datos = new ArrayList<>();
        if (fuente != null) {
            for (int i = 0; i < fuente.size(); i++) {

                String[] linea = new String[5];//0--> fuenteId, 1--> Descripcion, 2--> Tipo, 3-->Obligatorieidad
                linea[0] = fuente.get(i).getFuenteId();
                linea[1] = fuente.get(i).getFuenteDescr();
                linea[2] = fuente.get(i).getFuenteTipo();
                if (fuente.get(i).getFuenteObligatoria()) {
                    linea[3] = "Y";
                } else {
                    linea[3] = "N";
                }
                String estadoId = fuente.get(i).getEstadoValidacion().getEstadoValidId();
                EstadoValidacion stat = C_EstadoValidacion.recuperarEstadoDescr(estadoId);
                linea[4] = stat.getEstadoValidLong();
                datos.add(linea);
            }
        }
        return datos;
    }

    public static void modificarFuente(String fuenteId, String descripcion, String tipo, String obligatorio) {
        FuenteVerificacion modifFuente = C_FuenteVerificacion.fuenteById(fuenteId);
        modifFuente.setFuenteDescr(descripcion);
        modifFuente.setFuenteTipo(tipo);
        boolean obliga = "Y".equals(obligatorio);
        modifFuente.setFuenteObligatoria(obliga);
        C_FuenteVerificacion.modificar(modifFuente);
        String criterioId = modifFuente.getCriterio().getCriterioId();
        recalcularPctjIngCriterio(criterioId);
    }

    public static void subirDocumento(String url, String docNombre, Date creationDate, String fuenteId) throws IOException, DbxException {
        FuenteVerificacion fuente = C_FuenteVerificacion.fuenteById(fuenteId);
        String docId = C_Documento.generarId(fuenteId);
        Documento documento = new Documento(docId, fuente, creationDate, docNombre);
        documento.setFuenteVerificacion(fuente);
        String name = docId + "-" + docNombre;
        UploadDropbox.cargarArchivo(name, url);
        C_Documento.nuevo(documento);

        String criterioId = fuente.getCriterio().getCriterioId();
        recalcularPctjIngCriterio(criterioId);

    }

    public static String descargarDocumento(String docId) throws IOException, DbxException {
        System.out.println("fdsjfdlkfjs "+ docId);
        Documento documento = C_Documento.documentoById(docId);
        String docName = documento.getDocNombre();
        String name = docId + "-" + docName;
        String url = "C:\\Users\\marisol\\Desktop\\PROYECTO SW\\ULTIMO PROYECTO SOFTWARE CALIDAD\\Gestion_Calidad" + docName; //poner ruta para archivos descargados

        UploadDropbox.descargarArchivo(name, url);
        return docName;
    }

    public static ArrayList<String[]> documentosByFuente(String fuenteId) {
        List<Documento> documento = C_Documento.documentosByFuente(fuenteId);
        ArrayList<String[]> datos = new ArrayList<>();
        if (documento != null) {
            for (int i = 0; i < documento.size(); i++) {
                String[] linea = new String[2];//0--> docId, 1--> Nombre
                linea[0] = documento.get(i).getDocId();
                linea[1] = documento.get(i).getDocNombre();
                datos.add(linea);
            }
        }
        return datos;
    }

    private static void recalcularPctjIngCriterio(String criterioId) {
        double totFuentes = C_FuenteVerificacion.totalFuentesByCriterio(criterioId);
        double totFuentesObliga = C_FuenteVerificacion.totalFuentesObligaByCriterio(criterioId);
        double fuentesIngOblig = C_FuenteVerificacion.totalFuentesObligIngresadasByCriterio(criterioId);
        double fuentesIngTot = C_FuenteVerificacion.totalFuentesIngresadasByCriterio(criterioId);
        double prctjOblig = 0.0;
        double prctjOpcio = 0.0;
        if (totFuentes != 0.0) {
            prctjOblig = (fuentesIngOblig / totFuentesObliga) * 100;

            prctjOpcio = (fuentesIngTot / totFuentes) * 100;

        }
        Criterio criterio = C_Criterio.CriteriosById(criterioId);
        criterio.setIngresaPrctOblig(prctjOblig);
        criterio.setIngresaPrctOpcio(prctjOpcio);
        C_Criterio.modificar(criterio);
    }

    public static void validarFuente(String fuenteId) {
        FuenteVerificacion fuente = C_FuenteVerificacion.fuenteById(fuenteId);
        EstadoValidacion estVal = C_EstadoValidacion.recuperarEstadoId("Validado");
        fuente.setEstadoValidacion(estVal);
        C_FuenteVerificacion.modificar(fuente);
        String criterioId = fuente.getCriterio().getCriterioId();
        recalcularPctjValCriterio(criterioId);
    }

    public static boolean estaValidadaFuente(String fuenteId) {
        return C_FuenteVerificacion.esFuenteValidada(fuenteId);
    }

    private static void recalcularPctjValCriterio(String criterioId) {
        double totFuentes = C_FuenteVerificacion.totalFuentesByCriterio(criterioId);
        double totFuentesObliga = C_FuenteVerificacion.totalFuentesObligaByCriterio(criterioId);
        double fuentesValOblig = C_FuenteVerificacion.totalFuentesObligValidadasByCriterio(criterioId);
        double fuentesValTot = C_FuenteVerificacion.totalFuentesValidadasByCriterio(criterioId);
        double prctjOblig = 0.0;
        double prctjOpcio = 0.0;
        if (totFuentes != 0.0) {
            prctjOblig = (fuentesValOblig / totFuentesObliga) * 100;

            prctjOpcio = (fuentesValTot / totFuentes) * 100;

        }
        Criterio criterio = C_Criterio.CriteriosById(criterioId);
        criterio.setValidPrctOblig(prctjOblig);
        criterio.setValidPrctOpcio(prctjOpcio);
        C_Criterio.modificar(criterio);
    }
    /*++++++++++++++++++++++++++++++++++++++++++++++++++*/
 /*          FAC_001 - 01/07/17 - fin             */
 /*++++++++++++++++++++++++++++++++++++++++++++++++++*/
}
