/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servicios;

import Auxiliares.Arbol;
import Controlador.C_Proceso;
import javax.swing.JTree;

/**
 *
 * @author  
 */
public class S_Arbol {

    public static String guardarTree(JTree tree, String process_name) {
        String procesoId = C_Proceso.processByName(process_name).getProcesoId();
        return Arbol.saveTree(tree, procesoId);
    }

    public static JTree obtieneArbol(String process_name) {
        String procesoId = C_Proceso.processByName(process_name).getProcesoId();
        return Arbol.obtieneArbol(procesoId);
    }
}
