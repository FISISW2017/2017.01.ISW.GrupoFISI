

use sistema_calidad;
/*******************/
/*VALOR DE FACULTAD*/
/*******************/
INSERT INTO FACULTAD VALUES ('F01','Ingeniería de Sistemas');



/********************/
/*VALORES DE ESTADOS*/
/********************/

/*+ estado de Proceso+*/
INSERT INTO ESTADO_PROCESO VALUES ('EP01','I','Inactivo','Proceso que no podrá ser asignado');
INSERT INTO ESTADO_PROCESO VALUES ('EP02','A','Activo','Proceso apto para ser asignado');


/*+ estado de asociación de proceso a programa+*/
INSERT INTO ESTADO_PROC_PROG VALUES ('EPP01','APT','Aperturado','Se ha realizado la asignación');
INSERT INTO ESTADO_PROC_PROG VALUES ('EPP02','DEF','Definido','Se han definido los criterios');
INSERT INTO ESTADO_PROC_PROG VALUES ('EPP03','FIN','Finalizado','Se concluyó correctamente');
INSERT INTO ESTADO_PROC_PROG VALUES ('EPP04','CRR','Cerrado','Ya no se usará el proceso en el programa');
INSERT INTO ESTADO_PROC_PROG VALUES ('EPP05','INH','Inhabilitado','Temporalmente inhabilitado de operaciones de ingreso');
INSERT INTO ESTADO_PROC_PROG VALUES ('EPP06','HAB','Habilitado','Habilitado para operaciones de ingreso');
INSERT INTO ESTADO_PROC_PROG VALUES ('EPP07','RPT','Reaperturado','Reapertura luego de Cerrar');

/*+ estado de cumplimiento de proceso/nivel/criterio+*/
INSERT INTO ESTADO_CUMPL VALUES ('EC01','INI','Iniciado','Primeras definiciones');
INSERT INTO ESTADO_CUMPL VALUES ('EC02','REG','Registrado','Todos sus subniveles están definidos');
INSERT INTO ESTADO_CUMPL VALUES ('EC03','ING','Ingresado','Todos sus subniveles contienen los adjuntos');
INSERT INTO ESTADO_CUMPL VALUES ('EC04','EVL','En Validación','Ingresado y con algún validado');
INSERT INTO ESTADO_CUMPL VALUES ('EC05','VAL','Validado','Todos sus subniveles validados');

/*+ estado de validación de factor+*/
INSERT INTO ESTADO_VALIDACION VALUES ('EV01','DEF','Definido','Se ha ingresado la Fuente de Verificación');
INSERT INTO ESTADO_VALIDACION VALUES ('EV02','ADJ','Adjunto','Se ha adjuntado el documento de Verificación');
INSERT INTO ESTADO_VALIDACION VALUES ('EV03','VAL','Validado','Se ha aprobado la Fuente de Verificación');
INSERT INTO ESTADO_VALIDACION VALUES ('EV04','IVL','Invalidado','No se ha aprobado la Fuente de Verificación');
INSERT INTO ESTADO_VALIDACION VALUES ('EV05','OBS','Observado','Alerta de posible error en la Fuente de Verificación');
INSERT INTO ESTADO_VALIDACION VALUES ('EV06','ELM','Eliminado','Se ha borrado el documento de Verificación');

