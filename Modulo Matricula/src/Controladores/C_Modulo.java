/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*          FAC_001 - 11/05/17 - inicio             */
/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
package Controladores;

/**
 *
 * @author Fabio
 */

import Conector.Conexion;
import Modelo.Alumno;
import Modelo.Modulo;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;

public class C_Modulo {
     public static Modulo ver(String moduloId) {

        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        Modulo apoyo = (Modulo) session.get(Modulo.class, moduloId);
      
        tx.commit();
        session.close();
        return apoyo;
    }
     
     public static List<Modulo> lista(){
        List <Modulo> apoyo = null;
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        apoyo = session.createQuery("from Modulo").list();
        session.close();
        return apoyo;
    }
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*          FAC_001 - 11/05/17 - fin                */
/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
