/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Conector.Conexion;
import Modelo.Alumno;
import Modelo.Nivel;
import java.util.List;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;

/**
 *
 * @author Richard
 */
public class C_Nivel {
    
    public static List<Nivel> lista() {

        List<Nivel> apoyo = null;

        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction(); 
        apoyo = session.createQuery("from Nivel").list();
        tx.commit();
        session.close();

        return apoyo;
    }
    
     public static void nuevo(Nivel nuevo) {
         StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();

        session.insert(nuevo);
        
        tx.commit();

        session.close();
    }
     
     public static Nivel ver(String Id) {

        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        Nivel apoyo = (Nivel) session.get(Nivel.class, Id);
      
        tx.commit();
        session.close();
        return apoyo;
    }  

}
