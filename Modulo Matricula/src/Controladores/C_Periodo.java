/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Conector.Conexion;
import Modelo.Periodo;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;

/**
 *
 * @author Richard
 */
public class C_Periodo {

    public static List<Periodo> lista() {

        List<Periodo> apoyo = null;

        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction(); 
        apoyo = session.createQuery("from Periodo order by periodoAno").list();
        tx.commit();
        session.close();

        return apoyo;
    }

    public static void nuevo(Periodo nuevo) {
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();

        session.insert(nuevo);

        tx.commit();

        session.close();
    }

    public static void modificar(Periodo periodo) {
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        

        session.update(periodo);
        
        tx.commit();
        session.close();
    }

    public static Periodo ver(String id) {
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        Periodo apoyo = (Periodo) session.get(Periodo.class, id);

        tx.commit();
        session.close();
        return apoyo;
    }
    
  
}
