/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*          FAC_001 - 11/05/17 - inicio             */
/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
package Controladores;

/**
 *
 * @author Fabio
 */

import Conector.Conexion;
import Modelo.Modulo;
import Modelo.Submodulo;

import Modelo.SubmoduloId;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;

public class C_Submodulo {
    public static Submodulo ver(String moduloId, String submoduloId) {
         
         
        List <Submodulo> apoyo;
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        //System.out.println("ACA HAY ERROR");
        apoyo = session.createQuery("from Submodulo where id.moduloId = '"+moduloId+"' and id.submoduloId = '"+submoduloId+"'").list();
        session.close();
        if (apoyo.size()> 0){
            return apoyo.get(0);
        }
        else 
            return null;
    }
    
    //obtener los submodulos según el modulo
    public static List<Submodulo> lista(Modulo modulo){
        List <Submodulo> apoyo = null;
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        String moduloId = modulo.getModuloId();
        apoyo = session.createQuery("from Submodulo where id.moduloId = '"+moduloId+"'").list();
        session.close();
        return apoyo;
    }
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*          FAC_001 - 11/05/17 - fin                */
/*++++++++++++++++++++++++++++++++++++++++++++++++++*/