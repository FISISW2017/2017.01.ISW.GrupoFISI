/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Conector.Conexion;

import Modelo.Familiar;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;

/**
 *
 * @author Richard
 */
public class C_Familiar {
      public static List<Familiar> lista() {

        List<Familiar> apoyo = null;

        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        
        Transaction tx = session.beginTransaction(); 
                apoyo = session.createQuery("from Familiar").list();
     
        tx.commit();
        session.close();

        return apoyo;
    }
      
     public static void nuevo(Familiar nuevo) {
        Session session = Conexion.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();

        session.save(nuevo);
        session.flush();
        tx.commit();

        session.close();
    }

    public static void modificar(Familiar apoderado) {
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        session.update(apoderado);
     
       
        tx.commit();
        session.close();
    }  
      
    public static Familiar ver(String Id) {

        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        Familiar apoyo = (Familiar) session.get(Familiar.class, Id);
        
        tx.commit();
        session.close();
        return apoyo;
    }
}
