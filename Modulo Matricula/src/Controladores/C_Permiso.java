/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*          FAC_001 - 11/05/17 - inicio             */
/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
package Controladores;

/**
 *
 * @author Fabio
 */
import Conector.Conexion;
import Modelo.Modulo;
import Modelo.PermisosUsuarios;
import Modelo.PermisosUsuariosId;
import Modelo.Submodulo;
import Modelo.Usuario;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;

public class C_Permiso {
    
    //Obtiene el permiso según el Usuario, Módulo y Submódulo
     public static PermisosUsuarios permiso(Usuario usuario, Modulo modulo, Submodulo submodulo) {
        List <PermisosUsuarios> apoyo = null;
        String usuarioId = usuario.getUsuarioId();
        String moduloId = modulo.getModuloId();
        String submoduloId = submodulo.getId().getSubmoduloId();
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        apoyo = session.createQuery("from PermisosUsuarios where id.usuarioId = '"+usuarioId+"' and id.moduloId = '"
                +moduloId+"' and id.submoduloId = '"+submoduloId+"'").list();
        session.close();
        
       
        if (apoyo.size()> 0){
           return apoyo.get(0);
       }
        else 
            return null;
    }
     
     //Indica si se han generado los permisos a un usuario
     public static boolean permisoUsuario(Usuario usuario){
          List <PermisosUsuarios> apoyo = null;
        String usuarioId = usuario.getUsuarioId();
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        apoyo = session.createQuery("from PermisosUsuarios where id.usuarioId = '"+usuarioId+"'").list();
        session.close();
        return apoyo.size()> 0;
        
     }
     
    
     
     //Indica si tiene acceso a un submódulo específico
      public static boolean permisoSubmodulo(Usuario usuario, Modulo modulo, Submodulo submodulo) {
        String usuarioId = usuario.getUsuarioId();
        String moduloId = modulo.getModuloId();
        String submoduloId = submodulo.getId().getSubmoduloId();
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
       List<PermisosUsuarios> apoyo = session.createQuery("from PermisosUsuarios where id.usuarioId = '" + usuarioId 
               + "' and id.moduloId = '"+moduloId+"' and id.submoduloId = '"+submoduloId+"' ").list();
   
     
       session.close();
        return apoyo.size() > 0;
    }
     
     //Indica si tiene acceso a un módulo específico
     public static boolean permisoModulo(Usuario usuario, Modulo modulo){
         String usuarioId = usuario.getUsuarioId();
        String moduloId = modulo.getModuloId();
         StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
       List<PermisosUsuarios> apoyo = session.createQuery("from PermisosUsuarios where id.usuarioId = '" + usuarioId 
               + "' and id.moduloId = '"+moduloId+"'").list();
   
     
       session.close();
         return apoyo.size() > 0;
        
     }
     
     
      public static void nuevo(PermisosUsuarios nuevo) {
         StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();

        session.insert(nuevo);
        
        tx.commit();

        session.close();
    }

    public static void modificar(PermisosUsuarios permiso) {
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        System.out.println("\nPRONTO A ACTUALIZAR");
        session.update(permiso);
         System.out.println("\n ACTUALIZADO");
        tx.commit();
        session.close();
    }
     
    
     
     
     
     
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*          FAC_001 - 11/05/17 - fin                */
/*++++++++++++++++++++++++++++++++++++++++++++++++++*/