/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Conector.Conexion;
import Modelo.Documento;
import java.util.List;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;

/**
 *
 * @author Richard
 */
public class C_Documentos {
    
     public static List<Documento> lista() {

        List<Documento> apoyo = null;

         StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction(); 
        apoyo = session.createQuery("from Documento").list();
        tx.commit();
        session.close();

        return apoyo;
    }

  

   

    public static Documento ver(String Id) {

        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        Documento apoyo = (Documento) session.get(Documento.class, Id);
      
        tx.commit();
        session.close();
        return apoyo;
    }

    public static void nuevo(Documento nuevo) {
         StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();

        session.insert(nuevo);
        
        tx.commit();

        session.close();
    }

    public static void modificar(Documento alumno) {
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        session.update(alumno);
        tx.commit();
        session.close();
    }
    
}
