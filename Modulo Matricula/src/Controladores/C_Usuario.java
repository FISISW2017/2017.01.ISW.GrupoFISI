/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*          FAC_001 - 11/05/17 - inicio             */
/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
package Controladores;

/**
 *
 * @author Fabio
 */

import Conector.Conexion;
import Modelo.Alumno;
import Modelo.Usuario;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;


public class C_Usuario {
    
    
      public static Usuario usuario(String apellidos, String nombres) {

        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        List<Usuario> apoyo = session.createQuery("from Usuario where "
                + "usuarioApellidos = '" + apellidos + "' and "
                + "usuarioNombres = '" + nombres + "'").list();;
        session.close();
        if (apoyo.size() > 0) {
            return apoyo.get(0);
        } else {
            return null;
        }
    
    }

    public static Usuario usuario(String dni) {

        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
       List<Usuario> apoyo = session.createQuery("from Usuario where "
                + "usuarioDni = '" + dni + "'").list();
   
     
       session.close();
        if (apoyo.size() > 0) {
            return apoyo.get(0);
        } else {
            return null;
        }
    }

    public static Usuario ver(String user_id) {

        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        Usuario apoyo = (Usuario) session.get(Usuario.class, user_id);
      
        tx.commit();
        session.close();
        return apoyo;
    }

    public static void nuevo(Usuario nuevo) {
         StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();

        session.insert(nuevo);
        
        tx.commit();

        session.close();
    }

    public static void modificar(Usuario user) {
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        session.update(user);
        tx.commit();
        session.close();
    }

}
/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*          FAC_001 - 11/05/17 - fin                */
/*++++++++++++++++++++++++++++++++++++++++++++++++++*/