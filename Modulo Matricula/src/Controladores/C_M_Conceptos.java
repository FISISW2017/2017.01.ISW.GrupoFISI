/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Conector.Conexion;
import Modelo.MatriculaConceptos;
import Modelo.MatriculaConceptosId;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;

/**
 *
 * @author Richard
 */
public class C_M_Conceptos {
    
     public static List<MatriculaConceptos> lista(String año,String alumno) {

        List<MatriculaConceptos> apoyo = null;
        
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
       Transaction tx = session.beginTransaction(); 
        apoyo = session.createQuery("from MatriculaConceptos where id.periodoAno = '"+año+"' and "
                +"id.alumnoCodigo = '"+alumno+"' and conceptoECancelado = false").list();
        tx.commit();
        session.close();
        if(apoyo.size() >0)
        return apoyo;
        else
            return null;
    }
    
       public static void nuevo(MatriculaConceptos nuevo) {
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();

        session.insert(nuevo);
        
        tx.commit();

        session.close();
    }
    
         public static MatriculaConceptos ver(MatriculaConceptosId id) {
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        MatriculaConceptos apoyo = (MatriculaConceptos) session.get(MatriculaConceptos.class,id);
       
        
        tx.commit();
        session.close();
        return apoyo;
    }
       
       public static void modificar(MatriculaConceptos concepto) {
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        session.update(concepto);
        
        tx.commit();
        session.close();
    }

     
  
     
}
