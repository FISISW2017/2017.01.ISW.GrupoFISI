/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Conector.Conexion;
import Modelo.Alumno;
import Modelo.MatriculaConceptos;
import Modelo.MatriculaConceptosId;
import Modelo.MatriculaPensiones;
import Modelo.MatriculaPensionesId;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;

/**
 *
 * @author Richard
 */
public class C_M_Pensiones {
    
     public static List<MatriculaPensiones> lista(String año,String alumno) {

        List<MatriculaPensiones> apoyo = null;

        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction(); 
        apoyo = session.createQuery("from MatriculaPensiones where id.periodoAno = '"+año+"' and "
                +"id.alumnoCodigo = '"+alumno+"' and pensionECancelado = false").list();
        tx.commit();
        session.close();

         if(apoyo.size() >0)
        return apoyo;
        else
            return null;
    }
     
    public static void nuevo(MatriculaPensiones nuevo) {
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();

        session.insert(nuevo);
        
        tx.commit();

        session.close();
    }
    
    
      
       public static void modificar(MatriculaPensiones pension) {
         StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        session.update(pension);
        tx.commit();
        session.close();
    }
      
          public static MatriculaPensiones ver(MatriculaPensionesId id) {
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        MatriculaPensiones apoyo = (MatriculaPensiones) session.get(MatriculaPensiones.class,id);
       
        
        tx.commit();
        session.close();
        return apoyo;
    }
   
     
}
