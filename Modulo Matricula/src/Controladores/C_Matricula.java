
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Conector.Conexion;
import Modelo.Alumno;
import Modelo.Matricula;
import Modelo.MatriculaId;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;

/**
 *
 * @author Richard
 */
public class C_Matricula {
   
    public static List<Matricula> lista(String tipo) {

        List<Matricula> apoyo = null;

        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction(); 
        String alternativa = tipo.toLowerCase();
        switch (alternativa) {
            case "todo":
                apoyo = session.createQuery("from Matricula").list();
                break;
            case "activos":
                apoyo = session.createQuery("from Matricula where matriculaEstado  = true ").list();
                break;
            case "inactivos":
                apoyo = session.createQuery("from Matricula where matriculaEstado  = false ").list();
                break;
        }
        tx.commit();
        session.close();

        return apoyo;
    }

    public static List<Matricula> matriculasDe(String codigo)
    {
        
         List<Matricula> apoyo = null;

        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction(); 
        
        apoyo = session.createQuery("from Matricula where id.alumnoCodigo  = '"+codigo+"' ").list();
     
        tx.commit();
        session.close();

        return apoyo;
    }
     /* public static List<Alumno> listaGrado(String año,String nivel, String grado, Character seccion) {

        List<Alumno> apoyo = null;

        Session session = Conexion.getSessionFactory().openSession();
       
        apoyo = session.createQuery("from Alumno a where exists (from"+
" Matricula m where a.alumnoCodigo = m.id.alumnoCodigo and "+
                "m.id.periodoAno = '"+año+"' and "+
                "m.matriculaNivel = '"+nivel+"' and "+
                "m.matriculaGrado = '"+grado+"' and "+
                "m.matriculaSeccion = '"+seccion+"') order by a.alumnoApellidos").list();
           
        session.close();

        return apoyo;
    }*/
    
    public static void nuevo(Matricula nuevo) {
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();

        Transaction tx = session.beginTransaction();

        session.insert(nuevo);
    
        tx.commit();

        session.close();
    }

    public static Matricula ver(MatriculaId id)
    {
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();

        Transaction tx = session.beginTransaction();
        Matricula apoyo = (Matricula) session.get(Matricula.class, id);

      
        tx.commit();
        session.close();
        
        return apoyo;
    }
    public static void modificar(Matricula matricula) {
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();

        Transaction tx = session.beginTransaction();
        session.update(matricula);
        tx.commit();
        session.close();
    }
 
    
}
