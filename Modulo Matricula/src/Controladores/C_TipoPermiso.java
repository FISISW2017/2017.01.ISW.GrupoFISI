/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*          FAC_001 - 11/05/17 - inicio             */
/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
package Controladores;

/**
 *
 * @author Fabio
 */

import Conector.Conexion;
import Modelo.Alumno;
import Modelo.TipoPermisos;
import Modelo.Usuario;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;

public class C_TipoPermiso {
    
     public static TipoPermisos ver(char codPermiso) {

        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        TipoPermisos apoyo = (TipoPermisos) session.get(TipoPermisos.class, codPermiso);
      
        tx.commit();
        session.close();
        return apoyo;
    }
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*          FAC_001 - 11/05/17 - fin                */
/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
