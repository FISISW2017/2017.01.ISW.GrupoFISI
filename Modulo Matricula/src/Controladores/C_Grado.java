/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Conector.Conexion;
import Modelo.Alumno;
import Modelo.Grado;
import Modelo.GradoId;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;

/**
 *
 * @author Richard
 */
public class C_Grado {
    
     public static List<Grado> lista(String nivel) {

        List<Grado> apoyo = null;

        Session session = Conexion.getSessionFactory().openSession();
       Transaction tx = session.beginTransaction(); 
   
         apoyo = session.createQuery("from Grado where id.nivelId = '"+nivel+"' order by id.gradoId").list();
       
         tx.commit();
        session.close();

        return apoyo;
    }
   
     public static Grado ver(GradoId Id) {

        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        Grado apoyo = (Grado) session.get(Grado.class, Id);
      
        tx.commit();
        session.close();
        return apoyo;
    }

    public static void nuevo(Grado nuevo) {
         StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();

        session.insert(nuevo);
        
        tx.commit();

        session.close();
    }

    public static void modificar(Grado grado) {
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        session.update(grado);
        tx.commit();
        session.close();
    }
      
     
}
