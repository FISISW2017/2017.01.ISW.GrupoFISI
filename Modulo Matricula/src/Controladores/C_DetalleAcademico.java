/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Conector.Conexion;
import Modelo.DetalleAcademico;

import java.util.List;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;

/**
 *
 * @author Richard
 */
public class C_DetalleAcademico {

    public static List<DetalleAcademico> lista() {

        List<DetalleAcademico> apoyo = null;

        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction(); 
        apoyo = session.createQuery("from DetalleAcademico").list();
        tx.commit();
        session.close();

        return apoyo;
    }

    public static DetalleAcademico ver(String Id) {

        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        DetalleAcademico apoyo = (DetalleAcademico) session.get(DetalleAcademico.class, Id);

        tx.commit();
        session.close();
        return apoyo;
    }

    public static void nuevo(DetalleAcademico nuevo) {
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();

        session.insert(nuevo);

        tx.commit();

        session.close();
    }

    public static void modificar(DetalleAcademico alumno) {
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        session.update(alumno);
        tx.commit();
        session.close();
    }

}
