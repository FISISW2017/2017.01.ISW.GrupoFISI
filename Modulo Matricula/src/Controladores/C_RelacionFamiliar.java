/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Conector.Conexion;
import Modelo.Familiar;
import Modelo.RelacionFamiliar;
import Modelo.RelacionFamiliarId;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;

/**
 *
 * @author Richard
 */
public class C_RelacionFamiliar {
    
    
    public static List<Familiar> familiarDe(String id)
    {
        List<Familiar> apoyo = null;

        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        
        Transaction tx = session.beginTransaction(); 
                apoyo = session.createQuery("from RelacionFamiliar where id.alumnoCodigo = '"+id+"'").list();
     
        tx.commit();
        session.close();

        return apoyo;
    }
    
    public static void nuevo(RelacionFamiliar nuevo) {
        Session session = Conexion.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();

        session.save(nuevo);
        session.flush();
        tx.commit();

        session.close();
    }

    public static void modificar(RelacionFamiliar apoderado) {
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        session.update(apoderado);
     
       
        tx.commit();
        session.close();
    }  
      
    public static RelacionFamiliar ver(RelacionFamiliarId Id) {

        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        RelacionFamiliar apoyo = (RelacionFamiliar) session.get(RelacionFamiliar.class, Id);
        
        tx.commit();
        session.close();
        return apoyo;
    }
    
}
