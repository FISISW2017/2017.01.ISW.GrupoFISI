/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Conector.Conexion;
import Modelo.Alumno;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;

/**
 *
 * @author Richard
 */
public class C_Alumno {

    public static List<Alumno> lista(String tipo) {

        List<Alumno> apoyo = null;

        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction(); 
        String alternativa = tipo.toLowerCase();
        switch (alternativa) {
            case "todo":
                apoyo = session.createQuery("from Alumno").list();
                break;
            case "deudores":
                apoyo = session.createQuery("from Alumno where alumnoDeudas > 0").list();
                break;
            case "activos":
                apoyo = session.createQuery("from Alumno where alumnoEstado = true").list();
                break;
            case "inactivos":
                apoyo = session.createQuery("from Alumno where alumnoEstado = false").list();
                break;
        }
        tx.commit();
        session.close();

        return apoyo;
    }

    public static Alumno alumno(String apellidos, String nombres) {

        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
       Transaction tx = session.beginTransaction(); 
        List<Alumno> apoyo = session.createQuery("from Alumno where "
                + "alumnoApellidos = '" + apellidos + "' and "
                + "alumnoNombres = '" + nombres + "'").list();;
        tx.commit();
        session.close();
        if (apoyo.size() > 0) {
            return apoyo.get(0);
        } else {
            return null;
        }
    }

    public static Alumno alumno(String dni) {

        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction(); 
        List<Alumno> apoyo = session.createQuery("from Alumno where "
                + "alumnoDni = '" + dni + "'").list();
   
        tx.commit();
       session.close();
        if (apoyo.size() > 0) {
            return apoyo.get(0);
        } else {
            return null;
        }
    }

    public static Alumno ver(String Id) {

        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        Alumno apoyo = (Alumno) session.get(Alumno.class, Id);
      
        tx.commit();
        session.close();
        return apoyo;
    }

    public static void nuevo(Alumno nuevo) {
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();

        session.insert(nuevo);
        
        tx.commit();

        session.close();
    }

    public static void modificar(Alumno alumno) {
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        session.update(alumno);
        tx.commit();
        session.close();
    }

 

   

    
}
