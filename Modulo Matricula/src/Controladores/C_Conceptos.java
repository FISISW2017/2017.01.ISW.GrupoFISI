/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Conector.Conexion;
import Modelo.Concepto;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;

/**
 *
 * @author Richard
 */
public class C_Conceptos {
     public static List<Concepto> lista(String tipo) {

        List<Concepto> apoyo = null;

       StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
       Transaction tx = session.beginTransaction(); 
       String alternativa = tipo.toLowerCase();
        switch (alternativa) {
            case "todo":
                apoyo = session.createQuery("from Concepto").list();
                break;
            case "activos":
                apoyo = session.createQuery("from Concepto where conceptoEstado  = true").list();
                break;
            case "inactivos":
                apoyo = session.createQuery("from Concepto where conceptoEstado = false").list();
                break;
            case "fijos":
                apoyo = session.createQuery("from Concepto where conceptoObligatorio = true").list();
                break;
        }
        tx.commit();
        session.close();

        return apoyo;
    }
     
     
     
 public static Concepto ver(String Id) {

        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        Concepto apoyo = (Concepto) session.get(Concepto.class, Id);
      
        tx.commit();
        session.close();
        return apoyo;
    }
 
    
    public static void nuevo(Concepto nuevo) {
       StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();

        session.insert(nuevo);
        
        tx.commit();

        session.close();
    }

    public static void modificar(Concepto concepto) {
        StatelessSession session = Conexion.getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();
        session.update(concepto);
        tx.commit();
        session.close();
    }
   
    
    
}
