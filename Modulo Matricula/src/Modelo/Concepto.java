package Modelo;
// Generated 24/05/2017 09:38:28 PM by Hibernate Tools 4.3.1


import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * Concepto generated by hbm2java
 */
public class Concepto  implements java.io.Serializable {


     private String conceptoCodigo;
     private String conceptoDescripcion;
     private BigDecimal conceptoCosto;
     private boolean conceptoEstado;
     private boolean conceptoObligatorio;
     private Set matriculaConceptoses = new HashSet(0);

    public Concepto() {
    }

	
    public Concepto(String conceptoCodigo, String conceptoDescripcion, BigDecimal conceptoCosto, boolean conceptoEstado, boolean conceptoObligatorio) {
        this.conceptoCodigo = conceptoCodigo;
        this.conceptoDescripcion = conceptoDescripcion;
        this.conceptoCosto = conceptoCosto;
        this.conceptoEstado = conceptoEstado;
        this.conceptoObligatorio = conceptoObligatorio;
    }
    public Concepto(String conceptoCodigo, String conceptoDescripcion, BigDecimal conceptoCosto, boolean conceptoEstado, boolean conceptoObligatorio, Set matriculaConceptoses) {
       this.conceptoCodigo = conceptoCodigo;
       this.conceptoDescripcion = conceptoDescripcion;
       this.conceptoCosto = conceptoCosto;
       this.conceptoEstado = conceptoEstado;
       this.conceptoObligatorio = conceptoObligatorio;
       this.matriculaConceptoses = matriculaConceptoses;
    }
   
    public String getConceptoCodigo() {
        return this.conceptoCodigo;
    }
    
    public void setConceptoCodigo(String conceptoCodigo) {
        this.conceptoCodigo = conceptoCodigo;
    }
    public String getConceptoDescripcion() {
        return this.conceptoDescripcion;
    }
    
    public void setConceptoDescripcion(String conceptoDescripcion) {
        this.conceptoDescripcion = conceptoDescripcion;
    }
    public BigDecimal getConceptoCosto() {
        return this.conceptoCosto;
    }
    
    public void setConceptoCosto(BigDecimal conceptoCosto) {
        this.conceptoCosto = conceptoCosto;
    }
    public boolean isConceptoEstado() {
        return this.conceptoEstado;
    }
    
    public void setConceptoEstado(boolean conceptoEstado) {
        this.conceptoEstado = conceptoEstado;
    }
    public boolean isConceptoObligatorio() {
        return this.conceptoObligatorio;
    }
    
    public void setConceptoObligatorio(boolean conceptoObligatorio) {
        this.conceptoObligatorio = conceptoObligatorio;
    }
    public Set getMatriculaConceptoses() {
        return this.matriculaConceptoses;
    }
    
    public void setMatriculaConceptoses(Set matriculaConceptoses) {
        this.matriculaConceptoses = matriculaConceptoses;
    }




}


