/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista.VacantesSecciones;

import Adicionales.Estilos.Icono;
import Estilo.Plantilla;
import Modelo.Grado;
import Servicios.S_NivelGrados;
import Servicios.S_Periodo;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Richard
 */
public class C_Vacantes extends javax.swing.JFrame {

    private List<Grado> inicial = S_NivelGrados.losGrados("INICIAL");
    private List<Grado> primaria = S_NivelGrados.losGrados("PRIMARIA");
    private List<Grado> secundaria = S_NivelGrados.losGrados("SECUNDARIA");

    public C_Vacantes() {
        initComponents();

        cargarInicial();
        cargarPrimaria();
        new Estilo.EstiloBoton().Normal(B1, "verde");
        new Estilo.EstiloBoton().Normal(B2, "naranja");

    }

    private void cargarInicial() {

        V1.setValue(inicial.get(0).getVacantesCapacidad());
        S1.setValue(inicial.get(0).getCantidadSecciones());

        V2.setValue(inicial.get(1).getVacantesCapacidad());
        S2.setValue(inicial.get(1).getCantidadSecciones());

        V3.setValue(inicial.get(2).getVacantesCapacidad());
        S3.setValue(inicial.get(2).getCantidadSecciones());

        V4.setValue(inicial.get(3).getVacantesCapacidad());
        S4.setValue(inicial.get(3).getCantidadSecciones());
    }

    private void modificarInicial() {
        inicial.get(0).setCantidadSecciones((int) S1.getValue());
        inicial.get(0).setVacantesCapacidad((int) V1.getValue());
        inicial.get(0).setGradoCapSec(((int) V1.getValue()) / (int) S1.getValue());

        inicial.get(1).setCantidadSecciones((int) S2.getValue());
        inicial.get(1).setVacantesCapacidad((int) V2.getValue());
        inicial.get(1).setGradoCapSec(((int) V2.getValue()) / (int) S2.getValue());

        inicial.get(2).setCantidadSecciones((int) S3.getValue());
        inicial.get(2).setVacantesCapacidad((int) V3.getValue());
        inicial.get(2).setGradoCapSec(((int) V3.getValue()) / (int) S3.getValue());

        inicial.get(3).setCantidadSecciones((int) S4.getValue());
        inicial.get(3).setVacantesCapacidad((int) V4.getValue());
        inicial.get(3).setGradoCapSec(((int) V4.getValue()) / (int) S4.getValue());

        S_NivelGrados.modificarVacantesSecciones(inicial);
    }

    private void cargarPrimaria() {

        V5.setValue(primaria.get(0).getVacantesCapacidad());
        S5.setValue(primaria.get(0).getCantidadSecciones());

        V6.setValue(primaria.get(1).getVacantesCapacidad());
        S6.setValue(primaria.get(1).getCantidadSecciones());

        V7.setValue(primaria.get(2).getVacantesCapacidad());
        S7.setValue(primaria.get(2).getCantidadSecciones());

        V8.setValue(primaria.get(3).getVacantesCapacidad());
        S8.setValue(primaria.get(3).getCantidadSecciones());

        V9.setValue(primaria.get(4).getVacantesCapacidad());
        S9.setValue(primaria.get(4).getCantidadSecciones());

        V10.setValue(primaria.get(5).getVacantesCapacidad());
        S10.setValue(primaria.get(5).getCantidadSecciones());

    }

    private void modificarPrimaria() {
        primaria.get(0).setCantidadSecciones((int) S5.getValue());
        primaria.get(0).setVacantesCapacidad((int) V5.getValue());
        primaria.get(0).setGradoCapSec(((int) V5.getValue()) / (int) S5.getValue());

        primaria.get(1).setCantidadSecciones((int) S6.getValue());
        primaria.get(1).setVacantesCapacidad((int) V6.getValue());
        primaria.get(1).setGradoCapSec(((int) V6.getValue()) / (int) S6.getValue());

        primaria.get(2).setCantidadSecciones((int) S7.getValue());
        primaria.get(2).setVacantesCapacidad((int) V7.getValue());
        primaria.get(2).setGradoCapSec(((int) V7.getValue()) / (int) S7.getValue());

        primaria.get(3).setCantidadSecciones((int) S8.getValue());
        primaria.get(3).setVacantesCapacidad((int) V8.getValue());
        primaria.get(3).setGradoCapSec(((int) V8.getValue()) / (int) S8.getValue());

        primaria.get(4).setCantidadSecciones((int) S9.getValue());
        primaria.get(4).setVacantesCapacidad((int) V9.getValue());
        primaria.get(4).setGradoCapSec(((int) V9.getValue()) / (int) S9.getValue());

        primaria.get(5).setCantidadSecciones((int) S10.getValue());
        primaria.get(5).setVacantesCapacidad((int) V10.getValue());
        primaria.get(5).setGradoCapSec(((int) V10.getValue()) / (int) S10.getValue());

        S_NivelGrados.modificarVacantesSecciones(primaria);
    }

    private void cargarSecundaria() {

        V11.setValue(secundaria.get(0).getVacantesCapacidad());
        S11.setValue(secundaria.get(0).getCantidadSecciones());

        V12.setValue(secundaria.get(1).getVacantesCapacidad());
        S12.setValue(secundaria.get(1).getCantidadSecciones());

        V13.setValue(secundaria.get(2).getVacantesCapacidad());
        S13.setValue(secundaria.get(2).getCantidadSecciones());

        V14.setValue(secundaria.get(3).getVacantesCapacidad());
        S14.setValue(secundaria.get(3).getCantidadSecciones());

        V15.setValue(secundaria.get(4).getVacantesCapacidad());
        S15.setValue(secundaria.get(4).getCantidadSecciones());
    }

    private void modificarSecundaria() {
        secundaria.get(0).setCantidadSecciones((int) S11.getValue());
        secundaria.get(0).setVacantesCapacidad((int) V11.getValue());
        secundaria.get(0).setGradoCapSec(((int) V11.getValue()) / (int) S11.getValue());

        secundaria.get(1).setCantidadSecciones((int) S12.getValue());
        secundaria.get(1).setVacantesCapacidad((int) V12.getValue());
        secundaria.get(1).setGradoCapSec(((int) V12.getValue()) / (int) S12.getValue());

        secundaria.get(2).setCantidadSecciones((int) S13.getValue());
        secundaria.get(2).setVacantesCapacidad((int) V13.getValue());
        secundaria.get(2).setGradoCapSec(((int) V13.getValue()) / (int) S13.getValue());

        secundaria.get(3).setCantidadSecciones((int) S14.getValue());
        secundaria.get(3).setVacantesCapacidad((int) V14.getValue());
        secundaria.get(3).setGradoCapSec(((int) V14.getValue()) / (int) S14.getValue());

        secundaria.get(4).setCantidadSecciones((int) S15.getValue());
        secundaria.get(4).setVacantesCapacidad((int) V15.getValue());
        secundaria.get(4).setGradoCapSec(((int) V15.getValue()) / (int) S15.getValue());

        S_NivelGrados.modificarVacantesSecciones(secundaria);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        V1 = new javax.swing.JSpinner();
        S1 = new javax.swing.JSpinner();
        jLabel3 = new javax.swing.JLabel();
        V2 = new javax.swing.JSpinner();
        S2 = new javax.swing.JSpinner();
        jLabel4 = new javax.swing.JLabel();
        S3 = new javax.swing.JSpinner();
        V3 = new javax.swing.JSpinner();
        jLabel5 = new javax.swing.JLabel();
        S4 = new javax.swing.JSpinner();
        V4 = new javax.swing.JSpinner();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        V5 = new javax.swing.JSpinner();
        S5 = new javax.swing.JSpinner();
        jLabel11 = new javax.swing.JLabel();
        V6 = new javax.swing.JSpinner();
        S6 = new javax.swing.JSpinner();
        jLabel12 = new javax.swing.JLabel();
        S7 = new javax.swing.JSpinner();
        V7 = new javax.swing.JSpinner();
        jLabel13 = new javax.swing.JLabel();
        S8 = new javax.swing.JSpinner();
        V8 = new javax.swing.JSpinner();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        S9 = new javax.swing.JSpinner();
        V9 = new javax.swing.JSpinner();
        V10 = new javax.swing.JSpinner();
        S10 = new javax.swing.JSpinner();
        jLabel19 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        V11 = new javax.swing.JSpinner();
        S11 = new javax.swing.JSpinner();
        jLabel21 = new javax.swing.JLabel();
        V12 = new javax.swing.JSpinner();
        S12 = new javax.swing.JSpinner();
        jLabel22 = new javax.swing.JLabel();
        S13 = new javax.swing.JSpinner();
        V13 = new javax.swing.JSpinner();
        jLabel23 = new javax.swing.JLabel();
        S14 = new javax.swing.JSpinner();
        V14 = new javax.swing.JSpinner();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        S15 = new javax.swing.JSpinner();
        V15 = new javax.swing.JSpinner();
        B1 = new javax.swing.JButton();
        B2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Configurar Vacantes y Secciones");

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));

        jLabel1.setBackground(new java.awt.Color(0, 102, 153));
        jLabel1.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("CONFIGURAR VACANTES Y SECCIONES");
        jLabel1.setOpaque(true);

        jPanel2.setBackground(new java.awt.Color(153, 153, 153));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        java.awt.GridBagLayout jPanel2Layout = new java.awt.GridBagLayout();
        jPanel2Layout.columnWidths = new int[] {0, 0, 0, 0, 0};
        jPanel2Layout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        jPanel2.setLayout(jPanel2Layout);

        jLabel2.setBackground(new java.awt.Color(153, 204, 255));
        jLabel2.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 16)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("2 Años");
        jLabel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel2.setMaximumSize(new java.awt.Dimension(90, 26));
        jLabel2.setMinimumSize(new java.awt.Dimension(90, 26));
        jLabel2.setOpaque(true);
        jLabel2.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel2.add(jLabel2, gridBagConstraints);

        V1.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        V1.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        V1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        V1.setMaximumSize(new java.awt.Dimension(100, 26));
        V1.setMinimumSize(new java.awt.Dimension(100, 26));
        V1.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        jPanel2.add(V1, gridBagConstraints);

        S1.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        S1.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10, 1));
        S1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        S1.setMaximumSize(new java.awt.Dimension(100, 26));
        S1.setMinimumSize(new java.awt.Dimension(100, 26));
        S1.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        jPanel2.add(S1, gridBagConstraints);

        jLabel3.setBackground(new java.awt.Color(153, 204, 255));
        jLabel3.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 16)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("3 Años");
        jLabel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel3.setMaximumSize(new java.awt.Dimension(90, 26));
        jLabel3.setMinimumSize(new java.awt.Dimension(90, 26));
        jLabel3.setOpaque(true);
        jLabel3.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        jPanel2.add(jLabel3, gridBagConstraints);

        V2.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        V2.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        V2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        V2.setMaximumSize(new java.awt.Dimension(100, 26));
        V2.setMinimumSize(new java.awt.Dimension(100, 26));
        V2.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        jPanel2.add(V2, gridBagConstraints);

        S2.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        S2.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10, 1));
        S2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        S2.setMaximumSize(new java.awt.Dimension(100, 26));
        S2.setMinimumSize(new java.awt.Dimension(100, 26));
        S2.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 6;
        jPanel2.add(S2, gridBagConstraints);

        jLabel4.setBackground(new java.awt.Color(153, 204, 255));
        jLabel4.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 16)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("4 Años");
        jLabel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel4.setMaximumSize(new java.awt.Dimension(90, 26));
        jLabel4.setMinimumSize(new java.awt.Dimension(90, 26));
        jLabel4.setOpaque(true);
        jLabel4.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        jPanel2.add(jLabel4, gridBagConstraints);

        S3.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        S3.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10, 1));
        S3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        S3.setMaximumSize(new java.awt.Dimension(100, 26));
        S3.setMinimumSize(new java.awt.Dimension(100, 26));
        S3.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 8;
        jPanel2.add(S3, gridBagConstraints);

        V3.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        V3.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        V3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        V3.setMaximumSize(new java.awt.Dimension(100, 26));
        V3.setMinimumSize(new java.awt.Dimension(100, 26));
        V3.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        jPanel2.add(V3, gridBagConstraints);

        jLabel5.setBackground(new java.awt.Color(153, 204, 255));
        jLabel5.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 16)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("5 Años");
        jLabel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel5.setMaximumSize(new java.awt.Dimension(90, 26));
        jLabel5.setMinimumSize(new java.awt.Dimension(90, 26));
        jLabel5.setOpaque(true);
        jLabel5.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        jPanel2.add(jLabel5, gridBagConstraints);

        S4.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        S4.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10, 1));
        S4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        S4.setMaximumSize(new java.awt.Dimension(100, 26));
        S4.setMinimumSize(new java.awt.Dimension(100, 26));
        S4.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 10;
        jPanel2.add(S4, gridBagConstraints);

        V4.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        V4.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        V4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        V4.setMaximumSize(new java.awt.Dimension(100, 26));
        V4.setMinimumSize(new java.awt.Dimension(100, 26));
        V4.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 10;
        jPanel2.add(V4, gridBagConstraints);

        jLabel6.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 14)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Grado");
        jLabel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel6.setMaximumSize(new java.awt.Dimension(90, 26));
        jLabel6.setMinimumSize(new java.awt.Dimension(90, 26));
        jLabel6.setOpaque(true);
        jLabel6.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel2.add(jLabel6, gridBagConstraints);

        jLabel8.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 14)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Vacantes");
        jLabel8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel8.setMaximumSize(new java.awt.Dimension(100, 26));
        jLabel8.setMinimumSize(new java.awt.Dimension(100, 26));
        jLabel8.setOpaque(true);
        jLabel8.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel2.add(jLabel8, gridBagConstraints);

        jLabel7.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 14)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("Secciones");
        jLabel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel7.setMaximumSize(new java.awt.Dimension(100, 26));
        jLabel7.setMinimumSize(new java.awt.Dimension(100, 26));
        jLabel7.setOpaque(true);
        jLabel7.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        jPanel2.add(jLabel7, gridBagConstraints);

        jLabel9.setBackground(new java.awt.Color(0, 102, 153));
        jLabel9.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("INICIAL");
        jLabel9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel9.setMaximumSize(new java.awt.Dimension(290, 26));
        jLabel9.setMinimumSize(new java.awt.Dimension(290, 26));
        jLabel9.setOpaque(true);
        jLabel9.setPreferredSize(new java.awt.Dimension(290, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 5;
        jPanel2.add(jLabel9, gridBagConstraints);

        jPanel3.setBackground(new java.awt.Color(153, 153, 153));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        java.awt.GridBagLayout jPanel3Layout = new java.awt.GridBagLayout();
        jPanel3Layout.columnWidths = new int[] {0, 0, 0, 0, 0};
        jPanel3Layout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        jPanel3.setLayout(jPanel3Layout);

        jLabel10.setBackground(new java.awt.Color(153, 204, 255));
        jLabel10.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 16)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("1° Grado");
        jLabel10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel10.setMaximumSize(new java.awt.Dimension(90, 26));
        jLabel10.setMinimumSize(new java.awt.Dimension(90, 26));
        jLabel10.setOpaque(true);
        jLabel10.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel3.add(jLabel10, gridBagConstraints);

        V5.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        V5.setModel(new javax.swing.SpinnerNumberModel());
        V5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        V5.setMaximumSize(new java.awt.Dimension(100, 26));
        V5.setMinimumSize(new java.awt.Dimension(100, 26));
        V5.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        jPanel3.add(V5, gridBagConstraints);

        S5.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        S5.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10, 1));
        S5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        S5.setMaximumSize(new java.awt.Dimension(100, 26));
        S5.setMinimumSize(new java.awt.Dimension(100, 26));
        S5.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        jPanel3.add(S5, gridBagConstraints);

        jLabel11.setBackground(new java.awt.Color(153, 204, 255));
        jLabel11.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 16)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("2° Grado");
        jLabel11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel11.setMaximumSize(new java.awt.Dimension(90, 26));
        jLabel11.setMinimumSize(new java.awt.Dimension(90, 26));
        jLabel11.setOpaque(true);
        jLabel11.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        jPanel3.add(jLabel11, gridBagConstraints);

        V6.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        V6.setModel(new javax.swing.SpinnerNumberModel());
        V6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        V6.setMaximumSize(new java.awt.Dimension(100, 26));
        V6.setMinimumSize(new java.awt.Dimension(100, 26));
        V6.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        jPanel3.add(V6, gridBagConstraints);

        S6.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        S6.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10, 1));
        S6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        S6.setMaximumSize(new java.awt.Dimension(100, 26));
        S6.setMinimumSize(new java.awt.Dimension(100, 26));
        S6.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 6;
        jPanel3.add(S6, gridBagConstraints);

        jLabel12.setBackground(new java.awt.Color(153, 204, 255));
        jLabel12.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 16)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("3° Grado");
        jLabel12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel12.setMaximumSize(new java.awt.Dimension(90, 26));
        jLabel12.setMinimumSize(new java.awt.Dimension(90, 26));
        jLabel12.setOpaque(true);
        jLabel12.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        jPanel3.add(jLabel12, gridBagConstraints);

        S7.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        S7.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10, 1));
        S7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        S7.setMaximumSize(new java.awt.Dimension(100, 26));
        S7.setMinimumSize(new java.awt.Dimension(100, 26));
        S7.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 8;
        jPanel3.add(S7, gridBagConstraints);

        V7.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        V7.setModel(new javax.swing.SpinnerNumberModel());
        V7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        V7.setMaximumSize(new java.awt.Dimension(100, 26));
        V7.setMinimumSize(new java.awt.Dimension(100, 26));
        V7.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        jPanel3.add(V7, gridBagConstraints);

        jLabel13.setBackground(new java.awt.Color(153, 204, 255));
        jLabel13.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 16)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("4° Grado");
        jLabel13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel13.setMaximumSize(new java.awt.Dimension(90, 26));
        jLabel13.setMinimumSize(new java.awt.Dimension(90, 26));
        jLabel13.setOpaque(true);
        jLabel13.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        jPanel3.add(jLabel13, gridBagConstraints);

        S8.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        S8.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10, 1));
        S8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        S8.setMaximumSize(new java.awt.Dimension(100, 26));
        S8.setMinimumSize(new java.awt.Dimension(100, 26));
        S8.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 10;
        jPanel3.add(S8, gridBagConstraints);

        V8.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        V8.setModel(new javax.swing.SpinnerNumberModel());
        V8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        V8.setMaximumSize(new java.awt.Dimension(100, 26));
        V8.setMinimumSize(new java.awt.Dimension(100, 26));
        V8.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 10;
        jPanel3.add(V8, gridBagConstraints);

        jLabel14.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 14)); // NOI18N
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel14.setText("Grado");
        jLabel14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel14.setMaximumSize(new java.awt.Dimension(90, 26));
        jLabel14.setMinimumSize(new java.awt.Dimension(90, 26));
        jLabel14.setOpaque(true);
        jLabel14.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel3.add(jLabel14, gridBagConstraints);

        jLabel15.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 14)); // NOI18N
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel15.setText("Vacantes");
        jLabel15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel15.setMaximumSize(new java.awt.Dimension(100, 26));
        jLabel15.setMinimumSize(new java.awt.Dimension(100, 26));
        jLabel15.setOpaque(true);
        jLabel15.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel3.add(jLabel15, gridBagConstraints);

        jLabel16.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 14)); // NOI18N
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel16.setText("Secciones");
        jLabel16.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel16.setMaximumSize(new java.awt.Dimension(100, 26));
        jLabel16.setMinimumSize(new java.awt.Dimension(100, 26));
        jLabel16.setOpaque(true);
        jLabel16.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        jPanel3.add(jLabel16, gridBagConstraints);

        jLabel17.setBackground(new java.awt.Color(0, 102, 153));
        jLabel17.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 18)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel17.setText("PRIMARIA");
        jLabel17.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel17.setMaximumSize(new java.awt.Dimension(290, 26));
        jLabel17.setMinimumSize(new java.awt.Dimension(290, 26));
        jLabel17.setOpaque(true);
        jLabel17.setPreferredSize(new java.awt.Dimension(290, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 5;
        jPanel3.add(jLabel17, gridBagConstraints);

        jLabel18.setBackground(new java.awt.Color(153, 204, 255));
        jLabel18.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 16)); // NOI18N
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel18.setText("5° Grado");
        jLabel18.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel18.setMaximumSize(new java.awt.Dimension(90, 26));
        jLabel18.setMinimumSize(new java.awt.Dimension(90, 26));
        jLabel18.setOpaque(true);
        jLabel18.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        jPanel3.add(jLabel18, gridBagConstraints);

        S9.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        S9.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10, 1));
        S9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        S9.setMaximumSize(new java.awt.Dimension(100, 26));
        S9.setMinimumSize(new java.awt.Dimension(100, 26));
        S9.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 12;
        jPanel3.add(S9, gridBagConstraints);

        V9.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        V9.setModel(new javax.swing.SpinnerNumberModel());
        V9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        V9.setMaximumSize(new java.awt.Dimension(100, 26));
        V9.setMinimumSize(new java.awt.Dimension(100, 26));
        V9.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 12;
        jPanel3.add(V9, gridBagConstraints);

        V10.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        V10.setModel(new javax.swing.SpinnerNumberModel());
        V10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        V10.setMaximumSize(new java.awt.Dimension(100, 26));
        V10.setMinimumSize(new java.awt.Dimension(100, 26));
        V10.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 14;
        jPanel3.add(V10, gridBagConstraints);

        S10.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        S10.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10, 1));
        S10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        S10.setMaximumSize(new java.awt.Dimension(100, 26));
        S10.setMinimumSize(new java.awt.Dimension(100, 26));
        S10.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 14;
        jPanel3.add(S10, gridBagConstraints);

        jLabel19.setBackground(new java.awt.Color(153, 204, 255));
        jLabel19.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 16)); // NOI18N
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel19.setText("6° Grado");
        jLabel19.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel19.setMaximumSize(new java.awt.Dimension(90, 26));
        jLabel19.setMinimumSize(new java.awt.Dimension(90, 26));
        jLabel19.setOpaque(true);
        jLabel19.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 14;
        jPanel3.add(jLabel19, gridBagConstraints);

        jPanel4.setBackground(new java.awt.Color(153, 153, 153));
        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel4.setLayout(new java.awt.GridBagLayout());

        jLabel20.setBackground(new java.awt.Color(153, 204, 255));
        jLabel20.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 16)); // NOI18N
        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel20.setText("1° Grado");
        jLabel20.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel20.setMaximumSize(new java.awt.Dimension(90, 26));
        jLabel20.setMinimumSize(new java.awt.Dimension(90, 26));
        jLabel20.setOpaque(true);
        jLabel20.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel4.add(jLabel20, gridBagConstraints);

        V11.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        V11.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        V11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        V11.setMaximumSize(new java.awt.Dimension(100, 26));
        V11.setMinimumSize(new java.awt.Dimension(100, 26));
        V11.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        jPanel4.add(V11, gridBagConstraints);

        S11.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        S11.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10, 1));
        S11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        S11.setMaximumSize(new java.awt.Dimension(100, 26));
        S11.setMinimumSize(new java.awt.Dimension(100, 26));
        S11.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        jPanel4.add(S11, gridBagConstraints);

        jLabel21.setBackground(new java.awt.Color(153, 204, 255));
        jLabel21.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 16)); // NOI18N
        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("2° Grado");
        jLabel21.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel21.setMaximumSize(new java.awt.Dimension(90, 26));
        jLabel21.setMinimumSize(new java.awt.Dimension(90, 26));
        jLabel21.setOpaque(true);
        jLabel21.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        jPanel4.add(jLabel21, gridBagConstraints);

        V12.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        V12.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        V12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        V12.setMaximumSize(new java.awt.Dimension(100, 26));
        V12.setMinimumSize(new java.awt.Dimension(100, 26));
        V12.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        jPanel4.add(V12, gridBagConstraints);

        S12.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        S12.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10, 1));
        S12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        S12.setMaximumSize(new java.awt.Dimension(100, 26));
        S12.setMinimumSize(new java.awt.Dimension(100, 26));
        S12.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 6;
        jPanel4.add(S12, gridBagConstraints);

        jLabel22.setBackground(new java.awt.Color(153, 204, 255));
        jLabel22.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 16)); // NOI18N
        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel22.setText("3° Grado");
        jLabel22.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel22.setMaximumSize(new java.awt.Dimension(90, 26));
        jLabel22.setMinimumSize(new java.awt.Dimension(90, 26));
        jLabel22.setOpaque(true);
        jLabel22.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        jPanel4.add(jLabel22, gridBagConstraints);

        S13.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        S13.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10, 1));
        S13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        S13.setMaximumSize(new java.awt.Dimension(100, 26));
        S13.setMinimumSize(new java.awt.Dimension(100, 26));
        S13.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 8;
        jPanel4.add(S13, gridBagConstraints);

        V13.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        V13.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        V13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        V13.setMaximumSize(new java.awt.Dimension(100, 26));
        V13.setMinimumSize(new java.awt.Dimension(100, 26));
        V13.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        jPanel4.add(V13, gridBagConstraints);

        jLabel23.setBackground(new java.awt.Color(153, 204, 255));
        jLabel23.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 16)); // NOI18N
        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel23.setText("4° Grado");
        jLabel23.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel23.setMaximumSize(new java.awt.Dimension(90, 26));
        jLabel23.setMinimumSize(new java.awt.Dimension(90, 26));
        jLabel23.setOpaque(true);
        jLabel23.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        jPanel4.add(jLabel23, gridBagConstraints);

        S14.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        S14.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10, 1));
        S14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        S14.setMaximumSize(new java.awt.Dimension(100, 26));
        S14.setMinimumSize(new java.awt.Dimension(100, 26));
        S14.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 10;
        jPanel4.add(S14, gridBagConstraints);

        V14.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        V14.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        V14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        V14.setMaximumSize(new java.awt.Dimension(100, 26));
        V14.setMinimumSize(new java.awt.Dimension(100, 26));
        V14.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 10;
        jPanel4.add(V14, gridBagConstraints);

        jLabel24.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 14)); // NOI18N
        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel24.setText("Grado");
        jLabel24.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel24.setMaximumSize(new java.awt.Dimension(90, 26));
        jLabel24.setMinimumSize(new java.awt.Dimension(90, 26));
        jLabel24.setOpaque(true);
        jLabel24.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel4.add(jLabel24, gridBagConstraints);

        jLabel25.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 14)); // NOI18N
        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel25.setText("Vacantes");
        jLabel25.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel25.setMaximumSize(new java.awt.Dimension(100, 26));
        jLabel25.setMinimumSize(new java.awt.Dimension(100, 26));
        jLabel25.setOpaque(true);
        jLabel25.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel4.add(jLabel25, gridBagConstraints);

        jLabel26.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 14)); // NOI18N
        jLabel26.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel26.setText("Secciones");
        jLabel26.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel26.setMaximumSize(new java.awt.Dimension(100, 26));
        jLabel26.setMinimumSize(new java.awt.Dimension(100, 26));
        jLabel26.setOpaque(true);
        jLabel26.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        jPanel4.add(jLabel26, gridBagConstraints);

        jLabel27.setBackground(new java.awt.Color(0, 102, 153));
        jLabel27.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 18)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(255, 255, 255));
        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel27.setText("SECUNDARIA");
        jLabel27.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel27.setMaximumSize(new java.awt.Dimension(290, 26));
        jLabel27.setMinimumSize(new java.awt.Dimension(290, 26));
        jLabel27.setOpaque(true);
        jLabel27.setPreferredSize(new java.awt.Dimension(290, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 5;
        jPanel4.add(jLabel27, gridBagConstraints);

        jLabel28.setBackground(new java.awt.Color(153, 204, 255));
        jLabel28.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 16)); // NOI18N
        jLabel28.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel28.setText("5° Grado");
        jLabel28.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jLabel28.setMaximumSize(new java.awt.Dimension(90, 26));
        jLabel28.setMinimumSize(new java.awt.Dimension(90, 26));
        jLabel28.setOpaque(true);
        jLabel28.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        jPanel4.add(jLabel28, gridBagConstraints);

        S15.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        S15.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10, 1));
        S15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        S15.setMaximumSize(new java.awt.Dimension(100, 26));
        S15.setMinimumSize(new java.awt.Dimension(100, 26));
        S15.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 12;
        jPanel4.add(S15, gridBagConstraints);

        V15.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        V15.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        V15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        V15.setMaximumSize(new java.awt.Dimension(100, 26));
        V15.setMinimumSize(new java.awt.Dimension(100, 26));
        V15.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 12;
        jPanel4.add(V15, gridBagConstraints);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        B1.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        B1.setText("Modificar");
        B1.setFocusPainted(false);
        B1.setMaximumSize(new java.awt.Dimension(133, 36));
        B1.setMinimumSize(new java.awt.Dimension(133, 36));
        B1.setPreferredSize(new java.awt.Dimension(133, 36));
        B1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B1ActionPerformed(evt);
            }
        });

        B2.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        B2.setText("Cancelar");
        B2.setFocusPainted(false);
        B2.setMaximumSize(new java.awt.Dimension(133, 36));
        B2.setMinimumSize(new java.awt.Dimension(133, 36));
        B2.setPreferredSize(new java.awt.Dimension(133, 36));
        B2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(B1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(B2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(B2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(B1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void B1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B1ActionPerformed
        
        Plantilla.PanelesAuxliares();
        int valor = JOptionPane.showConfirmDialog(this, "Esta seguro de modificar las vacantes\n"
                + "y secciones.", "CONFIRMACION", JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE, new Icono().confirmar()
        );

        if (valor == JOptionPane.OK_OPTION) {

            modificarInicial();
            modificarPrimaria();
            modificarSecundaria();
            JOptionPane.showMessageDialog(this,
                      "Se ha modificado de forma correcta las vacantes y.\n"
                    + "secciones para cada grado."
                   ,
                    "MENSAJE", JOptionPane.INFORMATION_MESSAGE,
                    new Icono().aceptar());
        }
    }//GEN-LAST:event_B1ActionPerformed

    private void B2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B2ActionPerformed

        this.dispose();
    }//GEN-LAST:event_B2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new C_Vacantes().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton B1;
    private javax.swing.JButton B2;
    private javax.swing.JSpinner S1;
    private javax.swing.JSpinner S10;
    private javax.swing.JSpinner S11;
    private javax.swing.JSpinner S12;
    private javax.swing.JSpinner S13;
    private javax.swing.JSpinner S14;
    private javax.swing.JSpinner S15;
    private javax.swing.JSpinner S2;
    private javax.swing.JSpinner S3;
    private javax.swing.JSpinner S4;
    private javax.swing.JSpinner S5;
    private javax.swing.JSpinner S6;
    private javax.swing.JSpinner S7;
    private javax.swing.JSpinner S8;
    private javax.swing.JSpinner S9;
    private javax.swing.JSpinner V1;
    private javax.swing.JSpinner V10;
    private javax.swing.JSpinner V11;
    private javax.swing.JSpinner V12;
    private javax.swing.JSpinner V13;
    private javax.swing.JSpinner V14;
    private javax.swing.JSpinner V15;
    private javax.swing.JSpinner V2;
    private javax.swing.JSpinner V3;
    private javax.swing.JSpinner V4;
    private javax.swing.JSpinner V5;
    private javax.swing.JSpinner V6;
    private javax.swing.JSpinner V7;
    private javax.swing.JSpinner V8;
    private javax.swing.JSpinner V9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    // End of variables declaration//GEN-END:variables
}
