/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista.Conceptos;

import Adicionales.Estilos.Icono;
import Controladores.C_Periodo;
import Estilo.EstiloBoton;
import Estilo.EstiloSpinner;
import Modelo.Concepto;
import Servicios.S_Periodo;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import javax.swing.JOptionPane;

/**
 *
 * @author Richard
 */
public class M_Conceptos extends javax.swing.JFrame {

    private P_Conceptos principal = null;
    private String codigo;
    public M_Conceptos() {
        Estilo.Plantilla.basica();
        initComponents();
        setLocationRelativeTo(null);
        new EstiloBoton().Normal(C1, "naranja");
        new EstiloBoton().Normal(M1, "verde");
        new EstiloSpinner().sinEstilo(S1);
    }

    public void setPrincipal(P_Conceptos principal) {
        this.principal = principal;
        
    }

    public void setValores(String codigo,String descripcion, double valor)
    {
        this.codigo = codigo;
        E2.setText(descripcion);
        S1.setValue(valor);
    }
    
    private void Modificar() {
       
        
        String descripcion = E2.getText().toUpperCase();
        double costo = Double.parseDouble(""+S1.getValue());

        Concepto concepto = S_Periodo.buscarConcepto(codigo);
        concepto.setConceptoCosto(BigDecimal.valueOf(costo));
        concepto.setConceptoDescripcion(descripcion);

        S_Periodo.modificarConcepto(concepto);
        concepto = null;

    }

    private boolean excepciones() {
        if (E2.getText().length() < 5) {
            JOptionPane.showMessageDialog(this,
                    "La descricpión del concepto escolar\n"
                    + "es muy corta.",
                    "ERROR", JOptionPane.ERROR_MESSAGE,
                    new Icono().error());
            E2.requestFocus();
            return false;
         
        } else {
            return true;
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        E2 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        S1 = new javax.swing.JSpinner();
        jPanel2 = new javax.swing.JPanel();
        M1 = new javax.swing.JButton();
        C1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Modificar Concepto");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));

        jLabel2.setBackground(new java.awt.Color(0, 102, 153));
        jLabel2.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 22)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("MODIFICAR CONCEPTO");
        jLabel2.setOpaque(true);

        jLabel4.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 14)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel4.setText("Descripción:");

        E2.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        E2.setMargin(new java.awt.Insets(2, 5, 2, 5));
        E2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                E2KeyTyped(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel5.setText("Precio:");

        S1.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        S1.setModel(new javax.swing.SpinnerNumberModel(1.0d, 0.0d, 999.0d, 1.0d));
        S1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        S1.setEditor(new javax.swing.JSpinner.NumberEditor(S1, "0.00"));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 350, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(E2)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(S1, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(1, 1, 1)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(E2, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(S1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        jPanel2.setLayout(new java.awt.GridBagLayout());

        M1.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        M1.setText("Modificar");
        M1.setFocusPainted(false);
        M1.setMaximumSize(new java.awt.Dimension(133, 36));
        M1.setMinimumSize(new java.awt.Dimension(133, 36));
        M1.setPreferredSize(new java.awt.Dimension(133, 36));
        M1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                M1ActionPerformed(evt);
            }
        });
        jPanel2.add(M1, new java.awt.GridBagConstraints());

        C1.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        C1.setText("Cancelar");
        C1.setFocusPainted(false);
        C1.setMaximumSize(new java.awt.Dimension(133, 36));
        C1.setMinimumSize(new java.awt.Dimension(133, 36));
        C1.setPreferredSize(new java.awt.Dimension(133, 36));
        C1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                C1ActionPerformed(evt);
            }
        });
        jPanel2.add(C1, new java.awt.GridBagConstraints());

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void M1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_M1ActionPerformed

        if (excepciones()) {
            int valor = JOptionPane.showConfirmDialog(this,
                    "¿Esta seguro que desea modificar los campos\n"
                  + "del concepto?", "CONFIRMACION", JOptionPane.OK_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE, new Icono().confirmar()
            );

            if (valor == JOptionPane.OK_OPTION) {
                Modificar();
                JOptionPane.showMessageDialog(this,
                        "Se ha modificado el concepto escolar\n"
                        + "de forma correcta.",
                        "MENSAJE", JOptionPane.INFORMATION_MESSAGE,
                        new Icono().aceptar());
                this.dispose();
                principal.setEnabled(true);
                principal.requestFocus();
                
                principal.actualizarTabla();
                
            }
        }
    }//GEN-LAST:event_M1ActionPerformed

    private void C1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_C1ActionPerformed

        principal.setEnabled(true);
        principal.requestFocus();

    }//GEN-LAST:event_C1ActionPerformed

    private void E2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_E2KeyTyped

        char car = evt.getKeyChar();
        if ((car < 'a' || car > 'z') && (car < 'A' || car > 'Z') && car != 'Ñ' && car != 'ñ'
                && (car != (char) KeyEvent.VK_SPACE)) {
            evt.consume();

            if (E2.getText().length() == 30) {
                evt.consume();
            }
        }
    }//GEN-LAST:event_E2KeyTyped

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing

        principal.setEnabled(true);

        principal.requestFocus();        // TODO add your handling code here:
    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new M_Conceptos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton C1;
    private javax.swing.JTextField E2;
    private javax.swing.JButton M1;
    private javax.swing.JSpinner S1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    // End of variables declaration//GEN-END:variables
}
