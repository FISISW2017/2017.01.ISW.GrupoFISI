/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista.Conceptos;

import Adicionales.Estilos.Icono;
import Controladores.C_Periodo;
import Estilo.EstiloBoton;
import Estilo.EstiloSpinner;
import Estilo.EstiloTexto;
import Modelo.Concepto;
import Servicios.S_Periodo;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import javax.swing.JOptionPane;

/**
 *
 * @author Richard
 */
public class R_Conceptos extends javax.swing.JFrame {

    private P_Conceptos principal = null;

    public R_Conceptos() {
        initComponents();
        setLocationRelativeTo(null);
        estilo();
    }

    private void estilo() {
       // new EstiloTexto().Estandar("Máximo 6 letras", E1, false);
        //new EstiloTexto().Estandar("Nombre del Concepto", E2, false);
        new Estilo.EstiloBoton().Normal(R1, "verde");
        new Estilo.EstiloBoton().Normal(C1, "naranja");
        new EstiloSpinner().sinEstilo(S1);
    }

    public void setPrincipal(P_Conceptos principal) {
        this.principal = principal;
    }

    private void Registrar() {
        String id = E1.getText().toUpperCase();
        String descripcion = E2.getText().toUpperCase();
        double costo = Double.parseDouble(""+S1.getValue());

        Concepto concepto = new Concepto(id, descripcion, BigDecimal.valueOf(costo), true, false);

        S_Periodo.registrarConcepto(concepto);

    }

    private boolean excepciones() {
        if (E1.getText().length() < 6) {
            JOptionPane.showMessageDialog(this,
                    "el codigo del concepto debe tener 6\n"
                    + "caracteres.",
                    "ERROR", JOptionPane.ERROR_MESSAGE,
                    new Icono().error());
            E1.requestFocus();
            return false;
        } else if (E2.getText().length() < 5) {
            JOptionPane.showMessageDialog(this,
                    "La descricpión del concepto escolar\n"
                    + "es muy corta.",
                    "ERROR", JOptionPane.ERROR_MESSAGE,
                    new Icono().error());
            E2.requestFocus();
            return false;
        } else if (S_Periodo.existeConcepto(E1.getText())) {

            JOptionPane.showMessageDialog(this,
                    "El código escrito ya esta siendo usado,\n"
                    + "cambie de código.",
                    "ERROR", JOptionPane.ERROR_MESSAGE,
                    new Icono().error());
            E1.requestFocus();
            return false;
        } else {
            return true;
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        E1 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        E2 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        S1 = new javax.swing.JSpinner();
        jPanel2 = new javax.swing.JPanel();
        R1 = new javax.swing.JButton();
        C1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Registrar Concepto");
        setBackground(new java.awt.Color(255, 255, 255));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));

        jLabel2.setBackground(new java.awt.Color(0, 102, 153));
        jLabel2.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 22)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("REGISTRAR CONCEPTO");
        jLabel2.setOpaque(true);

        E1.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        E1.setForeground(new java.awt.Color(0, 102, 153));
        E1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        E1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        E1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                E1ActionPerformed(evt);
            }
        });
        E1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                E1KeyTyped(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 14)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel3.setText("Código: ");

        jLabel4.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 14)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel4.setText("Descripción:");

        E2.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        E2.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)), javax.swing.BorderFactory.createEmptyBorder(1, 5, 1, 5)));
        E2.setMargin(new java.awt.Insets(2, 5, 2, 5));
        E2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                E2KeyTyped(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel5.setText("Precio:");

        S1.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        S1.setModel(new javax.swing.SpinnerNumberModel(1.0d, 0.0d, 999.0d, 1.0d));
        S1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        S1.setEditor(new javax.swing.JSpinner.NumberEditor(S1, "0.00"));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(E1, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(S1)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(E2))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3))
                .addGap(1, 1, 1)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(E1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(S1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addGap(1, 1, 1)
                .addComponent(E2, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel2.setLayout(new java.awt.GridBagLayout());

        R1.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        R1.setText("Registrar");
        R1.setFocusPainted(false);
        R1.setMaximumSize(new java.awt.Dimension(133, 36));
        R1.setMinimumSize(new java.awt.Dimension(133, 36));
        R1.setPreferredSize(new java.awt.Dimension(133, 36));
        R1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                R1ActionPerformed(evt);
            }
        });
        jPanel2.add(R1, new java.awt.GridBagConstraints());

        C1.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        C1.setText("Cancelar");
        C1.setFocusPainted(false);
        C1.setMaximumSize(new java.awt.Dimension(133, 36));
        C1.setMinimumSize(new java.awt.Dimension(133, 36));
        C1.setPreferredSize(new java.awt.Dimension(133, 36));
        C1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                C1ActionPerformed(evt);
            }
        });
        jPanel2.add(C1, new java.awt.GridBagConstraints());

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void E1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_E1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_E1ActionPerformed

    private void R1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_R1ActionPerformed

        if (excepciones()) {
            int valor = JOptionPane.showConfirmDialog(this,
                    "¿Esta seguro que desea registrar un nuevo\n"
                    + "concepto?", "CONFIRMACION", JOptionPane.OK_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE, new Icono().confirmar()
            );

            if (valor == JOptionPane.OK_OPTION) {
                Registrar();
                JOptionPane.showMessageDialog(this,
                        "Se ha regsitrado el concepto escolar\n"
                        + "de forma correcta.",
                        "MENSAJE", JOptionPane.INFORMATION_MESSAGE,
                        new Icono().aceptar());
                this.dispose();
                principal.setEnabled(true);

                principal.requestFocus();
                principal.actualizarTabla();
            }
        }
    }//GEN-LAST:event_R1ActionPerformed

    private void C1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_C1ActionPerformed

        principal.setEnabled(true);
        principal.requestFocus();

    }//GEN-LAST:event_C1ActionPerformed

    private void E1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_E1KeyTyped
        char valor = evt.getKeyChar();
        if (valor == KeyEvent.VK_SPACE) {
            evt.consume();
        }

        if (E1.getText().length() == 6) {
            evt.consume();
        }

    }//GEN-LAST:event_E1KeyTyped

    private void E2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_E2KeyTyped

        char car = evt.getKeyChar();
        if ((car < 'a' || car > 'z') && (car < 'A' || car > 'Z') && car != 'Ñ' && car != 'ñ'
                && (car != (char) KeyEvent.VK_SPACE)) {
            evt.consume();

            if (E2.getText().length() == 30) {
                evt.consume();
            }
        }
    }//GEN-LAST:event_E2KeyTyped

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing

        principal.setEnabled(true);

        principal.requestFocus();        // TODO add your handling code here:
    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new R_Conceptos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton C1;
    private javax.swing.JTextField E1;
    private javax.swing.JTextField E2;
    private javax.swing.JButton R1;
    private javax.swing.JSpinner S1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    // End of variables declaration//GEN-END:variables
}
