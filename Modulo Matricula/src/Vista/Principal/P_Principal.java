/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista.Principal;

import Adicionales.Estilos.Icono;
import Servicios.S_Periodo;
import Vista.Conceptos.M_Conceptos;
import Vista.Conceptos.P_Conceptos;
import Vista.Documentos.P_Documentos;
import Vista.Periodo.M_Periodo;
import Vista.Periodo.R_Periodo;
import Vista.VacantesSecciones.C_Vacantes;
import java.awt.Color;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author Richard
 */
public class P_Principal extends javax.swing.JFrame {

    private String año = "" + (new Date().getYear() + 1900);

    public P_Principal() {
        Estilo.Plantilla.basica();
        initComponents();
        accionBoton(E1);
        accionBoton(E2);
        accionBoton(E3);
       
        accionBoton(E4);
        accionBoton(E6);
       

    }

    private void accionBoton(JLabel label) {
        label.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                label.setForeground(new Color(51, 153, 255));
                label.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 14));
            }

            @Override
            public void mouseExited(java.awt.event.MouseEvent evt) {
                label.setForeground(new Color(0, 0, 0));
                label.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14));
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Fondo = new javax.swing.JPanel();
        Contenedor = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        E1 = new javax.swing.JLabel();
        E2 = new javax.swing.JLabel();
        E3 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        E6 = new javax.swing.JLabel();
        E4 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        E7 = new javax.swing.JLabel();
        E8 = new javax.swing.JLabel();
        Cabecera = new javax.swing.JPanel();
        Termino = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        Fondo.setBackground(new java.awt.Color(255, 255, 255));

        Contenedor.setBackground(new java.awt.Color(255, 255, 255));
        Contenedor.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 15, 15));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel2.setFocusTraversalPolicyProvider(true);
        jPanel2.setMaximumSize(new java.awt.Dimension(320, 200));
        jPanel2.setMinimumSize(new java.awt.Dimension(320, 200));
        jPanel2.setPreferredSize(new java.awt.Dimension(320, 200));

        jLabel1.setBackground(new java.awt.Color(0, 102, 153));
        jLabel1.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("PERIODO ESCOLAR");
        jLabel1.setOpaque(true);

        E1.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        E1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Activar.png"))); // NOI18N
        E1.setText("Registrar Periodo Escolar");
        E1.setIconTextGap(8);
        E1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                E1MouseReleased(evt);
            }
        });

        E2.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        E2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Activar.png"))); // NOI18N
        E2.setText("Modificar Periodo Escolar Actual");
        E2.setIconTextGap(8);
        E2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                E2MouseReleased(evt);
            }
        });

        E3.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        E3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Activar.png"))); // NOI18N
        E3.setText("Gestionar Documentos");
        E3.setIconTextGap(8);
        E3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                E3MouseReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(E3)
                    .addComponent(E2)
                    .addComponent(E1))
                .addContainerGap(52, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(E1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(E2, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(E3, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 57, Short.MAX_VALUE))
        );

        Contenedor.add(jPanel2);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel4.setFocusTraversalPolicyProvider(true);
        jPanel4.setMaximumSize(new java.awt.Dimension(320, 200));
        jPanel4.setMinimumSize(new java.awt.Dimension(320, 200));
        jPanel4.setName(""); // NOI18N
        jPanel4.setPreferredSize(new java.awt.Dimension(320, 200));

        jLabel3.setBackground(new java.awt.Color(0, 102, 153));
        jLabel3.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("VACANTES Y CONCEPTOS");
        jLabel3.setOpaque(true);

        E6.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        E6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Activar.png"))); // NOI18N
        E6.setText("Configurar Vacantes y Secciones");
        E6.setIconTextGap(8);
        E6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                E6MouseReleased(evt);
            }
        });

        E4.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        E4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Activar.png"))); // NOI18N
        E4.setText("Gestionar Conceptos Escolares");
        E4.setIconTextGap(8);
        E4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                E4MouseReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 318, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(E4)
                    .addComponent(E6))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(E6, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(E4, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 97, Short.MAX_VALUE))
        );

        Contenedor.add(jPanel4);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setFocusTraversalPolicyProvider(true);
        jPanel3.setMaximumSize(new java.awt.Dimension(320, 200));
        jPanel3.setMinimumSize(new java.awt.Dimension(320, 200));
        jPanel3.setPreferredSize(new java.awt.Dimension(320, 200));

        jLabel2.setBackground(new java.awt.Color(0, 102, 153));
        jLabel2.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("MATRICULA");
        jLabel2.setOpaque(true);

        E7.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        E7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Activar.png"))); // NOI18N
        E7.setText("Registrar nueva Matricula");
        E7.setIconTextGap(8);

        E8.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        E8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Activar.png"))); // NOI18N
        E8.setText("Vizualizar Matriculas");
        E8.setIconTextGap(8);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(E7)
                    .addComponent(E8))
                .addContainerGap(102, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(E7, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(E8, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 92, Short.MAX_VALUE))
        );

        Contenedor.add(jPanel3);

        Cabecera.setBackground(new java.awt.Color(0, 102, 153));

        javax.swing.GroupLayout CabeceraLayout = new javax.swing.GroupLayout(Cabecera);
        Cabecera.setLayout(CabeceraLayout);
        CabeceraLayout.setHorizontalGroup(
            CabeceraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        CabeceraLayout.setVerticalGroup(
            CabeceraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 96, Short.MAX_VALUE)
        );

        Termino.setBackground(new java.awt.Color(0, 102, 153));
        Termino.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout TerminoLayout = new javax.swing.GroupLayout(Termino);
        Termino.setLayout(TerminoLayout);
        TerminoLayout.setHorizontalGroup(
            TerminoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        TerminoLayout.setVerticalGroup(
            TerminoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 46, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout FondoLayout = new javax.swing.GroupLayout(Fondo);
        Fondo.setLayout(FondoLayout);
        FondoLayout.setHorizontalGroup(
            FondoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Cabecera, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(FondoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(Contenedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(Termino, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        FondoLayout.setVerticalGroup(
            FondoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FondoLayout.createSequentialGroup()
                .addComponent(Cabecera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Contenedor, javax.swing.GroupLayout.PREFERRED_SIZE, 336, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(Termino, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Fondo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Fondo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void E3MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_E3MouseReleased

        E3.setForeground(Color.RED);
        P_Documentos periodo = new P_Documentos();
        periodo.setVisible(true);
    }//GEN-LAST:event_E3MouseReleased

    private void E1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_E1MouseReleased
        E1.setForeground(Color.RED);
        if (S_Periodo.buscarPeriodo(año) == null) {
            R_Periodo periodo = new R_Periodo();
            periodo.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this,
                    "Ya se a registado el año escolar "+año+".",
                    "MENSAJE", JOptionPane.ERROR_MESSAGE,
                    new Icono().error());
        }
    }//GEN-LAST:event_E1MouseReleased

    private void E4MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_E4MouseReleased
         E4.setForeground(Color.RED);
         P_Conceptos periodo = new P_Conceptos();
        
        periodo.setVisible(true);
    }//GEN-LAST:event_E4MouseReleased

    private void E2MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_E2MouseReleased

         E2.setForeground(Color.RED);
         M_Periodo periodo = new M_Periodo();
         periodo.periodo(año);
        periodo.setVisible(true);
    }//GEN-LAST:event_E2MouseReleased

    private void E6MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_E6MouseReleased
         E6.setForeground(Color.RED);
         C_Vacantes vacantes = new C_Vacantes();
        vacantes.setVisible(true);
    }//GEN-LAST:event_E6MouseReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new P_Principal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Cabecera;
    private javax.swing.JPanel Contenedor;
    private javax.swing.JLabel E1;
    private javax.swing.JLabel E2;
    private javax.swing.JLabel E3;
    private javax.swing.JLabel E4;
    private javax.swing.JLabel E6;
    private javax.swing.JLabel E7;
    private javax.swing.JLabel E8;
    private javax.swing.JPanel Fondo;
    private javax.swing.JPanel Termino;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    // End of variables declaration//GEN-END:variables
}
