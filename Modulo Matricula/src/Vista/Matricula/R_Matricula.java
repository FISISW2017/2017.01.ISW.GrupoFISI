/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista.Matricula;

import Adicionales.Estilos.Icono;
import Controladores.C_Documentos;
import Estilo.EstiloBoton;
import Estilo.EstiloCalendario;
import Estilo.EstiloComboBox;
import Estilo.Plantilla;
import Modelo.Alumno;

import Modelo.Documento;
import Modelo.Nivel;
import Servicios.S_Matricula;
import Servicios.S_NivelGrados;
import Servicios.S_Periodo;
import com.sun.java.swing.plaf.motif.MotifTabbedPaneUI;
import com.sun.java.swing.plaf.windows.WindowsTabbedPaneUI;
import java.awt.Color;
import java.awt.Insets;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.plaf.basic.BasicTabbedPaneUI;
import javax.swing.plaf.metal.MetalTabbedPaneUI;
import javax.swing.plaf.synth.SynthTabbedPaneUI;

/**
 *
 * @author Richard
 */
public class R_Matricula extends javax.swing.JFrame {

    private final List<Documento> losDocumentos = C_Documentos.lista();
    private final JCheckBox ch[] = new JCheckBox[losDocumentos.size()];

    
    
    private Alumno alumno;
   
    private int año;

    public R_Matricula() {
        UIManager.put("TabbedPane.selected", Color.white);
         UIManager.put("TabbedPane.background",new Color(204,204,204));
          UIManager.put("TabbedPane.highlight", Color.BLACK);
          UIManager.put("TabbedPane.light", Color.BLACK);
          UIManager.put("TabbedPane.darkShadow", Color.BLACK);
          UIManager.put("TabbedPane.shadow", Color.WHITE);
         
          UIManager.put("TabbedPane.contentBorderInsets", new Insets(1,1,1,1));
          UIManager.put("TabbedPane.contentOpaque",false);
        Plantilla.basica();
        initComponents();
        TP.setUI(new BasicTabbedPaneUI());
        cargarDocumentos();
        cargarNiveles();
        estilos();

        Alu.add(Al1);
        Alu.add(Al2);

        Genero.add(G1);
        Genero.add(G2);

        Enf.add(Pa1);
        Enf.add(Pa2);

    }

    private void cargarNiveles()
    {
        List<Nivel> niveles = S_NivelGrados.losNiveles();
        Cb0.removeAllItems();
        for(int i = 0; i< niveles.size();i++)
            Cb0.addItem(niveles.get(i).getNivelId());
    }
    
    private void estilos() {
        new EstiloBoton().Normal(B1, "verde");
        new EstiloBoton().Normal(B2, "naranja");

        new EstiloCalendario().basico(D1);

        new EstiloComboBox().sinEstilo(Cb0);
        new EstiloComboBox().sinEstilo(Cb1);
        new EstiloComboBox().sinEstilo(Cb2);
        new EstiloComboBox().sinEstilo(Sangre);
    }

    public void buscarAlumno(String codigo) {

       
        int ultimoAño = Integer.parseInt(S_Matricula.matriculaActual(codigo).getId().getPeriodoAno());
        if (ultimoAño < año) {
            alumno = S_Matricula.buscarAlumno(codigo);
            
            cargarAlumno();
            
        }
        else
        {
            Plantilla.PanelesAuxliares();
             JOptionPane.showMessageDialog(this,
                        "El alumno ya presenta una matricula en el\n"
                        + "año presente. ",
                        "MENSAJE", JOptionPane.INFORMATION_MESSAGE,
                        new Icono().error());
        }
    }

  

    private void cargarAlumno() {
        E2.setText(alumno.getAlumnoApellidos());
        E3.setText(alumno.getAlumnoNombres());

        if (alumno.getAlumnoGenero() == 'M') {
            G1.setSelected(true);
        } else {
            G2.setSelected(true);
        }

        D1.setDate(alumno.getAlumnoFNacimiento());

        E4.setText(alumno.getAlumnoDireccion());
        E5.setText(alumno.getAlumnoTelefono());

        A1.setText(alumno.getAlumnoEnfermedad());

        A2.setText(alumno.getAlumnoProcendencia());

        Sangre.setSelectedItem(alumno.getAlumnoTiposangre());
    }

  

    private void cargarDocumentos() {

        for (int i = 0; i < losDocumentos.size(); i++) {
            ch[i] = new JCheckBox(losDocumentos.get(i).getDocumentoNombre());
            ch[i].setFocusPainted(false);
            ch[i].setBackground(new java.awt.Color(255, 255, 255));
            ch[i].setFont(new java.awt.Font("Swis721 LtEx BT", 0, 13));

            P_Doc.add(ch[i]);
        }
        P_Doc.updateUI();
        new Estilo.EstiloPanel().sinEstilo(jScrollPane1);

    }

    private Alumno datosAlumno()
    {
        Alumno alumno = new Alumno();
        alumno.setAlumnoApellidos(E2.getText());
        alumno.setAlumnoDni(E1.getText());
        alumno.setAlumnoNombres(E3.getText());
        alumno.setAlumnoGenero('M');
        alumno.setAlumnoDireccion(E4.getText());
        alumno.setAlumnoTelefono(E5.getText());
        alumno.setAlumnoEnfermedad(A1.getText());
        alumno.setAlumnoProcendencia(A2.getText());
        alumno.setAlumnoTiposangre(Sangre.getSelectedItem().toString());
        alumno.setAlumnoFNacimiento(D1.getDate());
        alumno.setAlumnoEstado(true);
        return alumno;
                
    }
    
    private boolean HayErrores()
    {
        Plantilla.PanelesAuxliares();
        if(!E1.getText().equals(""))
        {
            if(E1.getText().length() != 8)
            {
                 JOptionPane.showMessageDialog(this,
                        "La cantidad de digitos del DNI es menor\n"
                      + "que 8.",
                        "MENSAJE", JOptionPane.INFORMATION_MESSAGE,
                        new Icono().error());
                 E1.requestFocus();
                 return true;
            }
            else
            {
                if(S_Matricula.existeDNIalumno(E1.getText()))
                {
                    JOptionPane.showMessageDialog(this,
                        "El DNI descrito ya ha sido registrado.",
                        "MENSAJE", JOptionPane.INFORMATION_MESSAGE,
                        new Icono().error());
                 E1.requestFocus();
                 return true;
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        Genero = new javax.swing.ButtonGroup();
        Enf = new javax.swing.ButtonGroup();
        Alu = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        Al1 = new javax.swing.JRadioButton();
        Al2 = new javax.swing.JRadioButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        P_Doc = new javax.swing.JPanel();
        jLabel24 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        A2 = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        E1 = new javax.swing.JTextField();
        E2 = new javax.swing.JTextField();
        E3 = new javax.swing.JTextField();
        G1 = new javax.swing.JRadioButton();
        G2 = new javax.swing.JRadioButton();
        D1 = new com.toedter.calendar.JDateChooser();
        E4 = new javax.swing.JTextField();
        E5 = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        Pa1 = new javax.swing.JRadioButton();
        Pa2 = new javax.swing.JRadioButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        A1 = new javax.swing.JTextArea();
        jLabel10 = new javax.swing.JLabel();
        Sangre = new javax.swing.JComboBox<>();
        jPanel5 = new javax.swing.JPanel();
        TP = new javax.swing.JTabbedPane();
        jPanel11 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        E6 = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        E7 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        E8 = new javax.swing.JTextField();
        E9 = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        E10 = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        E11 = new javax.swing.JTextField();
        jPanel12 = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        E12 = new javax.swing.JTextField();
        E13 = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        E14 = new javax.swing.JTextField();
        E15 = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        E16 = new javax.swing.JTextField();
        E17 = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jPanel13 = new javax.swing.JPanel();
        jLabel32 = new javax.swing.JLabel();
        E18 = new javax.swing.JTextField();
        E19 = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        E20 = new javax.swing.JTextField();
        E21 = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        E22 = new javax.swing.JTextField();
        E23 = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        Cb2 = new javax.swing.JComboBox<>();
        Cb1 = new javax.swing.JComboBox<>();
        jLabel20 = new javax.swing.JLabel();
        Cb0 = new javax.swing.JComboBox<>();
        jPanel7 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        Ch1 = new javax.swing.JCheckBox();
        Ch2 = new javax.swing.JCheckBox();
        S1 = new javax.swing.JSpinner();
        S2 = new javax.swing.JSpinner();
        jLabel21 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        S3 = new javax.swing.JSpinner();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        P1 = new javax.swing.JCheckBox();
        P6 = new javax.swing.JCheckBox();
        P2 = new javax.swing.JCheckBox();
        P7 = new javax.swing.JCheckBox();
        P3 = new javax.swing.JCheckBox();
        P8 = new javax.swing.JCheckBox();
        P4 = new javax.swing.JCheckBox();
        P9 = new javax.swing.JCheckBox();
        P5 = new javax.swing.JCheckBox();
        P10 = new javax.swing.JCheckBox();
        B1 = new javax.swing.JButton();
        B2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(960, 720));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel1.setMinimumSize(new java.awt.Dimension(905, 688));

        jLabel13.setBackground(new java.awt.Color(0, 102, 153));
        jLabel13.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 22)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("REGISTRAR MATRICULA");
        jLabel13.setOpaque(true);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), "Matricula ", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Swis721 LtEx BT", 1, 14), new java.awt.Color(0, 102, 153))); // NOI18N

        Al1.setBackground(new java.awt.Color(255, 255, 255));
        Al1.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 13)); // NOI18N
        Al1.setText("Alumno Nuevo");
        Al1.setFocusPainted(false);

        Al2.setBackground(new java.awt.Color(255, 255, 255));
        Al2.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 13)); // NOI18N
        Al2.setText("Alumno Existente");
        Al2.setFocusPainted(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(Al1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Al2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Al1)
                    .addComponent(Al2))
                .addGap(4, 4, 4))
        );

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), "Documentos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Swis721 LtEx BT", 1, 14), new java.awt.Color(0, 102, 153))); // NOI18N

        P_Doc.setBackground(new java.awt.Color(255, 255, 255));
        P_Doc.setLayout(new javax.swing.BoxLayout(P_Doc, javax.swing.BoxLayout.PAGE_AXIS));

        jLabel24.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel24.setText("Exigidos por la institución");
        P_Doc.add(jLabel24);

        jScrollPane1.setViewportView(P_Doc);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), "Procendencia", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Swis721 LtEx BT", 1, 14), new java.awt.Color(0, 102, 153))); // NOI18N

        jScrollPane2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 51)));

        A2.setColumns(20);
        A2.setLineWrap(true);
        A2.setRows(1);
        A2.setWrapStyleWord(true);
        jScrollPane2.setViewportView(A2);

        jLabel1.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel1.setText("Nombre de la Institución:");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane2))
                .addGap(6, 6, 6))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6))
        );

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), "Datos del Alumno", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Swis721 LtEx BT", 1, 14), new java.awt.Color(0, 102, 153))); // NOI18N
        jPanel4.setMinimumSize(new java.awt.Dimension(341, 372));

        jLabel2.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel2.setText("DNI");

        jLabel3.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel3.setText("Apellidos");

        jLabel4.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel4.setText("Nombres");

        jLabel5.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel5.setText("Género");

        jLabel6.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel6.setText("Fecha de N.");

        jLabel7.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel7.setText("Dirección");

        jLabel8.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel8.setText("Teléfono");

        G1.setBackground(new java.awt.Color(255, 255, 255));
        G1.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 13)); // NOI18N
        G1.setText("Masculino");
        G1.setFocusPainted(false);

        G2.setBackground(new java.awt.Color(255, 255, 255));
        G2.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 13)); // NOI18N
        G2.setText("Femenino");
        G2.setFocusPainted(false);

        jLabel9.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel9.setText("Pádece enfermedades");

        Pa1.setBackground(new java.awt.Color(255, 255, 255));
        Pa1.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 13)); // NOI18N
        Pa1.setText("SI");
        Pa1.setFocusPainted(false);
        Pa1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Pa1ActionPerformed(evt);
            }
        });

        Pa2.setBackground(new java.awt.Color(255, 255, 255));
        Pa2.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 13)); // NOI18N
        Pa2.setText("No");
        Pa2.setFocusPainted(false);

        jScrollPane3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 51)));

        A1.setColumns(20);
        A1.setLineWrap(true);
        A1.setRows(1);
        A1.setWrapStyleWord(true);
        jScrollPane3.setViewportView(A1);

        jLabel10.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel10.setText("Tipo de Sangre");

        Sangre.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " ", "A+", "A-", "B+", "B-", "AB+", "B-", "O+", "O-" }));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(2, 2, 2)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(E4)
                            .addComponent(E3)
                            .addComponent(E2)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(D1, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addComponent(G1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(G2))
                                    .addComponent(E5, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(E1, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 81, Short.MAX_VALUE))))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(2, 2, 2)
                                .addComponent(Pa1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Pa2))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Sangre, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(6, 6, 6))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(E1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(4, 4, 4)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(E2, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(4, 4, 4)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(E3)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(G1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(G2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(D1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(4, 4, 4)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(E4, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(E5, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(2, 2, 2)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(Pa1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(Pa2, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)))
                .addGap(0, 0, 0)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Sangre, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), "Familiar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Swis721 LtEx BT", 1, 14), new java.awt.Color(0, 102, 153))); // NOI18N

        TP.setFocusable(false);
        TP.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 13)); // NOI18N

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));

        jLabel11.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel11.setText("DNI");

        jButton2.setBackground(new java.awt.Color(0, 102, 153));
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Buscar.png"))); // NOI18N
        jButton2.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 0, 1, 1, new java.awt.Color(0, 0, 0)));
        jButton2.setFocusPainted(false);
        jButton2.setMaximumSize(new java.awt.Dimension(26, 26));
        jButton2.setMinimumSize(new java.awt.Dimension(26, 26));
        jButton2.setPreferredSize(new java.awt.Dimension(26, 26));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel12.setText("Apellidos");

        jLabel14.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel14.setText("Nombres");

        jLabel15.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel15.setText("Teléfono");

        jLabel16.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel16.setText("Dirección");

        jLabel25.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel25.setText("E-mail");

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, 79, Short.MAX_VALUE)
                            .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(E7)
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addComponent(E6, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 79, Short.MAX_VALUE)
                            .addComponent(jLabel15, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(E8)
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addComponent(E9, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 233, Short.MAX_VALUE))
                            .addComponent(E10)
                            .addComponent(E11))))
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(E6)
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(E7)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(E8, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(E9, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(E10, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE))
                .addGap(4, 4, 4)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(E11, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        TP.addTab("PADRE", jPanel11);

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));

        jLabel26.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel26.setText("E-mail");

        jLabel27.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel27.setText("Dirección");

        jLabel28.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel28.setText("Teléfono");

        jLabel29.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel29.setText("Nombres");

        jLabel30.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel30.setText("Apellidos");

        jLabel31.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel31.setText("DNI");

        jButton3.setBackground(new java.awt.Color(0, 102, 153));
        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Buscar.png"))); // NOI18N
        jButton3.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 0, 1, 1, new java.awt.Color(0, 0, 0)));
        jButton3.setFocusPainted(false);
        jButton3.setMaximumSize(new java.awt.Dimension(26, 26));
        jButton3.setMinimumSize(new java.awt.Dimension(26, 26));
        jButton3.setPreferredSize(new java.awt.Dimension(26, 26));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel30, javax.swing.GroupLayout.DEFAULT_SIZE, 79, Short.MAX_VALUE)
                            .addComponent(jLabel31, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(E16)
                            .addGroup(jPanel12Layout.createSequentialGroup()
                                .addComponent(E17, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel26, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel27, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel29, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 79, Short.MAX_VALUE)
                            .addComponent(jLabel28, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(E12, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(E15)
                            .addGroup(jPanel12Layout.createSequentialGroup()
                                .addComponent(E14, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 233, Short.MAX_VALUE))
                            .addComponent(E13))))
                .addContainerGap())
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(E17)
                        .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(E16)
                    .addComponent(jLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(E15, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(E14, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(E13, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel27, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE))
                .addGap(4, 4, 4)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(E12, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        TP.addTab("MADRE", jPanel12);

        jPanel13.setBackground(new java.awt.Color(255, 255, 255));

        jLabel32.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel32.setText("E-mail");

        jLabel33.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel33.setText("Dirección");

        jLabel34.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel34.setText("Teléfono");

        jLabel35.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel35.setText("Nombres");

        jLabel36.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel36.setText("Apellidos");

        jLabel37.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel37.setText("DNI");

        jButton4.setBackground(new java.awt.Color(0, 102, 153));
        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Buscar.png"))); // NOI18N
        jButton4.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 0, 1, 1, new java.awt.Color(0, 0, 0)));
        jButton4.setFocusPainted(false);
        jButton4.setMaximumSize(new java.awt.Dimension(26, 26));
        jButton4.setMinimumSize(new java.awt.Dimension(26, 26));
        jButton4.setPreferredSize(new java.awt.Dimension(26, 26));
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel35, javax.swing.GroupLayout.DEFAULT_SIZE, 79, Short.MAX_VALUE)
                            .addComponent(jLabel34, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(E21)
                            .addGroup(jPanel13Layout.createSequentialGroup()
                                .addComponent(E20, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 233, Short.MAX_VALUE))))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel36, javax.swing.GroupLayout.DEFAULT_SIZE, 79, Short.MAX_VALUE)
                            .addComponent(jLabel37, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(E22)
                            .addGroup(jPanel13Layout.createSequentialGroup()
                                .addComponent(E23, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(E18)
                            .addComponent(E19))))
                .addContainerGap())
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(E23)
                        .addComponent(jLabel37, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(E22)
                    .addComponent(jLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(E21, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(E20, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel34, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(E19, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel33, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE))
                .addGap(4, 4, 4)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(E18, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        TP.addTab("APODERADO", jPanel13);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(TP)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(TP, javax.swing.GroupLayout.PREFERRED_SIZE, 220, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), "Registro Escolar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Swis721 LtEx BT", 1, 14), new java.awt.Color(0, 102, 153))); // NOI18N

        jLabel17.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel17.setText("Nivel  ");

        jLabel18.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel18.setText("Grado   ");

        jLabel19.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel19.setText("Sección");

        Cb2.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N

        Cb1.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        Cb1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Cb1MouseClicked(evt);
            }
        });
        Cb1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Cb1ActionPerformed(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel20.setText("Vacantes disponibles:");

        Cb0.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        Cb0.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Cb0MouseClicked(evt);
            }
        });
        Cb0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Cb0ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel17, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel18, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Cb1, 0, 155, Short.MAX_VALUE)
                            .addComponent(Cb0, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jLabel20)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Cb2, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(6, 6, 6))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Cb0, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Cb1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(Cb2)
                    .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), "Pensiones y Conceptoos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Swis721 LtEx BT", 1, 14), new java.awt.Color(0, 102, 153))); // NOI18N

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setLayout(new java.awt.GridBagLayout());

        Ch1.setBackground(new java.awt.Color(255, 255, 255));
        Ch1.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        Ch1.setSelected(true);
        Ch1.setText("Matricula");
        Ch1.setFocusPainted(false);
        Ch1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Ch1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel8.add(Ch1, gridBagConstraints);

        Ch2.setBackground(new java.awt.Color(255, 255, 255));
        Ch2.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        Ch2.setSelected(true);
        Ch2.setText("Pruebas");
        Ch2.setFocusPainted(false);
        Ch2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        Ch2.setMaximumSize(new java.awt.Dimension(90, 26));
        Ch2.setMinimumSize(new java.awt.Dimension(90, 26));
        Ch2.setPreferredSize(new java.awt.Dimension(90, 26));
        Ch2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Ch2ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel8.add(Ch2, gridBagConstraints);

        S1.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        S1.setModel(new javax.swing.SpinnerNumberModel(10.0d, 10.0d, 500.0d, 1.0d));
        S1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        S1.setEditor(new javax.swing.JSpinner.NumberEditor(S1, "0.00"));
        S1.setMinimumSize(new java.awt.Dimension(110, 20));
        S1.setPreferredSize(new java.awt.Dimension(80, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanel8.add(S1, gridBagConstraints);

        S2.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        S2.setModel(new javax.swing.SpinnerNumberModel(10.0d, 10.0d, 500.0d, 1.0d));
        S2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        S2.setEditor(new javax.swing.JSpinner.NumberEditor(S2, "0.00"));
        S2.setMinimumSize(new java.awt.Dimension(110, 20));
        S2.setPreferredSize(new java.awt.Dimension(80, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanel8.add(S2, gridBagConstraints);

        jLabel21.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel21.setText("Conceptos:");

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.setLayout(new java.awt.GridBagLayout());

        S3.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        S3.setModel(new javax.swing.SpinnerNumberModel(10.0d, 10.0d, 500.0d, 1.0d));
        S3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        S3.setEditor(new javax.swing.JSpinner.NumberEditor(S3, "0.00"));
        S3.setMinimumSize(new java.awt.Dimension(110, 24));
        S3.setPreferredSize(new java.awt.Dimension(80, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanel9.add(S3, gridBagConstraints);

        jLabel22.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        jLabel22.setText("Precio:");
        jLabel22.setPreferredSize(new java.awt.Dimension(90, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel9.add(jLabel22, gridBagConstraints);

        jLabel23.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 13)); // NOI18N
        jLabel23.setText("Pensiones:");

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.setLayout(new java.awt.GridBagLayout());

        P1.setBackground(new java.awt.Color(255, 255, 255));
        P1.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 13)); // NOI18N
        P1.setSelected(true);
        P1.setText("Mar");
        P1.setFocusPainted(false);
        P1.setMaximumSize(new java.awt.Dimension(68, 19));
        P1.setMinimumSize(new java.awt.Dimension(68, 19));
        P1.setPreferredSize(new java.awt.Dimension(68, 19));
        jPanel10.add(P1, new java.awt.GridBagConstraints());

        P6.setBackground(new java.awt.Color(255, 255, 255));
        P6.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 13)); // NOI18N
        P6.setSelected(true);
        P6.setText("Ago");
        P6.setFocusPainted(false);
        P6.setMaximumSize(new java.awt.Dimension(68, 19));
        P6.setMinimumSize(new java.awt.Dimension(68, 19));
        P6.setPreferredSize(new java.awt.Dimension(68, 19));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        jPanel10.add(P6, gridBagConstraints);

        P2.setBackground(new java.awt.Color(255, 255, 255));
        P2.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 13)); // NOI18N
        P2.setSelected(true);
        P2.setText("Abr");
        P2.setFocusPainted(false);
        P2.setMaximumSize(new java.awt.Dimension(68, 19));
        P2.setMinimumSize(new java.awt.Dimension(68, 19));
        P2.setPreferredSize(new java.awt.Dimension(68, 19));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        jPanel10.add(P2, gridBagConstraints);

        P7.setBackground(new java.awt.Color(255, 255, 255));
        P7.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 13)); // NOI18N
        P7.setSelected(true);
        P7.setText("Set");
        P7.setFocusPainted(false);
        P7.setMaximumSize(new java.awt.Dimension(68, 19));
        P7.setMinimumSize(new java.awt.Dimension(68, 19));
        P7.setPreferredSize(new java.awt.Dimension(68, 19));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        jPanel10.add(P7, gridBagConstraints);

        P3.setBackground(new java.awt.Color(255, 255, 255));
        P3.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 13)); // NOI18N
        P3.setSelected(true);
        P3.setText("May");
        P3.setFocusPainted(false);
        P3.setMaximumSize(new java.awt.Dimension(68, 19));
        P3.setMinimumSize(new java.awt.Dimension(68, 19));
        P3.setPreferredSize(new java.awt.Dimension(68, 19));
        P3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                P3ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        jPanel10.add(P3, gridBagConstraints);

        P8.setBackground(new java.awt.Color(255, 255, 255));
        P8.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 13)); // NOI18N
        P8.setSelected(true);
        P8.setText("Oct");
        P8.setFocusPainted(false);
        P8.setMaximumSize(new java.awt.Dimension(68, 19));
        P8.setMinimumSize(new java.awt.Dimension(68, 19));
        P8.setPreferredSize(new java.awt.Dimension(68, 19));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        jPanel10.add(P8, gridBagConstraints);

        P4.setBackground(new java.awt.Color(255, 255, 255));
        P4.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 13)); // NOI18N
        P4.setSelected(true);
        P4.setText("Jun");
        P4.setFocusPainted(false);
        P4.setMaximumSize(new java.awt.Dimension(68, 19));
        P4.setMinimumSize(new java.awt.Dimension(68, 19));
        P4.setPreferredSize(new java.awt.Dimension(68, 19));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        jPanel10.add(P4, gridBagConstraints);

        P9.setBackground(new java.awt.Color(255, 255, 255));
        P9.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 13)); // NOI18N
        P9.setSelected(true);
        P9.setText("Nov");
        P9.setFocusPainted(false);
        P9.setMaximumSize(new java.awt.Dimension(68, 19));
        P9.setMinimumSize(new java.awt.Dimension(68, 19));
        P9.setPreferredSize(new java.awt.Dimension(68, 19));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel10.add(P9, gridBagConstraints);

        P5.setBackground(new java.awt.Color(255, 255, 255));
        P5.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 13)); // NOI18N
        P5.setSelected(true);
        P5.setText("Jul");
        P5.setFocusPainted(false);
        P5.setMaximumSize(new java.awt.Dimension(68, 19));
        P5.setMinimumSize(new java.awt.Dimension(68, 19));
        P5.setPreferredSize(new java.awt.Dimension(68, 19));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel10.add(P5, gridBagConstraints);

        P10.setBackground(new java.awt.Color(255, 255, 255));
        P10.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 13)); // NOI18N
        P10.setSelected(true);
        P10.setText("Dic");
        P10.setFocusPainted(false);
        P10.setMaximumSize(new java.awt.Dimension(68, 19));
        P10.setMinimumSize(new java.awt.Dimension(68, 19));
        P10.setPreferredSize(new java.awt.Dimension(68, 19));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        jPanel10.add(P10, gridBagConstraints);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel23)
                            .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(6, 6, 6))))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel21)
                .addGap(0, 0, 0)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(jLabel23)
                .addGap(0, 0, 0)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 273, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
            .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 343, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );

        B1.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        B1.setText("Registrar");
        B1.setFocusPainted(false);
        B1.setMaximumSize(new java.awt.Dimension(133, 36));
        B1.setMinimumSize(new java.awt.Dimension(133, 36));
        B1.setPreferredSize(new java.awt.Dimension(133, 36));
        B1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B1ActionPerformed(evt);
            }
        });

        B2.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        B2.setText("Cancelar");
        B2.setFocusPainted(false);
        B2.setMaximumSize(new java.awt.Dimension(133, 36));
        B2.setMinimumSize(new java.awt.Dimension(133, 36));
        B2.setPreferredSize(new java.awt.Dimension(133, 36));
        B2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(B1, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(B2, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(B1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(B2, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Cb1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Cb1MouseClicked

    }//GEN-LAST:event_Cb1MouseClicked

    private void Cb1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Cb1ActionPerformed

    }//GEN-LAST:event_Cb1ActionPerformed

    private void Cb0MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Cb0MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_Cb0MouseClicked

    private void Cb0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Cb0ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Cb0ActionPerformed

    private void Ch1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Ch1ActionPerformed

        if (!Ch1.isSelected()) {
            S1.setEnabled(false);
        } else {
            S1.setEnabled(true);
        }
    }//GEN-LAST:event_Ch1ActionPerformed

    private void Ch2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Ch2ActionPerformed
        if (!Ch2.isSelected()) {
            S2.setEnabled(false);
        } else {
            S2.setEnabled(true);
        }
    }//GEN-LAST:event_Ch2ActionPerformed

    private void P3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_P3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_P3ActionPerformed

    private void B1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B1ActionPerformed

      if(!HayErrores())  
        S_Matricula.AlumnoNuevo(datosAlumno());

    }//GEN-LAST:event_B1ActionPerformed

    private void B2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B2ActionPerformed

        this.dispose();

    }//GEN-LAST:event_B2ActionPerformed

    private void Pa1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Pa1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Pa1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        Plantilla.PanelesAuxliares();

        if (E6.getText().length() == 8) {
           
        } else {
            JOptionPane.showMessageDialog(this,
                "El DNI descrito tiene menos de 8 caracteres.",
                "MENSAJE", JOptionPane.INFORMATION_MESSAGE,
                new Icono().error());
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton4ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new R_Matricula().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea A1;
    private javax.swing.JTextArea A2;
    private javax.swing.JRadioButton Al1;
    private javax.swing.JRadioButton Al2;
    private javax.swing.ButtonGroup Alu;
    private javax.swing.JButton B1;
    private javax.swing.JButton B2;
    private javax.swing.JComboBox<String> Cb0;
    private javax.swing.JComboBox<String> Cb1;
    private javax.swing.JComboBox<String> Cb2;
    private javax.swing.JCheckBox Ch1;
    private javax.swing.JCheckBox Ch2;
    private com.toedter.calendar.JDateChooser D1;
    private javax.swing.JTextField E1;
    private javax.swing.JTextField E10;
    private javax.swing.JTextField E11;
    private javax.swing.JTextField E12;
    private javax.swing.JTextField E13;
    private javax.swing.JTextField E14;
    private javax.swing.JTextField E15;
    private javax.swing.JTextField E16;
    private javax.swing.JTextField E17;
    private javax.swing.JTextField E18;
    private javax.swing.JTextField E19;
    private javax.swing.JTextField E2;
    private javax.swing.JTextField E20;
    private javax.swing.JTextField E21;
    private javax.swing.JTextField E22;
    private javax.swing.JTextField E23;
    private javax.swing.JTextField E3;
    private javax.swing.JTextField E4;
    private javax.swing.JTextField E5;
    private javax.swing.JTextField E6;
    private javax.swing.JTextField E7;
    private javax.swing.JTextField E8;
    private javax.swing.JTextField E9;
    private javax.swing.ButtonGroup Enf;
    private javax.swing.JRadioButton G1;
    private javax.swing.JRadioButton G2;
    private javax.swing.ButtonGroup Genero;
    private javax.swing.JCheckBox P1;
    private javax.swing.JCheckBox P10;
    private javax.swing.JCheckBox P2;
    private javax.swing.JCheckBox P3;
    private javax.swing.JCheckBox P4;
    private javax.swing.JCheckBox P5;
    private javax.swing.JCheckBox P6;
    private javax.swing.JCheckBox P7;
    private javax.swing.JCheckBox P8;
    private javax.swing.JCheckBox P9;
    private javax.swing.JPanel P_Doc;
    private javax.swing.JRadioButton Pa1;
    private javax.swing.JRadioButton Pa2;
    private javax.swing.JSpinner S1;
    private javax.swing.JSpinner S2;
    private javax.swing.JSpinner S3;
    private javax.swing.JComboBox<String> Sangre;
    private javax.swing.JTabbedPane TP;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    // End of variables declaration//GEN-END:variables
}
