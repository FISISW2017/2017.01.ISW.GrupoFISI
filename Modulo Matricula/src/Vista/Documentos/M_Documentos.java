/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista.Documentos;

import Vista.Conceptos.*;
import Adicionales.Estilos.Icono;
import Controladores.C_Periodo;
import Estilo.EstiloBoton;
import Estilo.EstiloSpinner;
import Estilo.EstiloTexto;
import Estilo.Plantilla;
import Modelo.Concepto;
import Modelo.Documento;
import Servicios.S_Periodo;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

/**
 *
 * @author Richard
 */
public class M_Documentos extends javax.swing.JFrame {

    private P_Documentos principal = null;
    private Documento documento;
    
    public M_Documentos() {
       
        
        initComponents();
        setLocationRelativeTo(null);
        estilo();
    }

    private void estilo() {
       // new EstiloTexto().Estandar("Máximo 6 letras", E1, false);
        //new EstiloTexto().Estandar("Nombre del Concepto", E2, false);
        new Estilo.EstiloBoton().Normal(R1, "verde");
        new Estilo.EstiloBoton().Normal(C1, "naranja");
        
    }

    public void setPrincipal(P_Documentos principal,String doc) {
        this.principal = principal;
        documento = S_Periodo.buscarDocumento(doc);
        cargarDatos();
    }

    private void cargarDatos()
    {
        E2.setText(documento.getDocumentoNombre());
        T1.setText(documento.getDocumentoDescripcion());
        Estado.setSelected(documento.isDocumentoTipo());
    }
    
    private void Modificar() {
        
        documento.setDocumentoNombre(E2.getText());
        documento.setDocumentoDescripcion(T1.getText());
        documento.setDocumentoTipo(Estado.isSelected());
        S_Periodo.modificarDocumento(documento);

    }

    private boolean excepciones() {
        if (E2.getText().length() < 5) {
            JOptionPane.showMessageDialog(this,
                    "El nombre del documento es muy corto.",
                    "ERROR", JOptionPane.ERROR_MESSAGE,
                    new Icono().error());
            E2.requestFocus();
            return false;
        } else if (T1.getText().length() < 5) {
            JOptionPane.showMessageDialog(this,
                    "La descripción del documento es muy corto.",
                    "ERROR", JOptionPane.ERROR_MESSAGE,
                    new Icono().error());
            E2.requestFocus();
            return false;
                }
        else
            return true;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        E2 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        T1 = new javax.swing.JTextArea();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        Estado = new javax.swing.JCheckBox();
        jPanel2 = new javax.swing.JPanel();
        R1 = new javax.swing.JButton();
        C1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Registrar Concepto");
        setBackground(new java.awt.Color(255, 255, 255));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));

        jLabel2.setBackground(new java.awt.Color(0, 102, 153));
        jLabel2.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 22)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("REGISTRAR DOCUMENTO");
        jLabel2.setOpaque(true);

        jLabel4.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 14)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel4.setText("Nombre:");

        E2.setMargin(new java.awt.Insets(2, 5, 2, 5));
        E2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                E2KeyTyped(evt);
            }
        });

        jScrollPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));

        T1.setColumns(20);
        T1.setLineWrap(true);
        T1.setRows(5);
        jScrollPane1.setViewportView(T1);

        jLabel5.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel5.setText("Descripción:");

        jLabel6.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 14)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel6.setText("Estado: ");

        Estado.setBackground(new java.awt.Color(255, 255, 255));
        Estado.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 13)); // NOI18N
        Estado.setText("Obligatorio");
        Estado.setFocusPainted(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(E2, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 350, Short.MAX_VALUE))
                            .addComponent(jLabel6)
                            .addComponent(Estado))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addGap(10, 10, 10)
                .addComponent(jLabel6)
                .addGap(1, 1, 1)
                .addComponent(Estado, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addGap(1, 1, 1)
                .addComponent(E2, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jLabel5)
                .addGap(0, 2, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel2.setLayout(new java.awt.GridBagLayout());

        R1.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        R1.setText("Modificar");
        R1.setFocusPainted(false);
        R1.setMaximumSize(new java.awt.Dimension(133, 36));
        R1.setMinimumSize(new java.awt.Dimension(133, 36));
        R1.setPreferredSize(new java.awt.Dimension(133, 36));
        R1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                R1ActionPerformed(evt);
            }
        });
        jPanel2.add(R1, new java.awt.GridBagConstraints());

        C1.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        C1.setText("Cancelar");
        C1.setFocusPainted(false);
        C1.setMaximumSize(new java.awt.Dimension(133, 36));
        C1.setMinimumSize(new java.awt.Dimension(133, 36));
        C1.setPreferredSize(new java.awt.Dimension(133, 36));
        C1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                C1ActionPerformed(evt);
            }
        });
        jPanel2.add(C1, new java.awt.GridBagConstraints());

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void R1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_R1ActionPerformed

        if (excepciones()) {
            int valor = JOptionPane.showConfirmDialog(this,
                    "¿Esta seguro que desea modificar el\n"
                    + "documento?", "CONFIRMACION", JOptionPane.OK_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE, new Icono().confirmar()
            );

            if (valor == JOptionPane.OK_OPTION) {
                Modificar();
                JOptionPane.showMessageDialog(this,
                        "Se ha modificado el documento\n"
                        + "de forma correcta.",
                        "MENSAJE", JOptionPane.INFORMATION_MESSAGE,
                        new Icono().aceptar());
                this.dispose();
                principal.setEnabled(true);

                principal.requestFocus();
                principal.actualizarTabla();
            }
        }
    }//GEN-LAST:event_R1ActionPerformed

    private void C1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_C1ActionPerformed

        principal.setEnabled(true);
        principal.requestFocus();

    }//GEN-LAST:event_C1ActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing

        principal.setEnabled(true);

        principal.requestFocus();        // TODO add your handling code here:
    }//GEN-LAST:event_formWindowClosing

    private void E2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_E2KeyTyped

        char car = evt.getKeyChar();
        if ((car < 'a' || car > 'z') && (car < 'A' || car > 'Z') && car != 'Ñ' && car != 'ñ'
            && (car != (char) KeyEvent.VK_SPACE)) {
            evt.consume();

            if (E2.getText().length() == 30) {
                evt.consume();
            }
        }
    }//GEN-LAST:event_E2KeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new M_Documentos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton C1;
    private javax.swing.JTextField E2;
    private javax.swing.JCheckBox Estado;
    private javax.swing.JButton R1;
    private javax.swing.JTextArea T1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
