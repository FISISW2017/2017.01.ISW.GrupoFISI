/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista.Documentos;

import Vista.Conceptos.*;
import Adicionales.Estilos.Cabecera;
import Adicionales.Estilos.Icono;
import Estilo.EstiloTabla;
import Estilo.Plantilla;
import Modelo.Concepto;
import Modelo.Documento;
import Servicios.S_Periodo;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.math.RoundingMode;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Richard
 */
public class P_Documentos extends javax.swing.JFrame {

    public P_Documentos() {
        Plantilla.basica();
        initComponents();
        setLocationRelativeTo(null);
       //new Estilo.EstiloTabla().Encabezado(T1, "plomo");
       //new Estilo.EstiloTabla().cuerpo(T1);
       
       cargarDoc();
        
        //Sc1.getViewport().setBackground(new Color(255,255,255));
        new Estilo.EstiloBoton().Normal(B1, "verde");
        new Estilo.EstiloBoton().Normal(B2, "verde");
        new Estilo.EstiloBoton().Normal(B3, "verde");
        new Estilo.EstiloBoton().Normal(B4, "verde");
        new EstiloTabla().diseñar(Sc1, T1, "plomo", new int[]{3}, new int[]{0}, new String[]{"ACTIVO"});
    }

    public void actualizarTabla() {
        cargarDoc();
    }

    private void cargarDoc() {
        List<Documento> lista = S_Periodo.losDocumentos();
        Object[][] datos = new Object[lista.size()][4];
        for (int i = 0; i < lista.size(); i++) {

            if (lista.get(i).isDocumentoEstado()) {
                datos[i][0] = "ACTIVO";
            } else {
                datos[i][0] = "INACTIVO";
            }

            if (lista.get(i).isDocumentoTipo()) {
                datos[i][1] = "OBLIGATORIO";
            } else {
                datos[i][1] = "OPCIONAL";
            }

            datos[i][2] = lista.get(i).getDocumentoId();
            datos[i][3] = lista.get(i).getDocumentoNombre();
            

        }
        String[] columna = {"Estado", "Tipo", "Codigo", "Descripción"};
        DefaultTableModel dtm = new DefaultTableModel(datos, columna) {
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return false;
            }
          
        };

        T1.setModel(dtm);

        T1.getColumnModel().getColumn(0).setPreferredWidth(80);
        T1.getColumnModel().getColumn(0).setMinWidth(80);
        T1.getColumnModel().getColumn(0).setMaxWidth(120);

        T1.getColumnModel().getColumn(1).setPreferredWidth(100);
        T1.getColumnModel().getColumn(1).setMinWidth(100);
        T1.getColumnModel().getColumn(1).setMaxWidth(150);

        T1.getColumnModel().getColumn(2).setPreferredWidth(70);
        T1.getColumnModel().getColumn(2).setMinWidth(70);
        T1.getColumnModel().getColumn(2).setMaxWidth(120);

        T1.getTableHeader().setReorderingAllowed(false);
    }

  

    private void Modificar()
    {
          String cod = T1.getValueAt(T1.getSelectedRow(), 2).toString();
          
           
           M_Documentos modificar = new M_Documentos();
           
          
           modificar.setPrincipal(this,cod);
           modificar.setVisible(true);
           this.setEnabled(false);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        Sc1 = new javax.swing.JScrollPane();
        T1 = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        B1 = new javax.swing.JButton();
        B2 = new javax.swing.JButton();
        B3 = new javax.swing.JButton();
        B4 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Gestionar Conceptos Escolares");

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));

        Sc1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));

        T1.setAutoCreateRowSorter(true);
        T1.setBackground(new java.awt.Color(240, 240, 240));
        T1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"HJKHJ", null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        T1.setGridColor(new java.awt.Color(51, 51, 51));
        T1.setIntercellSpacing(new java.awt.Dimension(0, 0));
        T1.setRowHeight(26);
        T1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        Sc1.setViewportView(T1);

        jLabel2.setBackground(new java.awt.Color(0, 102, 153));
        jLabel2.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 22)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("GESTIONAR DOCUMENTOS");
        jLabel2.setOpaque(true);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));

        jLabel1.setBackground(new java.awt.Color(102, 102, 102));
        jLabel1.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("OPCIONES");
        jLabel1.setFocusable(false);
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel1.setOpaque(true);
        jLabel1.setPreferredSize(new java.awt.Dimension(133, 22));

        B1.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 13)); // NOI18N
        B1.setText("Agregar");
        B1.setFocusPainted(false);
        B1.setPreferredSize(new java.awt.Dimension(133, 36));
        B1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B1ActionPerformed(evt);
            }
        });

        B2.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 13)); // NOI18N
        B2.setText("Modificar");
        B2.setFocusPainted(false);
        B2.setPreferredSize(new java.awt.Dimension(133, 36));
        B2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B2ActionPerformed(evt);
            }
        });

        B3.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 13)); // NOI18N
        B3.setText("Suspender");
        B3.setFocusPainted(false);
        B3.setPreferredSize(new java.awt.Dimension(133, 36));
        B3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B3ActionPerformed(evt);
            }
        });

        B4.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 13)); // NOI18N
        B4.setText("Recuperar");
        B4.setFocusPainted(false);
        B4.setPreferredSize(new java.awt.Dimension(133, 36));
        B4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(B4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(B2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(B3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(B1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(5, 5, 5))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(B1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(B2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(B3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(B4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(102, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(Sc1, javax.swing.GroupLayout.DEFAULT_SIZE, 591, Short.MAX_VALUE)
                .addGap(10, 10, 10)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Sc1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void B1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B1ActionPerformed

        R_Documentos nuevo = new R_Documentos();
        nuevo.setPrincipal(this);
        nuevo.setVisible(true);
        this.setEnabled(false);
    }//GEN-LAST:event_B1ActionPerformed

    private void B2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B2ActionPerformed

    Plantilla.PanelesAuxliares();
        if (T1.getSelectedRow() > -1)
        {
           
                Modificar();
           
         
            
        } 
        else
        {
            JOptionPane.showMessageDialog(this,
                    "Seleccione una fila de la tabla.",
                     "ERROR", JOptionPane.ERROR_MESSAGE,
                    new Icono().error());
        }
    }//GEN-LAST:event_B2ActionPerformed

    private void B3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B3ActionPerformed

       
        Plantilla.PanelesAuxliares();
                 if (T1.getSelectedRow() > -1)
        {
          
                String estado = T1.getValueAt(T1.getSelectedRow(), 0).toString();
                if(estado.equals("ACTIVO"))
                
                { S_Periodo.desactivarDocumento(T1.getValueAt(T1.getSelectedRow(), 2).toString());
                  JOptionPane.showMessageDialog(this,
                    "El Concepto: "+T1.getValueAt(T1.getSelectedRow(), 3)+
                    ", a sido\ndesactivado.",
                     "MENSAJE", JOptionPane.INFORMATION_MESSAGE,
                    new Icono().aceptar());
                cargarDoc();
                }
                else
                {
                      JOptionPane.showMessageDialog(this,
                    "El concepto ya esta desactivado."
                  
                     ,"ERROR", JOptionPane.ERROR_MESSAGE,
                    new Icono().error());
                }
            
            
        } 
        else
        {
           
            
              JOptionPane.showMessageDialog(this,
                    "Seleccione una fila de la tabla.",
                     "ERROR", JOptionPane.ERROR_MESSAGE,
                    new Icono().error());
        }
        
    }//GEN-LAST:event_B3ActionPerformed

    private void B4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B4ActionPerformed
        Plantilla.PanelesAuxliares();
        if (T1.getSelectedRow() > -1)
        {
         
                String estado = T1.getValueAt(T1.getSelectedRow(), 0).toString();
                if(estado.equals("INACTIVO"))
                
                { S_Periodo.activarDocumento(T1.getValueAt(T1.getSelectedRow(), 2).toString());
                JOptionPane.showMessageDialog(this,
                    "El Documento: "+T1.getValueAt(T1.getSelectedRow(), 3)+
                    ", a sido\nactivado.",
                     "MENSAJE", JOptionPane.INFORMATION_MESSAGE,
                    new Icono().aceptar());
                cargarDoc();
                }
                else
                {
                    JOptionPane.showMessageDialog(this,
                    "El concepto ya esta activado."
                  
                     ,"ERROR", JOptionPane.ERROR_MESSAGE,
                    new Icono().error());
                }
            
           
        } 
        else
        {
            JOptionPane.showMessageDialog(this,
                    "Seleccione una fila de la tabla.",
                     "ERROR", JOptionPane.ERROR_MESSAGE,
                    new Icono().error());
        }
    }//GEN-LAST:event_B4ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new P_Documentos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton B1;
    private javax.swing.JButton B2;
    private javax.swing.JButton B3;
    private javax.swing.JButton B4;
    private javax.swing.JScrollPane Sc1;
    private javax.swing.JTable T1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    // End of variables declaration//GEN-END:variables
}
