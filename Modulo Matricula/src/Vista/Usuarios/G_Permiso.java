/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
 /*          FAC_001 - 11/05/17 - inicio             */
 /*++++++++++++++++++++++++++++++++++++++++++++++++++*/
package Vista.Usuarios;

import static Controladores.C_Permiso.permiso;
import Modelo.Modulo;
import Modelo.PermisosUsuarios;
import Modelo.PermisosUsuariosId;
import Modelo.Submodulo;
import Modelo.SubmoduloId;
import Modelo.TipoPermisos;
import Modelo.Usuario;
import Servicios.S_Usuarios;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Fabio
 */
public class G_Permiso extends javax.swing.JFrame {

    ArrayList<ArrayList<String>> lista;
    List<Modulo> modulos;

    /**
     * Creates new form G_PERMISO
     */
    public G_Permiso() {
        initComponents();
        lista = new ArrayList<>();
        modulos = S_Usuarios.listarModulo();

        //Cargar módulos
        for (int i = 0; i < modulos.size(); i++) {
            ArrayList<String> apoyo = new ArrayList<>();
            String moduloId = modulos.get(i).getModuloId();
            apoyo.add(moduloId);
            lista.add(apoyo);
        }

        //Cargar submodulos
        for (int i = 0; i < lista.size(); i++) {
            Modulo modulo = modulos.get(i);
            List<Submodulo> submodulo = S_Usuarios.listarSubmodulo(modulo);
            System.out.println(submodulo.size());
            for (int j = 0; j < submodulo.size(); j++) {
                String submoduloId = submodulo.get(j).getId().getSubmoduloId();
                lista.get(i).add(submoduloId);
            }
        }

        //Mostrar carga de modulos y submodulos
       /* for (int i = 0; i < lista.size(); i++) {
            ArrayList array = lista.get(i);
            System.out.print("\n" + array.get(0) + ": ");
            for (int j = 1; j < array.size(); j++) {
                System.out.print(array.get(j) + " ");
            }
        }*/
    }

    private String obtenerValor(String userId, String moduloId, String submoduloId) {
        Usuario user = S_Usuarios.obtenerUsuario(userId);
        Modulo modulo = S_Usuarios.obtenerModulo(moduloId);
        Submodulo submodulo = S_Usuarios.obtenerSubmodulo(moduloId, submoduloId);
        PermisosUsuarios permiso = S_Usuarios.valorPermiso(user, modulo, submodulo);
        if (permiso == null) {
            return "NINGUNO";
        } else {
            char valor = permiso.getTipoPermisos().getCodPermiso();
            switch (valor) {
                case 'L':
                    return "LECTURA";
                case 'E':
                    return "ESCRITURA";
                default:
                    return "NINGUNO";
            }
        }
    }

    private char obtenerValorInverso(String permiso) {
        switch (permiso) {
            case "LECTURA":
                return 'L';
            case "ESCRITURA":
                return 'E';
            default:
                return 'N';
        }

    }

    private void manejarPermiso(int mod, int submod, String user_id, String valpermiso, String accion) {
        
        ArrayList modulo_array = lista.get(mod);
        String moduloId = (String) modulo_array.get(0);
        String submoduloId = (String) modulo_array.get(submod);
        
        Usuario usuario = S_Usuarios.obtenerUsuario(user_id);
        Submodulo submodulo = S_Usuarios.obtenerSubmodulo(moduloId, submoduloId);
        Modulo modulo = S_Usuarios.obtenerModulo(moduloId);
        
        char codpermiso = obtenerValorInverso(valpermiso);
        TipoPermisos tipopermiso = S_Usuarios.obtenerPermiso(codpermiso);
        
        PermisosUsuariosId id = new PermisosUsuariosId(moduloId, submoduloId, user_id);

        PermisosUsuarios permiso;
        if (accion.equals("INSERT")) {
            permiso = new PermisosUsuarios(id, submodulo, tipopermiso, usuario);
            S_Usuarios.registrarPermiso(permiso);
        } else if (accion.equals("UPDATE")) {
            permiso = S_Usuarios.valorPermiso(usuario, modulo, submodulo);
            permiso.setTipoPermisos(tipopermiso);
            S_Usuarios.modificarPermiso(permiso);
        }
    }

    private void tipoManejo(String accion, String user_id) {
        /*++++++++++++++++++MODULO 1+++++++++++++++++++*/
        String valpermiso;
        valpermiso = (String) MAT_STAT.getSelectedItem();
        manejarPermiso(0, 1, user_id, valpermiso, accion);

        valpermiso = (String) PER_STAT.getSelectedItem();
        manejarPermiso(0, 2, user_id, valpermiso, accion);

        valpermiso = (String) NIVEL_STAT.getSelectedItem();
        manejarPermiso(0, 3, user_id, valpermiso, accion);

        /*++++++++++++++++++MODULO 2+++++++++++++++++++*/
        valpermiso = (String) CONC_STAT.getSelectedItem();
        manejarPermiso(1, 1, user_id, valpermiso, accion);

        /*++++++++++++++++++MODULO 3+++++++++++++++++++*/
        valpermiso = (String) USER_STAT.getSelectedItem();
        manejarPermiso(2, 1, user_id, valpermiso, accion);

        valpermiso = (String) PERM_STAT.getSelectedItem();
        manejarPermiso(2, 2, user_id, valpermiso, accion);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        FIND_USER = new javax.swing.JTextField();
        FIND_BUTTON = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        PER_STAT = new javax.swing.JComboBox<>();
        USER_STAT = new javax.swing.JComboBox<>();
        NIVEL_STAT = new javax.swing.JComboBox<>();
        MAT_STAT = new javax.swing.JComboBox<>();
        CONC_STAT = new javax.swing.JComboBox<>();
        PERM_STAT = new javax.swing.JComboBox<>();
        UPDATE_BUTTON = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setBackground(new java.awt.Color(0, 102, 153));
        jLabel1.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 22)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("PERMISOS USUARIO");
        jLabel1.setOpaque(true);

        jLabel13.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel13.setText("PERIODOS");

        FIND_USER.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FIND_USERActionPerformed(evt);
            }
        });

        FIND_BUTTON.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        FIND_BUTTON.setText("Consultar");
        FIND_BUTTON.setFocusPainted(false);
        FIND_BUTTON.setMaximumSize(new java.awt.Dimension(133, 36));
        FIND_BUTTON.setMinimumSize(new java.awt.Dimension(133, 36));
        FIND_BUTTON.setPreferredSize(new java.awt.Dimension(133, 36));
        FIND_BUTTON.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FIND_BUTTONActionPerformed(evt);
            }
        });

        jLabel5.setBackground(new java.awt.Color(0, 102, 153));
        jLabel5.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 22)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel5.setText("MATRÍCULA");
        jLabel5.setOpaque(true);

        jLabel6.setBackground(new java.awt.Color(0, 102, 153));
        jLabel6.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 22)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel6.setText("USUARIOS");
        jLabel6.setOpaque(true);

        jLabel7.setBackground(new java.awt.Color(0, 102, 153));
        jLabel7.setFont(new java.awt.Font("Swis721 LtEx BT", 1, 22)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel7.setText("PAGOS");
        jLabel7.setOpaque(true);

        jLabel14.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel14.setText("ID USUARIO:");

        jLabel15.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel15.setText("MATRÍCULA");

        jLabel16.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel16.setText("NIVELES");

        jLabel17.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel17.setText("PERMISOS");

        jLabel18.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel18.setText("CONCEPTOS");

        jLabel19.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel19.setText("USUARIOS");

        PER_STAT.setFont(new java.awt.Font("Swis721 WGL4 BT", 0, 14)); // NOI18N
        PER_STAT.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "NINGUNO", "LECTURA", "ESCRITURA" }));
        PER_STAT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PER_STATActionPerformed(evt);
            }
        });

        USER_STAT.setFont(new java.awt.Font("Swis721 WGL4 BT", 0, 14)); // NOI18N
        USER_STAT.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "NINGUNO", "LECTURA", "ESCRITURA" }));
        USER_STAT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                USER_STATActionPerformed(evt);
            }
        });

        NIVEL_STAT.setFont(new java.awt.Font("Swis721 WGL4 BT", 0, 14)); // NOI18N
        NIVEL_STAT.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "NINGUNO", "LECTURA", "ESCRITURA" }));
        NIVEL_STAT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NIVEL_STATActionPerformed(evt);
            }
        });

        MAT_STAT.setFont(new java.awt.Font("Swis721 WGL4 BT", 0, 14)); // NOI18N
        MAT_STAT.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "NINGUNO", "LECTURA", "ESCRITURA" }));
        MAT_STAT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MAT_STATActionPerformed(evt);
            }
        });

        CONC_STAT.setFont(new java.awt.Font("Swis721 WGL4 BT", 0, 14)); // NOI18N
        CONC_STAT.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "NINGUNO", "LECTURA", "ESCRITURA" }));
        CONC_STAT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CONC_STATActionPerformed(evt);
            }
        });

        PERM_STAT.setFont(new java.awt.Font("Swis721 WGL4 BT", 0, 14)); // NOI18N
        PERM_STAT.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "NINGUNO", "LECTURA", "ESCRITURA" }));
        PERM_STAT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PERM_STATActionPerformed(evt);
            }
        });

        UPDATE_BUTTON.setFont(new java.awt.Font("Swis721 LtEx BT", 0, 14)); // NOI18N
        UPDATE_BUTTON.setText("Guardar");
        UPDATE_BUTTON.setFocusPainted(false);
        UPDATE_BUTTON.setMaximumSize(new java.awt.Dimension(133, 36));
        UPDATE_BUTTON.setMinimumSize(new java.awt.Dimension(133, 36));
        UPDATE_BUTTON.setPreferredSize(new java.awt.Dimension(133, 36));
        UPDATE_BUTTON.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UPDATE_BUTTONActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(UPDATE_BUTTON, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 629, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(78, 78, 78)
                            .addComponent(jLabel14)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(FIND_USER, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(36, 36, 36)
                            .addComponent(FIND_BUTTON, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 618, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 387, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 387, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 387, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(jLabel18)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(CONC_STAT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(23, 23, 23)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel19)
                                    .addGap(9, 9, 9)
                                    .addComponent(USER_STAT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(37, 37, 37)
                                    .addComponent(jLabel17)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(PERM_STAT, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(MAT_STAT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(36, 36, 36)
                                    .addComponent(jLabel13)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(PER_STAT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGap(41, 41, 41)
                            .addComponent(jLabel16)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(NIVEL_STAT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(53, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(jLabel15)
                    .addContainerGap(613, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel1)
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(FIND_USER)
                    .addComponent(FIND_BUTTON, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(52, 52, 52)
                .addComponent(jLabel5)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(PER_STAT, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(NIVEL_STAT, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(MAT_STAT, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addComponent(jLabel7)
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CONC_STAT, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addComponent(jLabel6)
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(USER_STAT, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(PERM_STAT, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addComponent(UPDATE_BUTTON, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(51, 51, 51))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(272, 272, 272)
                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(394, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void FIND_USERActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FIND_USERActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_FIND_USERActionPerformed

    private void FIND_BUTTONActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FIND_BUTTONActionPerformed
        String user_id = FIND_USER.getText();
        String modulo, submodulo;
        ArrayList modulo_array;
        String valor;

        /*++++++++++++++++++MODULO 1+++++++++++++++++++*/
        modulo_array = lista.get(0);
        modulo = (String) modulo_array.get(0);

        submodulo = (String) modulo_array.get(1);
        valor = obtenerValor(user_id, modulo, submodulo);
        MAT_STAT.setSelectedItem(valor);

        submodulo = (String) modulo_array.get(2);
        valor = obtenerValor(user_id, modulo, submodulo);
        PER_STAT.setSelectedItem(valor);

        submodulo = (String) modulo_array.get(3);
        valor = obtenerValor(user_id, modulo, submodulo);
        NIVEL_STAT.setSelectedItem(valor);

        /*++++++++++++++++++MODULO 2+++++++++++++++++++*/
        modulo_array = lista.get(1);
        modulo = (String) modulo_array.get(0);

        submodulo = (String) modulo_array.get(1);
        valor = obtenerValor(user_id, modulo, submodulo);
        CONC_STAT.setSelectedItem(valor);

        /*++++++++++++++++++MODULO 3+++++++++++++++++++*/
        modulo_array = lista.get(2);
        modulo = (String) modulo_array.get(0);

        submodulo = (String) modulo_array.get(1);
        valor = obtenerValor(user_id, modulo, submodulo);
        USER_STAT.setSelectedItem(valor);

        submodulo = (String) modulo_array.get(2);
        valor = obtenerValor(user_id, modulo, submodulo);
        PERM_STAT.setSelectedItem(valor);

    }//GEN-LAST:event_FIND_BUTTONActionPerformed

    private void PER_STATActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PER_STATActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PER_STATActionPerformed

    private void USER_STATActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_USER_STATActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_USER_STATActionPerformed

    private void NIVEL_STATActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NIVEL_STATActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_NIVEL_STATActionPerformed

    private void MAT_STATActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MAT_STATActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_MAT_STATActionPerformed

    private void CONC_STATActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CONC_STATActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CONC_STATActionPerformed

    private void PERM_STATActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PERM_STATActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PERM_STATActionPerformed

    private void UPDATE_BUTTONActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UPDATE_BUTTONActionPerformed
        String user_id = FIND_USER.getText();
        Usuario usuario = S_Usuarios.obtenerUsuario(user_id);

        if (!S_Usuarios.permisoUsuario(usuario)) {
            //PRIMERA VEZ QUE GUARDAN PERMISOS DE USUARIO
            System.out.println("\nCREANDO PERMISOS...");
            tipoManejo("INSERT", user_id);

        } else {
            //MODIFICACIÓN DE PERMISOS DE USUARIO
            System.out.println("\nMODIFICANDO PERMISOS...");
            tipoManejo("UPDATE", user_id);

        }
        // TODO add your handling code here:
    }//GEN-LAST:event_UPDATE_BUTTONActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(G_Permiso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(G_Permiso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(G_Permiso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(G_Permiso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new G_Permiso().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> CONC_STAT;
    private javax.swing.JButton FIND_BUTTON;
    private javax.swing.JTextField FIND_USER;
    private javax.swing.JComboBox<String> MAT_STAT;
    private javax.swing.JComboBox<String> NIVEL_STAT;
    private javax.swing.JComboBox<String> PERM_STAT;
    private javax.swing.JComboBox<String> PER_STAT;
    private javax.swing.JButton UPDATE_BUTTON;
    private javax.swing.JComboBox<String> USER_STAT;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JSeparator jSeparator1;
    // End of variables declaration//GEN-END:variables
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
 /*          FAC_001 - 11/05/17 - fin                */
 /*++++++++++++++++++++++++++++++++++++++++++++++++++*/
