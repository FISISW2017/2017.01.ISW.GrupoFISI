/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servicios;

import Controladores.C_Conceptos;
import Controladores.C_Documentos;
import Controladores.C_Nivel;
import Controladores.C_Periodo;
import Modelo.Concepto;
import Modelo.Documento;
import Modelo.Nivel;
import Modelo.Periodo;
import java.util.List;

/**
 *
 * @author Richard
 */
public class S_Periodo {
    
    
    public static void registrarPeriodo(Periodo periodo)
    {
        C_Periodo.nuevo(periodo);
    }
    
    public static Periodo buscarPeriodo(String año)
    {
        return C_Periodo.ver(año);
    }
    
    public static void modificarPeriodo(Periodo periodo)
    {
        C_Periodo.modificar(periodo);
    }
    
    
    
    
    public static List<Concepto> listaConceptos()
    {
        return C_Conceptos.lista("todo");
    }
    
    public static void registrarConcepto(Concepto concepto)
    {
        C_Conceptos.nuevo(concepto);
    }
    
    public static Concepto buscarConcepto(String id)
    {
        return C_Conceptos.ver(id);
    }
    
    public static boolean existeConcepto(String id)
    {
        boolean estado = false;
        if(C_Conceptos.ver(id) != null)
            estado = true;
        return estado;
    }
            
    
    public static void desactivarConcepto(String id)
    {
        Concepto concepto = C_Conceptos.ver(id);
        concepto.setConceptoEstado(false);
        C_Conceptos.modificar(concepto);
    }
    
      public static void activarConcepto(String id)
    {
        Concepto concepto = C_Conceptos.ver(id);
        concepto.setConceptoEstado(true);
        C_Conceptos.modificar(concepto);
    }
      
      public static void modificarConcepto(Concepto concepto)
      {
          C_Conceptos.modificar(concepto);
      }
      
      
   
      public static List<Documento> losDocumentos()
      {
          return C_Documentos.lista();
      }
      
      public static boolean existeDocumento(String id)
      {
          Documento doc = C_Documentos.ver(id);
          if(doc != null)
              return true;
          else 
              return false;
      }
      
      public static void registrarDocumento(Documento documento)
      {
          C_Documentos.nuevo(documento);
      }
      
      public static void modificarDocumento(Documento documento)
      {
          C_Documentos.modificar(documento);
      }
      
      public static Documento buscarDocumento(String id)
      {
          return C_Documentos.ver(id);
      }
      
      public static void desactivarDocumento(String id)
      {
          Documento doc = C_Documentos.ver(id);
          doc.setDocumentoEstado(false);
          C_Documentos.modificar(doc);
      }
      
         public static void activarDocumento(String id)
      {
          Documento doc = C_Documentos.ver(id);
          doc.setDocumentoEstado(true);
          C_Documentos.modificar(doc);
      }
}
