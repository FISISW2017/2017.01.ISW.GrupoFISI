/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servicios;

import Controladores.C_Grado;
import Controladores.C_Nivel;
import Modelo.Grado;
import Modelo.Nivel;
import java.util.List;

/**
 *
 * @author Richard
 */
public class S_NivelGrados {
    
    
  public static List<Grado> losGrados(String nivel)
  {
      return C_Grado.lista(nivel);    
  }
  
  
  
  public static void modificarVacantesSecciones(List<Grado> lista)
  {
      for(int i = 0; i < lista.size();i++)
            C_Grado.modificar(lista.get(i));
  }
  
  public static List<Nivel> losNiveles()
  {
      return C_Nivel.lista();
  }
          
}
