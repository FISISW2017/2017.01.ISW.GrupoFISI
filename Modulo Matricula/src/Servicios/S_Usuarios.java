/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*          FAC_001 - 11/05/17 - inicio             */
/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
package Servicios;

import Controladores.C_Usuario;
import Controladores.C_TipoPermiso;
import Controladores.C_Submodulo;
import Controladores.C_Modulo;
import Controladores.C_Permiso;
import Modelo.Modulo;
import Modelo.PermisosUsuarios;
import Modelo.PermisosUsuariosId;
import Modelo.Submodulo;
import Modelo.SubmoduloId;
import Modelo.TipoPermisos;
import Modelo.Usuario;
import java.util.List;

/**
 *
 * @author Fabio
 */
public class S_Usuarios {
    public static void registrarUsuario(Usuario usuario){
        C_Usuario.nuevo(usuario);
    }
    public static Usuario buscarUsuario(String apellidos, String nombres){
        return C_Usuario.usuario(apellidos,nombres);
    }
    public static Usuario buscarUsuario(String dni){
        return C_Usuario.usuario(dni);
    }
    public static Usuario obtenerUsuario(String user_id){
        return C_Usuario.ver(user_id);
    }
    public static void modificarUsuario(Usuario usuario){
        C_Usuario.modificar(usuario);
    } 
    public static TipoPermisos obtenerPermiso(char codPermiso){
        return C_TipoPermiso.ver(codPermiso);
    }
    public static Submodulo obtenerSubmodulo(String moduloId,String submodulo){
        return C_Submodulo.ver(moduloId,submodulo);
    }
      public static List<Submodulo> listarSubmodulo(Modulo modulo){
        return C_Submodulo.lista(modulo);
    } 
     public static Modulo obtenerModulo(String moduloId){
        return C_Modulo.ver(moduloId);
    }
     public static List<Modulo> listarModulo(){
        return C_Modulo.lista();
    } 
    public static PermisosUsuarios valorPermiso(Usuario usuario, Modulo modulo, Submodulo submodulo){
        return C_Permiso.permiso(usuario, modulo,submodulo);
    }
    public static boolean permisoUsuario(Usuario usuario){
        return C_Permiso.permisoUsuario(usuario);
    }
    public static boolean verSubmodulo(Usuario usuario, Modulo modulo, Submodulo submodulo){
        return C_Permiso.permisoSubmodulo(usuario, modulo, submodulo);
    }
    public static boolean verModulo(Usuario usuario, Modulo modulo){
        return C_Permiso.permisoModulo(usuario, modulo);
    }
    public static void registrarPermiso(PermisosUsuarios permiso){
        C_Permiso.nuevo(permiso);
    }
     public static void modificarPermiso(PermisosUsuarios permiso){
        C_Permiso.modificar(permiso);
    } 
    
     
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*          FAC_001 - 11/05/17 - fin                */
/*++++++++++++++++++++++++++++++++++++++++++++++++++*/