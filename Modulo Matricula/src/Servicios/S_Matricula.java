/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servicios;

import Controladores.C_Alumno;
import Controladores.C_Familiar;
import Controladores.C_Matricula;
import Modelo.Alumno;

import Modelo.Matricula;
import java.util.List;

/**
 *
 * @author Richard
 */
public class S_Matricula {
    
    public static Alumno buscarAlumno(String codigo)
    {
        return C_Alumno.ver(codigo);
    }
    

    
    public static Matricula matriculaActual(String codigo)
    {
        List<Matricula> matriculas = C_Matricula.matriculasDe(codigo);
        return matriculas.get(matriculas.size()-1);
    }
    
    public static boolean existeDNIalumno(String dni)
    {
        Alumno alumno = C_Alumno.alumno(dni);
        if(alumno == null)
            return false;
        else
            return true;
    }
    
    private static String codigoAlumno()
    {
        int maximo = C_Alumno.lista("todo").size();
        String valor = ""+maximo;
        String codigo = "";
        for(int i = valor.length();i < 14;i++)
            codigo = codigo+"0";
        
        codigo = codigo +valor;
        return codigo;           
    }
    
    public static void AlumnoNuevo(Alumno alumno)
    {
        alumno.setAlumnoCodigo(codigoAlumno());
        C_Alumno.nuevo(alumno);
    }
    
}
