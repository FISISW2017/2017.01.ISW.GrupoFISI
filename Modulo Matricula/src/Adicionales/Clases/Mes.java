/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Adicionales.Clases;

import java.util.Date;

/**
 *
 * @author Richard
 */
public class Mes {
    
    public String nombre(int num) 
    {
        String mes = "";
        switch(num)
        {
            case 3: mes = "MARZO";break;
            case 4: mes = "ABRIL";break;
            case 5: mes = "MAYO";break;
            case 6: mes = "JUNIO";break;
            case 7: mes = "JULIO";break;
            case 8: mes = "AGOSTO";break;
            case 9: mes = "SETIEMBRE";break;
            case 10: mes = "OCTUBRE";break;
            case 11: mes = "NOVIEMBRE";break;
            case 12: mes = "DICIEMBRE";break;
        }
        
        return mes;
    }
  
    
}
