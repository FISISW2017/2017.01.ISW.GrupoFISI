/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Adicionales.Estilos;

import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author Richard
 */
public class Icono {
    
    public Icon confirmar()
    {  return  new ImageIcon(getClass().getResource("/Imagenes/Opciones/Confirmacion.png"));
    
    }
    
    public Icon aceptar()
    {  return  new ImageIcon(getClass().getResource("/Imagenes/Opciones/Aceptar.png"));
    
    }
    
     public Icon error()
    {  return  new ImageIcon(getClass().getResource("/Imagenes/Opciones/Error.png"));
    
    }
    
}
