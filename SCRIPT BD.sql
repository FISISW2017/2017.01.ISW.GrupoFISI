
create database sistema_calidad;

use sistema_calidad;


/*==============================================================*/
/* Table: CODIGOS                                             */
/*==============================================================*/
create table CODIGOS
(
	COD_ID varchar(254) not null,
    ESTADO char(1) not null,
    primary key (COD_ID)
);
/*==============================================================*/
/* Table: AUDITORIA                                             */
/*==============================================================*/
create table AUDITORIA
(
   AUDITORIA_FECHA      date not null,
   USUARIO_ID           varchar(4) not null,
   DOC_ID               varchar(14) not null,
   AUDIT_COMMENTS       text,
   primary key (AUDITORIA_FECHA, USUARIO_ID, DOC_ID)
);

/*==============================================================*/
/* Table: CRITERIO                                              */
/*==============================================================*/
create table CRITERIO
(
   CRITERIO_ID          varchar(9) not null,
   PROGRAMA_ID          varchar(3) not null,
   PRO_PROCESO_ID       varchar(3) not null,
   NIVEL_ID             varchar(7) not null,
   JERARQUIA_ID         int not null,
   PROCESO_ID           varchar(3) not null,
   ESTADO_CUMPL_ID   varchar(5),
   CRITERIO_DESCRIPCION text not null,
   VALID_PRCT_OBLIG     Double,
   INGRESA_PRCT_OBLIG   Double,
   VALID_PRCT_OPCIO     Double,
   INGRESA_PRCT_OPCIO   Double,
   primary key (CRITERIO_ID)
);

/*==============================================================*/
/* Table: DOCUMENTO                                             */
/*==============================================================*/
create table DOCUMENTO
(
   DOC_ID               varchar(14) not null,
   DOC_F_CREACION       datetime not null,
   FUENTE_ID            varchar(11) not null,
   USUARIO_ID           varchar(4),
   DOC_NOMBRE           varchar(254) not null,
   DOC_STATUS           varchar(10),
   primary key (DOC_ID)
);

/*==============================================================*/
/* Table: ESTADO_CUMPL                                          */
/*==============================================================*/
create table ESTADO_CUMPL
(
   ESTADO_CUMPL_ID      varchar(5) not null,
   ESTADO_CUMPL_SHORT   varchar(3) not null,
   ESTADO_CUMPL_LONG    varchar(15) not null,
   ESTADO_CUMPL_DESCR   varchar(256) not null,
   primary key (ESTADO_CUMPL_ID)
);

/*==============================================================*/
/* Table: ESTADO_PROCESO                                        */
/*==============================================================*/
create table ESTADO_PROCESO
(
   ESTADO_PROC_ID       varchar(5) not null,
   ESTADO_PROC_SHORT    char(1) not null,
   ESTADO_PROC_LONG     varchar(15) not null,
   ESTADO_PORC_DESCR    varchar(256),
   primary key (ESTADO_PROC_ID)
);

/*==============================================================*/
/* Table: ESTADO_PROC_PROG                                      */
/*==============================================================*/
create table ESTADO_PROC_PROG
(
   ESTADO_PROCPROG_ID   varchar(5) not null,
   ESTADO_PROCPROG_SHORT varchar(3) not null,
   ESTADO_PROCPROG_LONG varchar(15) not null,
   ESTADO_PROCPROG_DESCR varchar(256) not null,
   primary key (ESTADO_PROCPROG_ID)
);

/*==============================================================*/
/* Table: ESTADO_VALIDACION                                     */
/*==============================================================*/
create table ESTADO_VALIDACION
(
   ESTADO_VALID_ID      varchar(5) not null,
   ESTADO_VALID_SHORT   varchar(3) not null,
   ESTADO_VALID_LONG    varchar(15) not null,
   ESTADO_VALID_DESCR   varchar(256) not null,
   primary key (ESTADO_VALID_ID)
);

/*==============================================================*/
/* Table: FACULTAD                                              */
/*==============================================================*/
create table FACULTAD
(
   FACULTAD_ID          varchar(3) not null,
   FACULTAD_NOMBRE      varchar(40) not null,
   primary key (FACULTAD_ID)
);

/*==============================================================*/
/* Table: FUENTE_VERIFICACION                                   */
/*==============================================================*/
create table FUENTE_VERIFICACION
(
   CRITERIO_ID          varchar(9) not null,
   FUENTE_ID            varchar(11) not null,
   ESTADO_VALID_ID      varchar(5),
   FUENTE_TIPO          varchar(10),
   FUENTE_DESCR         varchar(30),
   FUENTE_OBLIGATORIA   bool,
   primary key (FUENTE_ID)
);

/*==============================================================*/
/* Table: JERARQUIA                                             */
/*==============================================================*/
create table JERARQUIA
(
   JERARQUIA_ID         int not null,
   PROCESO_ID           varchar(3) not null,
   JERARQUIA_NOMBRE     text not null,
   primary key (JERARQUIA_ID, PROCESO_ID)
);

/*==============================================================*/
/* Table: MODULO                                                */
/*==============================================================*/
create table MODULO
(
   MODULO_ID            varchar(3) not null,
   FACULTAD_ID          varchar(3) not null,
   MODULO_NOMBRE        varchar(20) not null,
   MODULO_USUARIOS      int not null,
   primary key (MODULO_ID, FACULTAD_ID)
);

/*==============================================================*/
/* Table: NIVELES                                               */
/*==============================================================*/
create table NIVELES
(
   NIVEL_ID             varchar(7) not null,
   JERARQUIA_ID         int not null,
   PROCESO_ID           varchar(3) not null,
   NIVEL_NOMBRE         varchar(150) not null,
   primary key (NIVEL_ID, JERARQUIA_ID, PROCESO_ID)
);

/*==============================================================*/
/* Table: NIVEL_INFERIOR                                        */
/*==============================================================*/
create table NIVEL_INFERIOR
(
   ID_INF               varchar(15) not null,
   NIVEL_ID             varchar(7) not null,
   JERARQUIA_ID         int not null,
   PROCESO_ID           varchar(3) not null,
   ID_SUP               varchar(15) not null,
   primary key (ID_INF, ID_SUP)
);

/*==============================================================*/
/* Table: NIVEL_SUPERIOR                                        */
/*==============================================================*/
create table NIVEL_SUPERIOR
(
   ID_SUP               varchar(15) not null,
   NIVEL_ID             varchar(7) not null,
   JERARQUIA_ID         int not null,
   PROCESO_ID           varchar(3) not null,
   primary key (ID_SUP)
);

/*==============================================================*/
/* Table: NIV_PROC_PROGRAMA                                     */
/*==============================================================*/
create table NIV_PROC_PROGRAMA
(
   PROGRAMA_ID          varchar(3) not null,
   PRO_PROCESO_ID       varchar(3) not null,
   NIVEL_ID             varchar(7) not null,
   JERARQUIA_ID         int not null,
   PROCESO_ID           varchar(3) not null,
   ESTADO_CUMPL_ID      varchar(5),
   primary key (PROGRAMA_ID, PRO_PROCESO_ID, NIVEL_ID, JERARQUIA_ID, PROCESO_ID)
);

/*==============================================================*/
/* Table: PROCESO                                               */
/*==============================================================*/
create table PROCESO
(
   PROCESO_ID           varchar(3) not null,
   ESTADO_PROC_ID       varchar(5),
   PROCESO_NOMBRE       varchar(40) not null,
   DESCRIPCION			varchar(254) not null,
   CRITERIOS_GLOBAL     bool not null,
   primary key (PROCESO_ID)
);

/*==============================================================*/
/* Table: PROCESO_PROGRAMA                                      */
/*==============================================================*/
create table PROCESO_PROGRAMA
(
   PROGRAMA_ID          varchar(3) not null,
   PROCESO_ID           varchar(3) not null,
   ESTADO_PROCPROG_ID   varchar(5),
   ESTADO_CUMPL_ID      varchar(5),
   CONSIDERA_OPCIONALES bool,
   VALIDACION_HABILITADA bool,
   primary key (PROGRAMA_ID, PROCESO_ID)
);

/*==============================================================*/
/* Table: PROGRAMA                                              */
/*==============================================================*/
create table PROGRAMA
(
   PROGRAMA_ID          varchar(3) not null,
   FACULTAD_ID          varchar(3) not null,
   PROGRAMA_NOMBRE      varchar(40) not null,
   PROGRAMA_CODIGO      varchar(3),
   primary key (PROGRAMA_ID)
);

/*==============================================================*/
/* Table: USUARIO                                               */
/*==============================================================*/
create table USUARIO
(
   USUARIO_ID           varchar(4) not null,
   MODULO_ID            varchar(3),
   FACULTAD_ID          varchar(3),
   USUARIO_NOMBRE       varchar(40) not null,
   USUARIO_APELLIDO     char(40) not null,
   primary key (USUARIO_ID)
);

alter table AUDITORIA add constraint FK_RELATIONSHIP_13 foreign key (USUARIO_ID)
      references USUARIO (USUARIO_ID) on delete restrict on update restrict;

alter table AUDITORIA add constraint FK_RELATIONSHIP_16 foreign key (DOC_ID)
      references DOCUMENTO (DOC_ID) on delete restrict on update restrict;

alter table CRITERIO add constraint FK_RELATIONSHIP_19 foreign key (PROGRAMA_ID, PRO_PROCESO_ID, NIVEL_ID, JERARQUIA_ID, PROCESO_ID)
      references NIV_PROC_PROGRAMA (PROGRAMA_ID, PRO_PROCESO_ID, NIVEL_ID, JERARQUIA_ID, PROCESO_ID) on delete restrict on update restrict;

alter table CRITERIO add constraint FK_RELATIONSHIP_35 foreign key (ESTADO_CUMPL_ID)
      references ESTADO_CUMPL (ESTADO_CUMPL_ID) on delete restrict on update restrict;

alter table DOCUMENTO add constraint FK_RELATIONSHIP_15 foreign key (USUARIO_ID)
      references USUARIO (USUARIO_ID) on delete restrict on update restrict;

alter table DOCUMENTO add constraint FK_RELATIONSHIP_24 foreign key (FUENTE_ID)
      references FUENTE_VERIFICACION (FUENTE_ID) on delete restrict on update restrict;

alter table FUENTE_VERIFICACION add constraint FK_RELATIONSHIP_22 foreign key (CRITERIO_ID)
      references CRITERIO (CRITERIO_ID) on delete restrict on update restrict;

alter table FUENTE_VERIFICACION add constraint FK_RELATIONSHIP_31 foreign key (ESTADO_VALID_ID)
      references ESTADO_VALIDACION (ESTADO_VALID_ID) on delete restrict on update restrict;

alter table JERARQUIA add constraint FK_RELATIONSHIP_20 foreign key (PROCESO_ID)
      references PROCESO (PROCESO_ID) on delete restrict on update restrict;

alter table MODULO add constraint FK_RELATIONSHIP_14 foreign key (FACULTAD_ID)
      references FACULTAD (FACULTAD_ID) on delete restrict on update restrict;

alter table NIVELES add constraint FK_RELATIONSHIP_26 foreign key (JERARQUIA_ID, PROCESO_ID)
      references JERARQUIA (JERARQUIA_ID, PROCESO_ID) on delete restrict on update restrict;

alter table NIVEL_INFERIOR add constraint FK_RELATIONSHIP_23 foreign key (NIVEL_ID, JERARQUIA_ID, PROCESO_ID)
      references NIVELES (NIVEL_ID, JERARQUIA_ID, PROCESO_ID) on delete restrict on update restrict;

alter table NIVEL_INFERIOR add constraint FK_RELATIONSHIP_27 foreign key (ID_SUP)
      references NIVEL_SUPERIOR (ID_SUP) on delete restrict on update restrict;

alter table NIVEL_SUPERIOR add constraint FK_RELATIONSHIP_21 foreign key (NIVEL_ID, JERARQUIA_ID, PROCESO_ID)
      references NIVELES (NIVEL_ID, JERARQUIA_ID, PROCESO_ID) on delete restrict on update restrict;

alter table NIV_PROC_PROGRAMA add constraint FK_RELATIONSHIP_17 foreign key (NIVEL_ID, JERARQUIA_ID, PROCESO_ID)
      references NIVELES (NIVEL_ID, JERARQUIA_ID, PROCESO_ID) on delete restrict on update restrict;

alter table NIV_PROC_PROGRAMA add constraint FK_RELATIONSHIP_18 foreign key (PROGRAMA_ID, PRO_PROCESO_ID)
      references PROCESO_PROGRAMA (PROGRAMA_ID, PROCESO_ID) on delete restrict on update restrict;

alter table NIV_PROC_PROGRAMA add constraint FK_RELATIONSHIP_34 foreign key (ESTADO_CUMPL_ID)
      references ESTADO_CUMPL (ESTADO_CUMPL_ID) on delete restrict on update restrict;

alter table PROCESO add constraint FK_RELATIONSHIP_30 foreign key (ESTADO_PROC_ID)
      references ESTADO_PROCESO (ESTADO_PROC_ID) on delete restrict on update restrict;

alter table PROCESO_PROGRAMA add constraint FK_RELATIONSHIP_28 foreign key (PROGRAMA_ID)
      references PROGRAMA (PROGRAMA_ID) on delete restrict on update restrict;

alter table PROCESO_PROGRAMA add constraint FK_RELATIONSHIP_29 foreign key (PROCESO_ID)
      references PROCESO (PROCESO_ID) on delete restrict on update restrict;

alter table PROCESO_PROGRAMA add constraint FK_RELATIONSHIP_32 foreign key (ESTADO_PROCPROG_ID)
      references ESTADO_PROC_PROG (ESTADO_PROCPROG_ID) on delete restrict on update restrict;

alter table PROCESO_PROGRAMA add constraint FK_RELATIONSHIP_33 foreign key (ESTADO_CUMPL_ID)
      references ESTADO_CUMPL (ESTADO_CUMPL_ID) on delete restrict on update restrict;

alter table PROGRAMA add constraint FK_RELATIONSHIP_1 foreign key (FACULTAD_ID)
      references FACULTAD (FACULTAD_ID) on delete restrict on update restrict;

alter table USUARIO add constraint FK_RELATIONSHIP_25 foreign key (MODULO_ID, FACULTAD_ID)
      references MODULO (MODULO_ID, FACULTAD_ID) on delete restrict on update restrict;

